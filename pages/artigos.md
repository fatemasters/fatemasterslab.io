---
layout: page
show_meta: false
title: "Artigos e Reviews"
header:
    image_fullwidth: FundoBlog.png
    permalink: "/artigos/"
---

Aqui você encontra artigos e reviews sobre Fate escritos pelos Fate Masters e convidados:

<ul>
    {% for post in site.categories.artigos %}
    <li><a href="{{ post.url }}">{{ post.title | markdownify | remove: '<p>' | remove: '</p>' }}</a></li>
{% comment %}
{{ post.content }}
{% endcomment %}
    {% endfor %}
</ul>

