---
title: "Fate Masters Episódio 46 - Fate para Leigos"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-04-22 21:00:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Leigos
 - Básicos
 - inicial
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "93min"
itunes:
  duration: 01:33:34
audios:
 - OGG: https://archive.org/download/FateMasters46FateParaLeigos/FateMasters46-FateParaLeigos.ogg
 - MP3: https://archive.org/download/FateMasters46FateParaLeigos/FateMasters46-FateParaLeigos.mp3
iaplayer: FateMasters46FateParaLeigos
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:08"
    text: Uma Introdução e a História do Fate
  - time: "00:07:27"
    text: Sobre a colaboração e improviso no Fate, e sobre como o Fate não joga fora a Regra, mas sim como ele a molda para a narrativa
  - time: "00:11:08"
    text: Sobre os pilares do Fate, iniciando pelos Aspectos, que são o pilar que funciona como uma cola que liga narrativa e cenário, mecânica e personagem
  - time: "00:22:23"
    text: Sobre as Competências em Fate, independente de como elas são chamadas, e como, ao mesmo tempo que usar Adjetivos ajuda no nivelamento do personagem, o Fate não se preocupa tanto com o equilibrio de poder, mas sim no de _"tempo de tela"_ 
  - time: "00:43:27"
    text: Uma Introdução no _Fractal do Fate_ antes de entrar em Façanhas, aquilo que vai traduzir superpoderes, magia, treinamentos, equipamentos e qualquer outra coisa que torne seu personagem especial de alguma forma
  - time: "00:49:45"
    text: Sobre as Quatro Ações e as Quatro Resoluções, que resumem de maneira geral TODO tipo de Ação em Fate
  - time: "01:03:04"
    text: Estresse e Consequências, a forma que o Fate tem de traduzir o Dano que o personagem sofre
  - time: "01:11:46"
    text: Sobre Recarga e Pontos de Destino
  - time: "01:21:30"
    text: O Fractal do Fate e como isso ajuda demais para determinar coisas que interajam com o jogador, de chuva torrencial à PRÓPRIA CAMPANHA!
  - time: "01:27:53"
    text: Considerações Finais
related_links:
  - text: Podcast do QuestCast sobre Fate
    link: https://questcast.com.br/podcast/fate-e-o-novo-gurps-taverna-do-jasper-35/
  - text: Podcast do Café com Dungeon sobre Fate
    link: https://regradacasa.podbean.com/e/cafe-com-dungeon-141-fate-rpg/
  - text: Maestrina Regina Orlana, personagem de Nest citado
    link: http://fabiocosta0305.gitlab.io/personagens/MaestrinaReginaOrlana/
  - text: Rolando +4 - o podcast dos que aceitaram o Destino
    link: http://rolandomaisquatro.gitlab.io/
  - text: Cena do Final Fantasy Advent Children que foi citada
    link: https://www.youtube.com/watch?v=pHAhx4H5RLM
  - text: Cena Final de WiFi Ralph citada no podcast
    link: https://www.youtube.com/watch?v=thnlOG5QHz0
  - text: Donald no País da Matemágica
    link: https://www.youtube.com/watch?v=wbftu093Yqk
  - text: Uprising
    link: https://www.drivethrurpg.com/product/191782/Good-Neighbors-o-A-World-of-Adventure-for-Fate-Core
  - text: Good Neighbors
    link: https://www.drivethrurpg.com/product/191782/Good-Neighbors-o-A-World-of-Adventure-for-Fate-Core
  - text: Daring Comics
    link: https://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
  - text: Maestrina Regina Orlana, personagem de Nest citado
    link: http://fabiocosta0305.gitlab.io/personagens/MaestrinaReginaOrlana/
  - text: Connor McRabbit, personagem de Destinos de Desenhos Animados citado
    link: http://fabiocosta0305.gitlab.io/personagens/MaestrinaReginaOrlana/
episode: 64
---

E temos um novo Fate Masters, e um muito importante.

Depeois de ouvirmos alguns podcasts que, na opinião dos Fate Masters Fábio, Luís e Rafael, deixaram um pouco a desejar ao tentar falar sobre o Fate. Então, foi necessário a gente pegar e explicar como vemos que funciona o Fate. O fato de falarmos sobre ele desde 2016 quer dizer alguma coisa, afinal de contas? ;-)

Falamos sobre os pilares do Fate e como eles se conectam no jogo e como eles interagem entre si:

+ Aspectos: o pilar que liga regras, cenário e personagem
+ Competências (Perícias, Façanhas, etc...): o que mede a capacidade do personagem de resolver problemas e ajuda a manter o equilibrio de tempo de tela
+ Façanhas: o que permite que seu personagem seja interessante de maneira única e especial
+ As Quatro Ações e Quatro Resoluções: como você age em jogo e como são resolvidas tais ações
+ Estresse e Consequências: o dano no Fate
+ Recarga e Pontos de Destino: a "moeda de troca" que permite que haja a interação entre narrativa e mecânica e vice-versa
+ Fractal do Fate: o mecanismo para tratar o ambiente como um personagem que interage com os demais

Além de trazer o _Algoritmo de Valpaços para criação de Aspectos de Personagens_:

1. Escreva 6 a 7 frases que conte a história do personagem
1. Marque as cinco mais importantes
2. Escolha aquela que descreve melhor o personagem como um todo: esse é o _Conceito_
3. Escolha aquela que aparenta ser o defeito do personagem: esse será a _Dificuldade_
4. Distribua as três outras nos demais Aspectos

E sobre como o Fate sofre do _"problema 3D&T"_, onde pouco importa você usar o cuspe, um revolver ou um canhão espacial, se nada indicar.

E sobre como, apesar de tudo, ___FATE NÃO É UMA MARMELADA!___

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

