---
title: "Fate Masters Episódio 42 - Fate Masters Analisa: Fate Horror Toolkit"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2018-08-06 16:10:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate-Masters-Analisa
 - Fate-Horror-Toolkit
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Rafael Sant'anna Meyer
soundtrack:
- music: Tubular Bells (Tema de _O Exorcista_)
  artist: Mike Oldfield
  link: https://www.youtube.com/watch?v=33nyRB4PY14
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
podcast_time: "116min"
itunes:
  duration: 01:56:34
audios:
 - OGG: https://archive.org/download/FateMasters42AnalisaHorrrorToolkit/FateMasters42-AnalisaHorrrorToolkit.ogg
 - MP3: https://archive.org/download/FateMasters42AnalisaHorrrorToolkit/FateMasters42-AnalisaHorrrorToolkit.mp3
iaplayer: FateMasters42AnalisaHorrrorToolkit
related_links:
- text: Fate Horror Toolkit
  link: http://www.drivethrurpg.com/product/242625/Fate-Horror-Toolkit
- text: Savagecast
  link: http://savagecast.com.br
- text: Pensando RPG
  link: http://www.pensandoded.com.br/search/label/Podcast
- text: Sigmata
  link: http://www.drivethrurpg.com/product/247973/SIGMATA-This-Signal-Kills-Fascists
- text: Fate Toolkits
  link: https://www.evilhat.com/home/fate-toolkits/
- text: X-Card
  link: http://fabiocosta0305.gitlab.io/rpg/Xcard.br/
- text: Controle Remoto
  link: http://tinyurl.com/nphed7m
- text: Drácula
  link: https://en.wikipedia.org/wiki/Dracula
- text: Bluebeard’s Wife
  link: http://www.magpiegames.com/bluebeards-bride/
- text: Participação do Kiss no Scooby Doo
  link: https://www.youtube.com/watch?v=k_plih5b9To 
- text: Hellraiser
  link: https://en.wikipedia.org/wiki/Hellraiser
- text: Povo Antigo de Yith
  link: https://en.wikipedia.org/wiki/Great_Race_of_Yith
- text: Alien
  link: https://en.wikipedia.org/wiki/Alien_(film)
- text: Aliens
  link: https://en.wikipedia.org/wiki/Aliens_(film)
- text: Ravenloft
  link: https://en.wikipedia.org/wiki/Ravenloft
- text: Relógio do Apocalipse
  link: https://thebulletin.org/doomsday-clock/
- text: Sombras Urbanas
  link: http://astereditora.com.br/loja/produto-tag/sombras-urbanas/
- text: Apocalypse World
  link: http://apocalypse-world.com 
- text: Masks
  link: http://www.magpiegames.com/masks/
- text: Radiofobia
  link: http://radiofobia.com.br/
- text: Café com Dungeon
  link: http://regradacasa.com.br/category/podcast/
- text: Resenha do Fate Horror Toolkit na Dungeon Geek
  link: https://www.dungeongeek21.com/bg/resenha-fate-horror-toolkit
- text: Alô Tenica
  link: http://radiofobia.com.br/podcast/tag/alo-tenica/
- text: Cronistas das Trevas
  link: http://cronistasdastrevasbr.com
- text: Criminal Minds
  link: https://en.wikipedia.org/wiki/Criminal_Minds
- text: Hellblazer
  link: https://en.wikipedia.org/wiki/Hellblazer
- text: Dresden Files Accelerated
  link: https://www.evilhat.com/home/dresden-files-accelerated/
- text: Jump Scare
  link: https://en.wikipedia.org/wiki/Jump_scare
- text: A Profecia
  link: https://en.wikipedia.org/wiki/The_Omen
- text: Evil Dead
  link: https://en.wikipedia.org/wiki/The_Evil_Dead
- text: 1984
  link: https://en.wikipedia.org/wiki/Nineteen_Eighty-Four
- text: Corra!
  link: https://en.wikipedia.org/wiki/Get_Out
- text: Eles Vivem!
  link: https://en.wikipedia.org/wiki/They_Live
- text: Eu sou o senhor do meu castelo
  link: https://en.wikipedia.org/wiki/Je_suis_le_seigneur_du_château
- text: Armageddon
  link: https://en.wikipedia.org/wiki/Armageddon_(1998_film)
- text: Impacto Profundo
  link: https://en.wikipedia.org/wiki/Deep_Impact_(film)
- text: Walking Dead
  link: https://en.wikipedia.org/wiki/The_Walking_Dead_(franchise)
- text: Highschool of the Dead
  link: https://en.wikipedia.org/wiki/Highschool_of_the_Dead
- text: Gaslighting
  link: https://en.wikipedia.org/wiki/Gaslighting
- text: Abracadabra
  link: https://en.wikipedia.org/wiki/Hocus_Pocus_(1993_film)
- text: Goonies
  link: https://en.wikipedia.org/wiki/The_Goonies
events:
- time: "00:00:01"
  text: _Disclaimer_ de gatilho
- time: "00:01:25"
  text: Introdução
- time: "00:02:27"
  text: Recados da Paróquia e o Motivo da ausência do Cicerone do Fate Horror
- time: "00:06:44"
  text: Análise básica do _Fate Horror Toolkit_ e _disclaimer_ da cópia enviada pela Evil Hat ao Mr. Mickey
- time: "00:08:31"
  text: O mito sobre o Horror em Fate
- time: "00:11:22"
  text: A série _Toolkit_
- time: "00:14:04"
  text: Sobre o design do Fate Horror Toolkit
- time: "00:16:42"
  text: Princípios, Regras e _Frameworks_
- time: "00:24:07"
  text: "Entrando nas Regras: dicas gerais de criação de personagem, Sacrifício Heróico, Fantasma, Aspecto Heróico"
- time: "00:33:39"
  text: "Dilemas Morais, NPCs simpáticos"
- time: "00:40:32"
  text: Humanização de Vitímas, Reconstrução e Flashbacks
- time: "00:44:14"
  text: Pontos de Destino Comunais, Traumas e Corações Embrutecidos, Horror Visceral, Aspectos de Intensidade
- time: "00:59:24"
  text: Criando Monstros
- time: "01:06:50"
  text: Metamorfoses (Fatais ou não)
- time: "01:09:42"
  text: O Outro
- time: "01:13:12"
  text: Destino Inevitável
- time: "01:17:52"
  text: Horror de Sobrevivência
- time: "01:22:43"
  text: Horror intimista
- time: "01:26:07"
  text: Terrir
- time: "01:33:52"
  text: Apêndices - X-Card e Controle Remoto
- time: "01:36:29"
  text: Aproveitamento de Regras
- time: "01:40:47"
  text: Rankeamento e os comentários do Cicerone Luís Cavalheiro
- time: "01:54:45"
  text: Considerações Finais
episode: 60
---

___TW:___ discussão sobre elementos do Horror

Temos mais um Fate Masters Analisa.

Fazemos aqui a análise do _Fate Horror Toolkit_, onde falamos sobre esse conjunto de ferramenta para Horror, e falamos de coisas como Sacrifícios Heróicos, Monstros, Destinos Inevitáveis, e até mesmo sobre Terrir envolvendo _Scooby-Doo_ e o KISS.

{% include youtube.html id="k_plih5b9To" %}

E também vamos revelar o porque do Cicerone do Fate Horror, justo nesse episódio, ter ficado de fora. Afinal, mesmo ele tem seus gatilhos.

E fica os recados:

+ Respondam a [PodPesquisa](http://abpod.com.br/podpesquisa/) para ajudar o cenário de Podcasts no Brasil
+ O Fate Masters está agora no [Google Podcast](https://play.google.com/store/apps/details?id=com.google.android.apps.podcasts), que é uma alternativa para ouvir podcasts no Android!
+ E entrem no [servidor no Discord do Movimento Fate Brasil][fb-discord]

Por fim, lembrem-se: _jogar horror não é desculpa para ser horrível_!

[![jogar horror não é desculpa para ser horrível](/images/HorrorToolkitNaoSerHorrivel.png)](/images/HorrorToolkitNaoSerHorrivel.png)

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            | 5         |
| **2**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         | 4.83      |
| **4**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         | 4,78      |
| **5**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          | 4,73      |
| **6**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          | 4,68      |
| **8**       | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          | 4,58      |
| **9**       | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         | 4,5       |
| **10**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            | 4,33      |
| **11**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         | 4,25      |
| **12**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         | 3,92      |
| **13**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         | 3,5       |
| **14**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         | 4         |
|             | [_Horror Toolkit_][fm-HorrorToolkit]                  | 4,5            | 4,5            | 2            | 3,6       |

Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar na [comunidade do Google+ do Fate Masters][gplus], na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, pela [página do Fate Masters no Facebook][fb-page] e agora pelo [servidor do Movimento Fate Brasil no Discord][fb-discord]

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url 2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url 2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url 2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url 2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url 2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url 2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url 2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url 2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url 2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-mechavskaiju]:  {% post_url 2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url 2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url 2017-11-17-FateMasters35-AnalisaDaringComics %}
[fma-wearingthecape]: {% post_url 2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url 2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url 2018-05-23-FateMasters41-AnalisaSecretsOfCats %}
[fm-HorrorToolkit]: {% post_url 2018-08-06-FateMasters42-AnalisaHorrorToolkit %}

[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters
