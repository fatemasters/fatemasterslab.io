---
title: "Fate Masters Episódio 45 - Fate Masters Analisa Duplo: Nest e Boa Vizinhança (Good Neighbors)"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-03-26 09:30:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Nest
 - Boa Vizinhança
 - Good Neighbors
 - Analisa
 - Analisa Duplo
 - Fate Masters Analisa
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "78min"
itunes:
  duration: 01:17:47
audios:
 - OGG: https://archive.org/download/FateMasters45AnalisaNestBoaVizinhanca/FateMasters45-AnalisaNestBoaVizinhanca.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters45AnalisaNestBoaVizinhanca/FateMasters45-AnalisaNestBoaVizinhanca.mp3
     size: 74307584
iaplayer: FateMasters45AnalisaNestBoaVizinhanca
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
- music: Feather Duster
  artist: Silverman Sound Studios
  link: https://www.silvermansound.com/free-music/feather-duster
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:03:53"
    text: Começando os serviços com Boa Vizinhança
  - time: "00:09:10"
    text: Falando um pouco sobre a _Árvore de Reação_, o guia de eventos de _Boa Vizinhança_
  - time: "00:15:03"
    text: Falando sobre os eventos que começam tudo
  - time: "00:18:52"
    text: Criando os personagens de _Boa Vizinhança_ (tanto o humano quanto a fada) e ligando Façanhas aos Aspectos
  - time: "00:23:04"
    text: Falando da Recarga em Boa Vizinhança e sobre a questão das Abordagens de _Fate Acelerado_ nas Fadas
  - time: "00:27:11"
    text: Começando a falar sobre _Nest_
  - time: "00:32:38"
    text: "Os Reinos de _Nest_: Época, Bugigandela, Enigma e a Cidade de Eg"
  - time: "00:34:54"
    text: Comentários sobre os climas possíveis em _Nest_ e sobre as possibilidades do _Tirano_
  - time: "00:38:58"
    text: Sobre a criação de personagens, o rolamento em equipe, Marcos, Declarações Fantásticas e o Quadro de Reserva em _Nest_
  - time: "00:46:15"
    text: Sobre os Talismãs em _Nest_
  - time: "00:50:46"
    text: Aproveitando ideias e elementos de _Nest_ e de Boa Vizinhança
  - time: "01:03:03"
    text: Rankeando _Nest_ e Boa Vizinhança
  - time: "01:15:42"
    text: Considerações Finais
related_links:
  - text: Nest
    link: http://drivethrurpg.com/product/153980/Nest--A-World-of-Adventure-for-Fate-Core
  - text: Good Neighbors
    link: https://www.drivethrurpg.com/product/191782/Good-Neighbors-o-A-World-of-Adventure-for-Fate-Core
  - text: Financiamento Fábio Silva
    link: https://www.catarse.me/mundosfate
  - text: Uprising
    link: https://www.evilhat.com/home/uprising/
  - text: Gods and Monsters
    link: http://www.drivethrurpg.com/product/150889/Gods-and-Monsters--A-World-of-Adventure-for-Fate-Core
  - text: O Defensor
    link: https://en.wikipedia.org/wiki/Forest_Warrior
  - text: "Gasparzinho: Como Tudo Começou"
    link: https://en.wikipedia.org/wiki/Casper:_A_Spirited_Beginning
  - text: Crônicas de Nárnia
    link: https://pt.wikipedia.org/wiki/The_Chronicles_of_Narnia
  - text: O Cavalo e Seu Menino
    link: https://pt.wikipedia.org/wiki/The_Horse_and_his_Boy
  - text: The Magicians
    link: https://en.wikipedia.org/wiki/The_Magicians_(U.S._TV_series)
  - text: Atomic Robo
    link: http://www.evilhat.com/home/atomic-robo/
  - text: Guia do Mochileiro das Galáxia
    link: https://pt.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy
  - text: Fundação
    link: https://pt.wikipedia.org/wiki/Série_da_Fundação
  - text: Hook - A Volta do Capitão Gancho
    link: https://pt.wikipedia.org/wiki/Hook_(filme)
  - text: O Retorno de Mary Poppins
    link: https://pt.wikipedia.org/wiki/Mary_Poppins_Returns
  - text: Um Jogo de Você
    link: http://www.universohq.com/reviews/sandman-um-jogo-de-voce/
  - text: Fábulas
    link: https://pt.wikipedia.org/wiki/Fábulas_(revista_em_quadrinhos)
  - text: Wicked
    link: https://pt.wikipedia.org/wiki/Wicked_(musical)
  - text: Ghost Planets
    link: http://www.drivethrurpg.com/product/198258/Ghost-Planets-o-A-World-of-Adventure-for-Fate-Core
episode: 63
---

E os Fate Masters estão de volta para 2019! E o estão em grande estilo!

Com os motores prontos, o Velho Lich Rafael Meyer, o Mr. Mickey Fábio Costa e o Cicerone Luiz Cavalheiro retornam a todo vapor, fazendo não um, mas DOIS Fate Masters Analisam ao mesmo tempo: Nest e Boa Vizinhança, os módulos que junto com [Mestres de Umdaar](http://fatemasters.gitlab.io/podcast/FateMasters21-AnalisaMastersOfUmdaar/), formaram os primeiros Mundos de Aventuras financiados no Brasil!

Falaremos sobre regras interessantes , como a forma de criação de Personagens e a Árvore de Reação do Boa Vizinhança e os Talismãs de Nest, e sobre como infelizmente (ou não) ambos os jogos são atuais e como eles foram muito legais de trazerem juntos com Mestres de Umdaar, já que ele é muito atual!

E fica de Presente os dois Talismãs citados pelo Mr. Mickey no podcast: o _Robe do Mestre das Armas_ e o _Lenço da Dama Ladra_

### O Lenço da Dama-Ladra

Um pedaço de pano muito fino que muda de cor e padrões de estampa de acordo com o humor do seu dono, o Lenço da Dama-Ladra é **_O Lenço que pode enganar os sentidos._** Esse Lenço pode ser usado para realizar muitos truques de ladinagem e ocultação nas sombras.

+  O usuário do mesmo recebe +2 em todas as ações de _Roubo_, e mesmo se falhar poderá _Ser bem sucedido a um custo menor_
+  Uma vez por cena, gastando um Ponto de Destino, escolha uma Perícia. Durante a mesma, desde que você seja capaz de explicar o porquê, você poderá utilizar _Roubo_ no lugar da Perícia escolhida em qualquer ação, à exceção de _Atacar_. Uma vez feita tal escolha, ela não pode ser mudada durante a cena em questão.


### O Robe do Mestre das Armas

Um robe feito de um tecido leve, ele pode parecer um simples robe de monge para um olho não treinado, mas na realidade ele é **_o Robe que Carrega em Si Todas as Armas_**. Disputado por vários Heróis de Nest durante todo o tempo, o _Robe do Mestre das Armas_ é um robe que é valorizado pelo seu efeito de fornecer àquele que o veste _qualquer arma necessária_ enquanto este o vestir.

+ Você recebe +2 em todas as ações de _Lutar_, e mesmo quando falhar  você poderá ser _bem-sucedido a um custo menor_. 
+ Uma vez por cena, faça um rolamento de _Criar Vantagem_ por _Lutar_. Se bem sucedido, você pode colocar um Aspecto _Fraqueza contra Arma_ em um alvo, com uma Invocação Gratuita. Você pode usar esse Aspecto enquanto estiver usando o Robe. Mesmo que falhe no teste, você ainda recebe um Impulso _Ponto Fraco_, como se tivesse empatado no teste.


Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar  na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, pela [página do Fate Masters no Facebook][fb-page] e agora pelo [servidor do Movimento Fate Brasil no Discord][fb-discord]

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            | 5         |
| **2**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         | 4.83      |
| **4**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         | 4,78      |
| **5**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          | 4,73      |
| **6**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          | 4,68      |
|             | [Nest][fma-nest]                                      | 4,8            | 4,5            | 4,75         | 4,68      |
| **9**       | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          | 4,58      |
| **10**      | [Boa Vizinhança/Good Neighbors][fma-nest]             | 4,7            | 4,25           | 4,75         | 4,56      |
| **11**      | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         | 4,5       |
| **12**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            | 4,33      |
| **13**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         | 4,25      |
| **14**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         | 3,92      |
| **15**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         | 3,5       |
| **16**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         | 4         |
|             | [_Horror Toolkit_][fm-HorrorToolkit]                  | 4,5            | 4,5            | 2            | 3,6       |


[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url 2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url 2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url 2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url 2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url 2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url 2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url 2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url 2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url 2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-mechavskaiju]:  {% post_url 2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url 2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url 2017-11-17-FateMasters35-AnalisaDaringComics %}
[fma-wearingthecape]: {% post_url 2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url 2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url 2018-05-23-FateMasters41-AnalisaSecretsOfCats %}
[fm-HorrorToolkit]: {% post_url 2018-08-06-FateMasters42-AnalisaHorrorToolkit %}
[fma-nest]: {% post_url 2019-03-26-FateMasters45-AnalisaNestBoaVizinhanca %}

[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters

