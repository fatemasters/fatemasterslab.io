---
title: "Fate Masters Episódio 41 - Fate Masters Analisa: The Secrets of Cats"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2018-05-23 16:20:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate-Masters-Analisa
 - The-Secrets-Of-Cats
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
soundtrack:
- music: Tubular Bells (Tema de _O Exorcista_)
  artist: Mike Oldfield
  link: https://www.youtube.com/watch?v=33nyRB4PY14
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
podcast_time: "74min"
itunes:
  duration: 01:14:51
audios:
 - OGG: https://archive.org/download/FateMasters41AnalisaSecretsOfCats/FateMasters41-AnalisaSecretsOfCats.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters41AnalisaSecretsOfCats/FateMasters41-AnalisaSecretsOfCats.mp3
     size: 71380992 
iaplayer: FateMasters41AnalisaSecretsOfCats
related_links:
- text: Secrets of the Cats
  link: http://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats--A-World-of-Adventure-for-Fate-Core
- text: Secrets of Cats - Animals & Threats
  link: http://www.drivethrurpg.com/product/147773/The-Secrets-of-Cats-Animals--Threats
- text: Secrets of Cats - Feline Magic
  link: http://www.drivethrurpg.com/product/194834/The-Secrets-of-Cats-Feline-Magic
- text: _Old Possum's Book Of Practical Cats_ - o Conto que inspirou _Secrets of Cats_, por T. S. Elliot
  link: https://gutenberg.ca/ebooks/eliotts-practicalcats/eliotts-practicalcats-01-h.html
- text: Os Gatos (_Old Possum's Book Of Practical Cats_ em Português)
  link: https://www.companhiadasletras.com.br/detalhe.php?codigo=40553
- text: _Chopstick_
  link: http://www.flyingape.com.br/chopstick/
- text: Buffy, Vampire Slayer
  link: https://en.wikipedia.org/wiki/Buffy_the_Vampire_Slayer
- text: Kult
  link: https://en.wikipedia.org/wiki/Kult_(role-playing_game)
- text: Tico e Teco e os Defensores da Lei
  link: https://pt.wikipedia.org/wiki/Chip_%27n_Dale_Rescue_Rangers
events:
- time: "00:00:10"
  text: Introdução 
- time: "00:01:37"
  text: Apresentando o Secrets of Cats
- time: "00:06:46"
  text: Sobre o cenário de The Secrets of Cats
- time: "00:13:59"
  text: Sobre os gatos e demais animais em Secrets of Cats e as formas de magia utilizadas por animais sencientes
- time: "00:23:21"
  text: Começando a parte de regras falando dos Aspectos, incluindo os vários nomes e o Nome Verdadeiro e Perícias novas e mudadas, além de alguns detalhes sobre as Perícias mágicas
- time: "00:37:02"
  text: Sobre profecias e outras formas de magia pelos Gatos
- time: "00:39:46"
  text: Sobre os suplementos de Secrets of Cats, _Feline Magic_ e _Animals & Threats_, incluindo regras de Magia Proibida, seu uso e redenção e os animais adicionais de _Animals & Threats_
- time: "00:48:25"
  text: Sobre a aventura pronta incluída em _Secrets of Cats_, _Black Silver_
- time: "00:52:09"
  text: Sobre as ameaças de _Secrets of Cats_
- time: "00:54:40"
  text: Aproveitamento de ideias
- time: "01:00:22"
  text: Notas e Considerações
- time: "01:12:23"
  text: Encerramento
episode: 59
---

Temos mais um Fate Masters Analisa.

E dessa vez, os Fate Masters caminham por Becos escuros e estranhos e procuram os Parlamentos dos Gatos que lutam contra o sobrenatural e o misterioso, protegendo o Fardo humano. O Velho Lich Rafael, o Mr. Mickey Fábio e o Cicerone Luiz visitam _The Secrets of Cats_, um dos mais antigos e apreciados suplementos dos _Worlds of Adventure_, falando sobre o funcionamento do mesmo, regras de magia, corrupção e magia proibida, Nome Verdadeiro, outros animais além de gatos, além de escalas de tempo e tamanho e sobre o que é ser um gato, além de falarmos de T. S. Elliot.

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            | 5         |
| **2**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         | 4.83      |
| **4**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         | 4,78      |
| **5**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          | 4,73      |
| **6**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          | 4,68      |
| **8**       | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          | 4,58      |
| **9**       | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         | 4,5       |
| **10**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            | 4,33      |
| **11**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         | 4,25      |
| **12**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         | 3,92      |
| **13**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         | 3,5       |
| **14**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         | 4         |

Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar na [comunidade do Google+ do Fate Masters][gplus], na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, e pela [nova página do Fate Masters no Facebook][fb-page]

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url 2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url 2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url 2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url 2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url 2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url 2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url 2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url 2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url 2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-mechavskaiju]:  {% post_url 2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url 2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url 2017-11-17-FateMasters35-AnalisaDaringComics %}
[fma-wearingthecape]: {% post_url 2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url 2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url 2018-05-23-FateMasters41-AnalisaSecretsOfCats %}

[fb-page]: https://www.facebook.com/fatemasterspodcast/

