---
title: Lista completa dos World of Adventure
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2024-10-07 08:30:00 -0300
tags:
 - Fate
 - Fate-Basico
 - Fate-Condensado
 - Fate-Acelerado
 - Fichas
 - Fichas-Editáveis
 - Solar
 - IndieVisivel
---

Olá a todos!

Depois do nosso retorno do nosso hiato, colocamos a lista completa dos _World of Adventure_ no shownotes do episódio. Então, agora incluímos a lista em um local a parte

A lista inclui o link para acesso a cada World of Adventure e, para os revisados, link para o episódio do Fate Masters onde os Analisamos

Esperamos que seja útil.

Sem maiores delongas, a lista dos World of Aventure

{% include wod.html %}
