---
title: Ficha em PDF editável para Fate Acelerado dos Fate Masters
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-08-04 12:00:00 -0300
tags:
 - Fate
 - Fate-Acelerado
---

E temos mais uma novidade pelos _Fate Masters_!  

Visando agregar conteúdo e facilidade para a vida de nossos queridos ouvintes, os _Fate Masters_ criaram uma ficha em PDF editável para Fate Acelerado! [Baixe aqui o arquivo PDF editável](/assets/fichaseditaveis/faeeditavel_fatemasters.pdf), distribua para seus amigos, crie seus personagens e aproveitem muito, pois **quanto mais FATE, melhor!**  

Observação: a ficha usa as fontes [Montserrat](/assets/fonts/Montserrat-Regular.ttf), [Montserrat Black](/assets/fonts/Montserrat-Black.ttf) e [Source Sans Pro](/assets/fonts/SourceSansPro-Regular.ttf). Instale-as em seu computador antes de usar a ficha.

Em breve disponibilizaremos uma ficha em PDF editável para Fate Básico. Aguardem!

Créditos pelo trabalho:

+ ___Design:___ Maína "Palomita" e Lu "Cicerone" Cavalheiro;  
+ ___Vetoração e criação do PDF:___ Lu "Cicerone" Cavalheiro.
