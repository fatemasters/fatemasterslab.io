---
title: Ficha em PDF editável para Fate Básico dos Fate Masters
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-09-05 12:00:00 -0300
tags:
 - Fate
 - Fate-Basico
---

E temos mais uma novidade pelos _Fate Masters_!  

Na esteira da ficha em PDF editável para Fate Acelerado ([disponível neste link](/assets/fichaseditaveis/faeeditavel_fatemasters.pdf)), os _Fate Masters_ disponibilizam agora a ficha em PDF editável para Fate Básico! [Baixe aqui o arquivo PDF editável](/assets/fichaseditaveis/basicoeditavel_fatemasters.pdf), distribua para seus amigos, crie seus personagens e aproveitem muito, pois **quanto mais FATE, melhor!**  

Observação: a ficha usa as fontes [Montserrat](/assets/fonts/Montserrat-Regular.ttf), [Montserrat Black](/assets/fonts/Montserrat-Black.ttf) e [Source Sans Pro](/assets/fonts/SourceSansPro-Regular.ttf). Instale-as em seu computador antes de usar a ficha.

Créditos pelo trabalho:

+ ___Design:___ Maína "Palomita" e Lu "Cicerone" Cavalheiro;  
+ ___Vetoração e criação do PDF:___ Lu "Cicerone" Cavalheiro.
