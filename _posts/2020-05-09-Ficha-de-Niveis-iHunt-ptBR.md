---
title: Ficha de Níveis do \#iHunt em português!
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-05-09 12:00:00 -0300
tags:
 - Fate
 - Fate-Basico
 - iHunt
---

E temos mais uma novidade pelos _Fate Masters_!  

Visando agregar conteúdo e facilidade para a vida de nossos queridos ouvintes, o _Cicerone_ traduziu para todos nós a _Ficha de Níveis_ (Levels Sheets) de \#iHunt! [Baixe aqui o arquivo em PDF](/assets/traducao_levels_sheet_ihunt.pdf), divulgue, distribua para seus amigos, e faça bom uso dessa ótima ferramenta para condução de mesas seguras!

Créditos pelo trabalho:

+ ___Tradução e tipografia___: Lu _Cicerone_ Cavalheiro

A _Ficha de Níveis_ é disponibilizada por Machine Ages Productions sob licença [Creative Commons Atribuição CompartilhaIgual 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR). A tradução da _Ficha de Níveis_ para português do Brasil é disponibilizada por Lu _Cicerone_ Cavalheiro sob licença [Creative Commons Atribuição CompartilhaIgual 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR).

