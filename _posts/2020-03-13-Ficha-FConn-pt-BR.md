---
title: "Fichas para _Fate Condensed_ em português!"
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-03-13 00:00:00 -0300
tags:
 - Fate
 - Fate-Condensed
---

No rastro do lançamento do [_Fate Condensed_](https://www.evilhat.com/home/fate-condensed/), sobre o qual falaremos em breve, temos uma novidade para todos vocês, caros leitores, que acompanham o podcast!  

Um dos membros do [servidor do Discord do _Movimento Fate Brasil_](https://discord.gg/FnqYJv6), [_yukiartic - Estevan_](https://discordapp.com/channels/@me/688059441714954267), traduziu para português e disponibilizou as fichas para _Fate Condensed_ em pdf, em versões para impressão e preenchivel!   

Confiram o trabalho fantástico dele: [baixe aqui](/assets/Ficha_FConn_pt-BR/Fate-Condensed-Ficha_de_Personagem_PT_BR.pdf) a ficha para impressão, e [baixe aqui](/assets/Ficha_FConn_pt-BR/Fate-Condensed-Ficha_de_Personagem-Preenchivel_PT_BR.pdf) a versão editável.  
