---
title:  "Fate Masters Episódio 58 - Analisa Fate Accessibilty Toolkit"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-06-23 08:26:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate Masters Analisa
 - Fate Condensed
 - Fate Condensado
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "160min"
itunes:
   duration: "02:40:58"
audios:
  - OGG: https://archive.org/download/fate-masters-58-analisa-fact/FateMasters58-AnalisaFAcT.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-58-analisa-fact/FateMasters58-AnalisaFAcT.mp3
      size: 180049920
iaplayer: fate-masters-58-analisa-fact
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:47"
    text: Apresentando o Fate Accessibility Toolkit, inclusive o quão seminal ele é para o RPG como um todo
  - time: "00:09:30"
    text: A História por trás do Fate Accessibility Toolkit e sobre como ele foi escrito por uma autora com tais condições de acessibilidade
  - time: "00:28:29"
    text: Sobre as condições listadas e como eles foram escritos por pessoas com tais condições e sobre uma "normatividade social"
  - time: "00:36:47"
    text: _FAÇA A LIÇÃO DE CASA!_ Estude e aprenda sobre condições, ouça as pessoas que tem tais condições, evitando os estereótipos!
  - time: "00:44:03"
    text: Sobre Linguagem e Identidade, e sobre evitar a narrativa de superação, sobre como os Aspectos permitem traduzir de maneira respeitosa as excelências e dificuldades de cada condição, e sobre NUNCA DIAGNOSTICAR EM MESA!!!!!
  - time: "01:03:56"
    text: Menos Coringa, mais Hannibal Lector! Evite os Clichês, e lembre-se que, em especial nas condições mentais, condições não são binárias (mesmo nos jogos de humor)
  - time: "01:07:05"
    text: Como é narrar para pessoas com Condições de Acessibilidade e dicas sobre a lição de casa a ser feita varia para cada grupo
  - time: "01:25:24"
    text: Comentários sobre as Condições de Acessibilidade mais comuns indicadas no Fate Acessibility Toolkit
  - time: "01:41:35"
    text: Cobre Condições Mentais e sobre o tanto de estereótipos e clichês envolvidos
  - time: "01:49:32"
    text: Sobre as regras do Fate aplicadas a personagens com Condições, incluindo mudanças de diagramação com Fontes Serifadas
  - time: "02:12:28"
    text: Rankeamento
  - time: "02:24:26"
    text: Considerações Finais
  - time: "02:35:26"
    text: Mensagens Finais, incluindo um pedido especial aos jogadores com condições especiais
related_links:
  - text: Fate Accessibility Toolkit
    link: https://evilhat.itch.io/fate-accessibility-toolkit-prototype-edition
  - text: Review do Fate Accessibility Toolkit pelo Mister Mickey
    link: https://www.dungeongeek21.com/resenha-fate-accessibility-toolkit/
  - text: Fate Space Toolkit
    link: https://evilhat.itch.io/fate-space-toolkit
  - text: GURPS Viagem no Tempo
    link: https://www.ludopedia.com.br/jogo/gurps-time-travel
  - text: Consent in Gaming
    link: https://www.montecookgames.com/games/
  - text: "Artigo sobre o acidente de Alessandro Zanardi em Lausitzring (TW: Gráfico, Partes de Corpo, Sangue)"
    link: https://www.grandepremio.com.br/indy/noticias/na-garagem-zanardi-sofre-amputacao-das-duas-pernas-apos-acidente-na-alemanha
  - text: "Imagem do acidente de Alessandro Zanardi em Lausitzring (TW: Gráfico, Partes de Corpo, Sangue)"
    link: "http://s.glbimg.com/es/ge/f/original/2016/09/14/gettyimages-2036149_Nin3m0N.jpg"
  - text: "Artigo sobre Alessandro Zanardi nas Paraolimpíadas do Rio (TW: Gráfico, Partes de Corpo, Sangue)"
    link: http://ge.globo.com/paralimpiadas/noticia/2016/09/exatos-15-anos-apos-brutal-acidente-zanardi-conquista-2-medalha-no-rio.html
  - text: Artigo sobre Alessandro Zanardi pós-segundo acidente
    link: https://www.grandepremio.com.br/outras/noticias/10-meses-acidente-filho-revela-alex-zanardi-esta-consciente-ha-algum-tempo/
  - text: "Aaron ‘Wheelz’ Fotheringham - o Cadeirante Radical do Nitro Circus"
    link: https://nitrocircus.com/the-kicker/featured-news/2016/08/12/aaron-wheelz-fotheringham-is-a-flipping-wheelchair-innovator/
  - text: "Aaron ‘Wheelz’ Fotheringham descendo a mega-rampa em uma cadeira de rodas"
    link: https://www.youtube.com/watch?v=UQuBzShOFew
  - text: "Modelos 3D para dados de RPG"
    link: https://www.dotsrpg.org/3d-models
  - text: "Rick Allen, o baterista sem um braço"
    link: "https://en.wikipedia.org/wiki/Rick_Allen_(drummer)#Crash_and_recovery"
  - text: "Demolidor"
    link: "https://en.wikipedia.org/wiki/Daredevil_(Marvel_Comics_character)"
  - text: "Oráculo"
    link: "https://en.wikipedia.org/wiki/Barbara_Gordon#Representation_for_the_disabled"
  - text: "Toph Beifong"
    link: "https://en.wikipedia.org/wiki/Toph_Beifong"
  - text: "Cem Olho"
    link: "https://marco-polo.fandom.com/wiki/Hundred_Eyes"
  - text: "Giordi LaForge"
    link: "https://memory-alpha.fandom.com/wiki/Geordi_La_Forge"
  - text: "Tyrion Lannister"
    link: "https://gameofthrones.fandom.com/pt-br/wiki/Tyrion_Lannister"
  - text: "Quasímodo - Corcunda de Notre Dame"
    link: "https://en.wikipedia.org/wiki/Quasimodo"
  - text: "Arkham Braço Metálico"
    link: "https://tormenta.fandom.com/pt/wiki/Arkam_Bra%C3%A7o_Met%C3%A1lico"
  - text: "Lipedema"
    link: "https://www.amato.com.br/lipedema-o-que-e/"
  - text: "Fundação Dorina Nowill"
    link: "https://fundacaodorina.org.br/"
  - text: "ADEVA"
    link: "https://www.adeva.org.br/"
  - text: "Gráfica Braille da ADEVA"
    link: https://www.adeva.org.br/servicos/graficabraille.php
  - text: "Instituto Benjamin Constant"
    link: http://www.ibc.gov.br/
  - text: "Zatoichi"
    link: https://en.wikipedia.org/wiki/Zatoichi
  - text: Tachibana Ukyo
    link: https://snk.fandom.com/wiki/Ukyo_Tachibana
  - text: "Changeling: O Sonhar"
    link: "https://whitewolf.fandom.com/wiki/Changeling:_The_Dreaming"
  - text: "Dreams and Nightmares"
    link: https://whitewolf.fandom.com/wiki/Dreams_and_Nightmares
  - text: "Immortal Eyes"
    link: "https://whitewolf.fandom.com/wiki/Immortal_Eyes:_The_Toybox"
  - text: "Movimento Antimanicomial"
    link: https://pt.wikipedia.org/wiki/Movimento_antimanicomial
  - text: "Lista de Personagens com Condições Atípicas"
    link: https://en.wikipedia.org/wiki/List_of_fictional_characters_with_disabilities
  - text: "Delírio Místico"
    link: https://amenteemaravilhosa.com.br/delirio-mistico-o-que-e/
  - text: "nVDA"
    link: https://www.nvaccess.org/download/
  - text: "Leitor de Tela Orca para Linux"
    link: https://help.gnome.org/users/orca/stable/introduction.html.pt_BR
  - text: "JAWS"
    link: https://pt.wikipedia.org/wiki/JAWS
  - text: "DOSVox"
    link: http://intervox.nce.ufrj.br/dosvox/
  - text: "Fonte OpenDyslexic"
    link: https://github.com/antijingoist/opendyslexic
  - text: "Sobre Fontes Serifadas e Nãos-Serifadas"
    link: https://www.tailorbrands.com/pt-br/blog/tipos-de-fontes
  - text: "Infográfico sobre Serifa versus Sem Serifa"
    link: https://tutano.trampos.co/12742-infografico-tipografia-serifa/
  - text: "PDFtoText"
    link: https://en.wikipedia.org/wiki/Pdftotext
  - text: "Sobre Fonte Serifada"
    link: https://www.futuraexpress.com.br/blog/fontes-com-serifa/
  - text: "Acromatopsia"
    link: https://www.tuasaude.com/acromatopsia/
  - text: "Dislexia"
    link: https://www.einstein.br/guia-doencas-sintomas/info/#124
  - text: Dragon Dice - aplicativo para rolar dados e administrar jogos online
    link: https://play.google.com/store/apps/details?id=rpg.mundo.mundorpg_dadosparawhatsapp&hl=pt_BR
  - text: DnDice 
    link: https://play.google.com/store/apps/details?id=com.christian.bar.dndice&hl=pt_BR
  - text: Breakfast Cult
    link: https://ettin.itch.io/breakfast-cult
  - text: Amigo Dragão
    link: https://coisinhaverde.com.br/jogos/portfolio/amigo-dragao/
  - text: Oscar Pistorius
    link: https://en.wikipedia.org/wiki/Oscar_Pistorius
  - text: Unforgettable
    link: https://en.wikipedia.org/wiki/Unforgettable_(American_TV_series)
  - text: Sans-Serif
    link: https://en.wikipedia.org/wiki/Sans-serif
  - text: Pixie e Brutus
    link: https://www.portaldoanimal.org/pixie-e-brutus-a-historia-de-uma-maravilhosa-amizade-atraves-de-quadrinhos-hilariamente-fofos/
  - text: Fullmetal Alchemist
    link: https://en.wikipedia.org/wiki/Fullmetal_Alchemist
  - text: Carta-X
    link: http://fatesrdbrasil.gitlab.io/rpg/2021/05/24/CartaX/
  - text: Script Change
    link: https://briebeau.com/thoughty/script-change/
  - text: Don Bluth
    link: https://en.wikipedia.org/wiki/Don_Bluth
  - text: Um Conto Americano (Fievel)
    link: https://en.wikipedia.org/wiki/An_American_Tail
  - text: Em Busca do Vale Encantado
    link: https://en.wikipedia.org/wiki/The_Land_Before_Time_(film)
  - text: Bernardo e Bianca
    link: https://en.wikipedia.org/wiki/The_Rescuers
  - text: Death Note
    link: https://en.wikipedia.org/wiki/Death_Note
  - text: Fairy Tail
    link: https://en.wikipedia.org/wiki/Fairy_Tail
  - text: Thirsty Sword Lesbians
    link: https://evilhat.itch.io/thirsty-sword-lesbians
episode_count: 78
episode: 58
---

E estamos de volta... 

Para falar de um dos livros mais necessários já publicados para Fate, não apenas para o sistema Fate, mas para o RPG como um todo.

Vamos falar do _Fate Accessibility Toolkit_ (FAcT), ou o _Guia para Acessibilidade em Fate_, focado em como tornar personagens com condições atípicas muito mais do que estereótipos e mostrar que condições atípicas podem ser muito mais que mecânicas de horror lovecraftiano ou pontos para aprimorar outras partes.

Falamos sobre o risco dos estereótipos, sobre como um personagem pode, ainda que contando com condições atípicas, ser tão competente, proativo e dramático quanto qualquer outro, sobre os problemas do _"kit do psicopata feliz"_ e da narrativa de superação e sobre como é importante sempre fazer a lição de casa, tanto ao criar personagens com condições atípicas quanto ao aumentar o acesso de jogadores com tais condições ao RPG, que é um hobby que tem possibilidade de ser muito mais inclusivo do que atualmente fazemos.

Esse longo episódio também é regado com exemplos pessoais e informações nossas.

E deixamos o pedido: você que conheça jogadores ou seja jogador de RPG que tenha condições que demandem algum tipo de atenção especial, como baixa visão, surdez ou condições psicológicas e que deseje dividir suas experiências conosco, por favor entre em contato conosco no <fatemasterspodcast@gmail.com> e divida conosco sua história, já que esse debate que está sendo aberto nesse podcast não foi encerrado

E lembrando aquilo que não te contaram sobre a acessibilidade:

+ Nem toda pessoa com alguma deficiência deseja ser curada
+ Nem sempre dá para se curar uma deficiência
+ Binarismo em deficiências é uma ilusão: a maior parte das deficiências não são oito ou oitenta, vindo em escalas.
+ Dispositivos de Acessibilidade são ferramentas, não brinquedos
+ A deficiência não é toda a sua identidade, ainda que possa ser algo importante. Você pode ser um cego judeu, ou uma pessoa de cor cadeirante, ou uma mulher com bipolaridade, ou um homem trans autista... ou qualquer outra coisa. Pessoas com deficiência são pessoas complexas, como qualquer outra.
+ Dor é um conceito relativo: certas dores podem ser simplesmente ignoradas se você as sente o tempo todo
+ Deficiências não são caixinhas: elas variam de pessoa para pessoa. Mesmo pessoas que tenham o mesmo tipo de deficiência podem lidar com ela de formas diferentes.
+ Nem toda deficiência é visível: algumas podem não ser vistas, mas ainda poderem influenciar a vida da pessoa. Pense em pessoas diagnosticadas como autistas.

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | Palomita | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:--------:|-----------|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            |          | 5         |
|             | [#iHunt][fma-ihunt]                                   | 5              | 5              | 5            |          | 5         |
| **3**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            |          | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         |          | 4.83      |
| **5**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         |          | 4,78      |
| **6**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          |          | 4,73      |
| **7**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         |          | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          |          | 4,68      |
|             | [Nest][fma-nest]                                      | 4,8            | 4,5            | 4,75         |          | 4,68      |
| **10**      | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          |          | 4,58      |
| **11**      | [Boa Vizinhança/Good Neighbors][fma-nest]             | 4,7            | 4,25           | 4,75         |          | 4,56      |
| **12**      | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         |          | 4,5       |
| **13**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            |          | 4,33      |
| **14**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         |          | 4,25      |
| **15**      | [Super-Powered Fate][fm-superpoweredfate]             | 4,2            | 3,75           | 3,75         | 4        | 3,92      |
| **16**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         |          | 3,92      |
| **17**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         |          | 3,5       |
| **18**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         |          | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:--------:|-----------|
|             | [_Accessibility Toolkit_][fm-FAcT]                    | 4,8            | 4              | 4            | 3,5      | 4,075          |
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         |          | 4         |
|             | [_Horror Toolkit_][fm-HorrorToolkit]                  | 4,5            | 4,5            | 2            |          | 3,6       |
|             | [Fate Condensado][fm-FateCondesando]                  | ----           | ----           | ----         |          | ---       |


[fm-FateCondesando]: {% post_url podcast/2020-08-17-FateMasters52-FateCondensed %}
[fm-FAcT]: {% post_url podcast/2021-06-22-FateMasters58-AnalisaFAcT %}
[fma-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt %}
[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url podcast/2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url podcast/2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url podcast/2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url podcast/2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url podcast/2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url podcast/2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url podcast/2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url podcast/2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url podcast/2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-superpoweredfate]:  {% post_url podcast/2020-12-14-FateMasters53-SuperPoweredFate %}
[fm-mechavskaiju]:  {% post_url podcast/2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url podcast/2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url podcast/2020-12-14-FateMasters53-SuperPoweredFate %}
[fma-wearingthecape]: {% post_url podcast/2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url podcast/2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url podcast/2018-05-23-FateMasters41-AnalisaSecretsOfCats %}
[fm-HorrorToolkit]: {% post_url podcast/2018-08-06-FateMasters42-AnalisaHorrorToolkit %}
[fma-nest]: {% post_url podcast/2019-03-26-FateMasters45-AnalisaNestBoaVizinhanca %}
[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters
