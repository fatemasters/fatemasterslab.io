---
title:  "Fate Masters Episódio 64 - Exemplo de Sessão Zero usando Uprising: The Dystopian Universe RPG"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2022-04-03 19:00:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Sessão Zero
 - Uprising
explicit: yes
header: no
podcast_comments: true
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "77min"
itunes:
   duration: "01:17:23"
audios:
  - OGG: https://archive.org/download/fate-masters-64-exemplo-sessao-zero-uprising/FateMasters64-ExemploSesaoZeroUprising.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-64-exemplo-sessao-zero-uprising/FateMasters64-ExemploSesaoZeroUprising.mp3
      size: 74182656
iaplayer: fate-masters-64-exemplo-sessao-zero-uprising
soundtrack:
- music: ChipStep
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/chipstep
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Início do Podcast
  - time: "00:03:16"
    text: "Uma introdução sobre Uprising: The Dystopian Universe RPG"
  - time: "00:13:42"
    text: "Comentários rápidos sobre regras interessantes: Condições, Regras de Traição e Segredos"
  - time: "00:20:13"
    text: "Definindo Limites e Véus, e sobre cuidados ao lidar com as coisas"
  - time: "00:31:04"
    text: "Uma passagem rápida sobre os _Playbooks_ disponíveis em Uprising"
  - time: "00:36:38"
    text: "Começando a criação de um _Hacker_ e um _Blueblood_ respondendo as perguntas dos _Playbooks_ para definir os Aspectos dos mesmos"
  - time: "00:49:16"
    text: "Escolha dos _Means_ (Meios, as _\"Abordagens\"_ do mesmo)"
  - time: "00:52:05"
    text: "Escolhendo Recarga e Façanhas"
  - time: "00:56:23"
    text: "Com os personagens criados, hora de criar _La Resistènce_ (A Resistência) e o Governo"
  - time: "01:03:45"
    text: "Criando NPCs, Faces e Locais"
  - time: "01:13:03"
    text: "Comentários Finais sobre a Sessão Zero"
related_links:
  - text: Parte 1 do jogo de \#iHunt dos Fate Masters
    link: http://fatemasters.gitlab.io/podcast/FateMasters61-JogaIhunt/
  - text: IndieVisível (Facebook)
    link: https://www.facebook.com/IndieVisivelPress
  - text: Ficha do Fari do Hacker
    link: /assets/TheHackerVelhoLich.fari.json
  - text: Ficha do Fari do Blueblood
    link: /assets/TheBluebloodMrMickey.fari.json
  - text: Ficha do Fari de _La Resistence_
    link: /assets/FateMastersLaResistance!.fari.json
  - text: Ficha do Fari do Hacker
    link: /assets/FateMasterstheGovernment!.fari.json
  - text: Ficha do Hacker na página
    link: /personagens/Hacker-Uprising/
  - text: Ficha do Blueblood na página
    link: /personagens/Blueblood-Uprising/
  - text: Notas tomadas de Sessão 0
    link: /personagens/NotasSessaoZeroUprising/
  - text: "Uprising - The Dystopian Universe RPG"
    link: https://www.drivethrurpg.com/product/252231/Uprising-The-Dystopian-Universe-RPG
  - text: One Night Revolution
    link: https://www.ludopedia.com.br/jogo/one-night-revolution
  - text: Coup
    link: https://funbox.com.br/jogos/coup/
  - text: The Resistance
    link: https://www.galapagosjogos.com.br/produtos/the-resistance
  - text: Condições no SRD do Fate em Português
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/condicoes/
  - text: Good Neighbors
    link: https://evilhat.itch.io/good-neighbors-a-world-of-adventure-for-fate-core
episode_count: 84
episode: 64
---

E o ano de 2022 se inicia para os Fate Masters (com muito atraso)

E vamos mostrar uma coisa que vocês pedem muito.

O Cicerone Luís Cavalheiro comanda hoje uma Sessão Zero em um dos nossos queridinhos, _Uprising: The Dystopian Universe RPG_, que está para vir para o Brasil nas mãso da IndieVisível Press, a mesma que está trazendo \#iHunt.

Nesse podcast mostraremos como é feito uma Sessão Zero, considerações, coisas que mudam de um jogo Fate Tradicional para Uprising e afins.

Nesse momento, o Mr. Mickey Fábio e o Velho Lich Rafael criam personagens, ajudam a construir _La Resistènce_ e o Governo e criamos NPCs. 

Agora, se esses personagens verão aventuras e a possibilidade de provocar a Revolução que Destruirá os Grilhões desse governo opressor (ou enfrentarão o Expurgo, sendo reduzidos a polpa de barata como eles são), fica a cargo de você ouvinte.

Queremos que vocês também nos contem se desejam ver os Fate Masters se aventurando pelas ruas opressivas em Realidade Aumentada de _Paris Noveau_ pelaa [comunidade do Movimento Fate Brasil][fb-fate], pelo [Discord do Movimento Fate Brasil][discord-fatebrasil], e  pelo email <fatemasterspodcast@gmail.com>. 

Se quiserem ouvir mais sobre _Uprising_, ouça nosso Episódio 39, [onde falamos de maneira muito mais detalhada sobre ele][fm-uprising].

![[A Resposta do Fred para o Cicerone]({{ site.url }}/assets/img/MeansUprising.png)]({{ site.url }}/assets/img/MeansUprising.png)

[fm-uprising]: {% post_url podcast/2018-04-08-FateMasters39-AnalisaUprising  %}
[fb-fate]: https://www.facebook.com/groups/336649349791444
[discord-fatebrasil]: https://discord.gg/zp7GQt9YCG
