---
title:  "Fate Masters Episódio 52 - Analisa Fate Condensado (_Fate Condensed_)"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-08-17 19:50:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate Masters Analisa
 - Fate Condensed
 - Fate Condensado
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "115min"
itunes:
  duration: "01:55:04"
audios:
  - OGG: https://archive.org/download/fate-masters-52-fate-condensado/FateMasters52-FateCondensado.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-52-fate-condensado/FateMasters52-FateCondensado.mp3
      size: 130574336
iaplayer: fate-masters-52-fate-condensado
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:14"
    text: Introdução
  - time: "00:01:43"
    text: Introdução ao tema e histórico por trás do mesmo
  - time: "00:11:02"
    text: "O que exatamente é o Fate Condensado?"
  - time: "00:17:06"
    text: As mudanças mais gerais do Fate Condensado (como na página 3 do Fate Condensado), começando pelas Caixas de Estresse de 1 Ponto
  - time: "00:21:36"
    text: Iniciativa _Popcorn_ e como isso favorece um bom _metagame_ para tornar cenas dramáticas e interessantes
  - time: "00:28:11"
    text: Mudança nos Marcos de Personagem, na Defesa Total e algumas clarificações das regras, em especial com o fim da Oposicação ativa
  - time: "00:36:27"
    text: Separação de _Conhecimentos_ (_Lore_) e _Acadêmico_ (_Academics_)
  - time: "00:39:51"
    text: Modificações e adições de regras no Fate Condensado, como o Efeito Espelho e Esforço Extra como opção na Falha
  - time: "00:44:41"
    text: Sobre as mudanças de regras em geral e em como elas foram escritas
  - time: "00:46:00"
    text: Sobre a _bogus rule_ para abuso do uso de Aspectos, Aspectos de Relacionamento e o fim da _Phase Trio_
  - time: "00:50:42"
    text: Sobre _Bônus Solo_, Imunidades, _Não é a minha Forma Final_, Armaduras de _Minions_, e outras mecânicas para a criação de _Chefes Finais_
  - time: "00:59:43"
    text: Sobre regra de Escala, _O Mapa é uma ameaça_ (VLM - Monstros Gigantes)
  - time: "01:04:32"
    text: Sobre Condições no lugar das Consequências e sobre os Perigos que vieram do _Adversary Toolkit_
  - time: "01:08:00"
    text: Sobre ausências no Fate Condensado
  - time: "01:13:24"
    text: Sobre design e as opções de distribuição de perícias do Fate Condensado, além da preocupação das Ferramentas de Segurança, e das dificuldades relacionadas à Ficha para pessoas com Dislexia e Déficit de Atenção
  - time: "01:32:32"
    text: Sobre o licenciamento do Fate (não importa a variação) e sobre Fate Condensado, Fate Acelerado e Fate Básico serem O. MESMO. JOGO! E os preferidos de cada um dos Fate Masters.
  - time: "01:41:16"
    text: Considerações Finais
  - time: "01:49:53"
    text: Encerramento
related_links:
  - text: Fate Condensed em inglês
    link: https://evilhat.itch.io/fate-condensed
  - text: Espírito do Século
    link: https://jovemnerd.com.br/nerdbunker/espirito-do-seculo-novo-rpg-pulp-da-retropunk-ja-entrou-em-pre-venda/
  - text: \#iHunt
    link: https://machineage.itch.io/ihunt-the-rpg
  - text: Blades in the Dark
    link: https://www.burobrasil.com/produtos/blades-in-the-dark/
  - text: Urban Shadows
    link: https://www.magpiegames.com/urban-shadows/
  - text: "Accidentally Designing Marvel’s Action Order System - Sobre Iniciativa _popcorn_"
    link: http://www.deadlyfredly.com/2012/02/marvel
  - text: Um Homem Fortão Precisando de Ajuda - Cena do Detona Ralph 2 citada pelo Mister Mickey
    link: "https://youtu.be/pSbzVB0r0Vc?t=23"
  - text: SRD do Fate em Português
    link: "https://fatesrdbrasil.gitlab.io"
episode_count: 69
episode: 52
---

E estamos de volta... E hoje com uma novidade que demanou um bom estudo. Um meio do caminho entre o Fate Básico e o Fate Acelerado. 

Nesse Fate Masters, o Velho Lich Rafael, o Mister Mickey Fábio, o Cicerone Luís Cavalheiro e a Palomita Maína comenta sobre um resultado de uma meta expandida de um produto que foi resultado de uma meta expandida do _Fate of Cthulhu_: o Fate Condensed, o Fate Condensado.

Falaremos sobre as mudanças, adições e remoções na regra do Fate 4 (o Fate Básico e Acelerado) em um manual mais enxuto que o Básico, mas ainda assim mais "completo e complexo" que o Fate Acelerado, um produto que ficou bem interessante para mestres que queiram começar no Fate mas, ao mesmo tempo que não queiram se envolver nas complexidades que o Fate Básico inclui, mas ao mesmo tempo quer algo mais complexo que o Fate Acelerado.

Entre novas opções e clarificação do funcionamento das regras, o Fate Condensado é bastante inovador, mesmo que não traga muita coisa nova é uma evolução de jogo bastante interessante. E entre coisas como Caixa de Estresse de 1 ponto, Iniciativa _Popcorn_, _Bogus Rule_ nos Aspectos, e _Bônus Solo_ para seus _boss finais_ ao estilo Toguro, Sephirot, Beerus, John Rambo, os Fate Masters vão dissecando as novidades e clarificações em coisas como a questão do _Efeito Espelho_ da ação de Defesa. 


A GIF de Pirâmide do Estevan:

![[Como funciona a pirâmide - GIF pelo Estevan, da Comunidade Movimento Fate Brasil](/images/APRIMORANDO_PERICIAS_PT_BR.gif)](/images/APRIMORANDO_PERICIAS_PT_BR.gif)

E o [Chamado do Tchutchuco](http://infameludico.blogspot.com/2019/06/all-you-need-is-lovecraft.html):

![[All you need lovecraft](http://infameludico.blogspot.com/2019/06/all-you-need-is-lovecraft.html)](/images/ChamadoTchutchuco.jpg)

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            | 5         |
|             | [#iHunt][fma-ihunt] | 5              | 5              | 5            | 5         |
| **3**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         | 4.83      |
| **5**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         | 4,78      |
| **6**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          | 4,73      |
| **7**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          | 4,68      |
|             | [Nest][fma-nest]                                      | 4,8            | 4,5            | 4,75         | 4,68      |
| **10**       | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          | 4,58      |
| **11**      | [Boa Vizinhança/Good Neighbors][fma-nest]             | 4,7            | 4,25           | 4,75         | 4,56      |
| **12**      | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         | 4,5       |
| **13**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            | 4,33      |
| **14**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         | 4,25      |
| **15**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         | 3,92      |
| **16**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         | 3,5       |
| **17**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         | 4         |
|             | [_Horror Toolkit_][fm-HorrorToolkit]                  | 4,5            | 4,5            | 2            | 3,6       |
|             | [Fate Condensado][fm-FateCondesando]                  | ----            | ----            | ----            | ---       |


[fm-FateCondesando]: {% post_url podcast/2020-08-17-FateMasters52-FateCondensed %}
[fma-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt %}
[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url podcast/2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url podcast/2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url podcast/2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url podcast/2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url podcast/2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url podcast/2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url podcast/2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url podcast/2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url podcast/2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-mechavskaiju]:  {% post_url podcast/2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url podcast/2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url podcast/2017-11-17-FateMasters35-AnalisaDaringComics %}
[fma-wearingthecape]: {% post_url podcast/2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url podcast/2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url podcast/2018-05-23-FateMasters41-AnalisaSecretsOfCats %}
[fm-HorrorToolkit]: {% post_url podcast/2018-08-06-FateMasters42-AnalisaHorrorToolkit %}
[fma-nest]: {% post_url podcast/2019-03-26-FateMasters45-AnalisaNestBoaVizinhanca %}

[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters
