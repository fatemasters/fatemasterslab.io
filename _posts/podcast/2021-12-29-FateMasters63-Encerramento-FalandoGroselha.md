---
title:  "Fate Masters Episódio 63 - Encerramento de 2021 (ou 'Os Fate Masters falam Groselha')"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-12-29 20:00:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Encerramento
explicit: yes
header: no
podcast_comments: true
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "130min"
itunes:
   duration: "01:50:26"
audios:
  - OGG: https://archive.org/download/fate-masters-63-encerramento-2021-falando-groselha/FateMasters63-Encerramento2021-FalandoGroselha.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-63-encerramento-2021-falando-groselha/FateMasters63-Encerramento2021-FalandoGroselha.mp3
      size: 147768576
iaplayer: fate-masters-63-encerramento-2021-falando-groselha
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Início do Podcast (e da groselha)
episode_count: 83
episode: 63
---

E o ano se encerra em 2021 para os Fate Masters, e decidimos fazer uma coisa que vocês vêem a gente fazer muito pouco.

Falar groselha.

Muita groselha.

O puro suco da groselha!

Enquanto fazemos o balanço do ano tanto para o podcast quanto para cada um de nós, também comentamos nossas esperanças e desejos para 2022, coisas que mantiveram nossa sanidade e que foram boas em 2021.

E falamos groselha!

Quase totalmente sem freio!

Falamos de Política, Cybercops, TikTok, Vampiro v5 (o Irmão do Meio), cosplay, literatura, planos de governo, Anita Baker, The Expanse e Way of the Househusband...

E muita groselha!

Eu já falei que tem muita groselha nesse podcast?

Mas queremos saber o que vocês acharam dessa groselha toda! Pouquíssima edição e um papo descontraído em uma pauta livre e temos mais um ano se encerrando para os Fate Masters.

E queremos que vocês também falem muita groselha com os Fate Masters na [comunidade do Movimento Fate Brasil][fb-fate], pelo [Discord do Movimento Fate Brasil][discord-fatebrasil], e,  pelo email <fatemasterspodcast@gmail.com>. 

E nos vemos em 2022!



[fm-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt  %}
[fm-indievisivel]: {% post_url podcast/2021-10-09-FateMasters60-ApresentaLeonardoHimura-IndieVisivel %}
[carina]: {% post_url Personagens/2021-11-20-CarinaSouza %}
[valdeci]: {% post_url Personagens/2021-11-20-ValdeciAndrade %}
[diego]: {% post_url Personagens/2021-11-20-DiegoDaSilva %}
[fb-fate]: https://www.facebook.com/groups/336649349791444
[fb-ihuntbrasil]: https://www.facebook.com/groups/337882581467185
[fb-ihunt]: https://www.facebook.com/iHuntSeries
[catarse-ihunt]: https://catarse.me/ihunt
[discord-fatebrasil]: https://discord.gg/zp7GQt9YCG
[fm-jogaihunt]: http://fatemasters.gitlab.io/podcast/FateMasters61-JogaIhunt/
[professor]: http://fatemasters.gitlab.io/personagens/JonasBeltrao/
[hacktheplanet]: https://www.youtube.com/watch?v=W5fM6WpC_nE&t=60s
