---
title:  "Fate Masters Episódio 50 - Utilidade Pùblica - Jogos Online"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-03-23 21:05:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Utilidade Pública
 - Coronavirus
 - COVID-19
 - Jogos Online
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Maína "Palomita" Paloma de Lima
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "100min"
itunes:
  duration: "01:40:00"
audios:
  - OGG: https://archive.org/download/fatemasters50jogosonline/FateMasters50-JogosOnline.ogg
  - MP3:
    - file: https://archive.org/download/fatemasters50jogosonline/FateMasters50-JogosOnline.mp3
      size: 121810944
iaplayer: fatemasters50jogosonline
soundtrack:
- music: "The Operation (Trauma Center: Under The Knife OST)"
  artist: "Kenichi Tsuchiya, Shoji Meguro, Kenichi Kikkawa"
  link: https://www.youtube.com/watch?v=0lXS5CMkBUc&t=24s
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:47"
    text: Contexto sobre o COVID-19
  - time: "00:07:30"
    text: Um pouco de história sobre o jogo _online_ e seus métodos
  - time: "00:26:30"
    text: A experiência de jogo _online_ dos Fate Masters, vantagens e desvantagens
  - time: "01:02:33"
    text: Algumas ferramentas para jogo _online_ - Discord, Roll20, Telegram e DnDice
  - time: "01:23:17"
    text: Considerações Finais
related_links:
  - text: Página com Orientações do Ministério da Saúde sobre o COVID 19 (Coronavirus)
    link: https://coronavirus.saude.gov.br/
  - text: Página com Orientações da OMS sobre o COVID 19 (Coronavirus)
    link: https://sbpt.org.br/portal/covid-19-oms/
  - text: Página do Evento da Dungeon Geek online
    link: https://www.facebook.com/events/2808650775871437/
  - text: Podcast do Braincast sobre o Coronavirus
    link: https://www.b9.com.br/shows/braincast/braincast-349-coronavirus-como-sobreviver-ao-home-office-sem-pirar/
  - text: Podcast do Mamilos sobre o Coronavirus
    link: https://www.b9.com.br/shows/mamilos/mamilos-243-sobrevivendo-ao-coronavirus/
  - text: Podcast do Mupoca sobre o Coronavirus
    link: https://www.b9.com.br/shows/mupoca/mupoca-108-o-que-e-mais-rapido-o-coronavirus-ou-o-panico/
  - text: Podcast do Xadrez Verbal sobre o Coronavirus
    link: https://xadrezverbal.com/2020/03/17/xadrez-verbal-e-atila-iamarino-especial-coronavirus-01/
  - text: Podcast do Revolushow sobre o Coronavirus
    link: https://revolushow.com/67-coronavirus-live/
  - text: Exemplo de Jogo Online em MP3 feito pelo Cicerone Luiz
    link: https://archive.org/download/20200322mastersofumdaarservidorferrugemrizomacultural/2020-03-22%20-%20Masters%20of%20Umdaar%20-%20Servidor%20Ferrugem%20Rizoma%20Cultural.mp3
  - text: Vários exemplos de Jogos Online gravados pelo Mr. Mickey
    link: https://mesaspredestinadas.gitlab.io
  - text: Servidor Rizoma Cultural
    link: https://discord.gg/nEYq3rB
  - text: Servidor RPGWorld
    link: https://discord.gg/Sb53Ppe
  - text: Servidor RPG Mestre dos Magos
    link: https://discord.gg/QFveaGW
  - text: Servidor RPG Horda RPGista
    link: https://discord.gg/RP9Quax
  - text: Skype
    link: https://skype.com
  - text: Mumble
    link: https://mumble.com
  - text: Role Gate
    link: https://www.rolegate.com/
  - text: Open RPG
    link: http://www.rpgobjects.com/index.php?c=orpg
  - text: Roll20
    link: http://www.roll20.com
  - text: Slack
    link: http://www.slack.com
  - text: Fantasy Grounds
    link: https://www.fantasygrounds.com/home/home.php
  - text: Astral
    link: https://www.astraltabletop.com/
  - text: Virtual TableTop
    link: https://store.steampowered.com/app/286160/Tabletop_Simulator/?l=portuguese
  - text: GM Forge
    link: https://store.steampowered.com/app/842250/GM_Forge__Virtual_Tabletop/?l=portuguese
  - text: Taulukko Legacy
    link: https://legacy.taulukko.com.br/
  - text: RRpg
    link: http://www.rrpg.com.br/
  - text: WhatsApp
    link: https://www.whatsapp.com
  - text: Telegram
    link: https://www.telegram.com
  - text: Twitch
    link: https://www.twitch.tv
  - text: OBS (_Open Broadcaster Software_)
    link: https://obsproject.com/pt-br
  - text: Critical Role
    link: https://critrole.com/
  - text: Vídeo do Canal Tabletop sobre o Fate
    link: https://www.youtube.com/watch?v=NOFXtAHg7vU
  - text: Discord 
    link: https://www.discord.com
  - text: DnDice 
    link: https://play.google.com/store/apps/details?id=com.christian.bar.dndice&hl=pt_BR
  - text: Sidekick 
    link: https://github.com/ArtemGr/Sidekick
  - text: Craig 
    link: https://craig.chat/
  - text: Rollem
    link: https://rollem.rocks/
  - text: Roll'em bot
    link: https://telegram.me/rollembot
  - text: Invisible Sun
    link: https://www.montecookgames.com/store/product/invisible-sun-preorder/
  - text: Good Society 
    link: https://storybrewersroleplaying.com/good-society/
  - text: Link para o post do _Pascal Godbout_ sobre como configurar Discord e Roll 20 para jogo em Fate 
    link: https://www.facebook.com/groups/faterpg/permalink/2550019671921434/
  - text: Documento do Pascal Godbout sobre  como configurar Discord e Roll 20 para jogo em Fate 
    link: https://drive.google.com/file/d/1IKV_PCN_82gAElb9v3lISkSsQVF7xHnJ/view
  - text: Evil Hat (baixe MUITOS jogos que a Evil Hat na faixa)
    link: https://evilhat.itch.io/
  - text: Til Dawn
    link: https://evilhat.itch.io/til-dawn-a-world-of-adventure-for-fate-core
  - text: Artigo sobre Conduzir campanhas via Internet do Randy Oest
    link: http://randyoest.com/2018/managing-long-term-fate-rpg-campaign-online/
  - text: Artigo da Bleeding Cool citado pelo Velho Lich 
    link: https://www.bleedingcool.com/2020/03/18/things-tabletop-gamers-can-do-to-cope-with-coronavirus/
  - text: VelociPastor
    link: https://en.wikipedia.org/wiki/The_VelociPastor
  - text: FateUtils - Rolador de Dados (entre outras coisaqs) para Fate feito pelo Lucas Peixoto, da Comunidade Fate Brasil
    link: https://lucaspeixoto.github.io/discord_bot.html
  - text: SavageBot - Rolador de Dados para Savage Worlds, feito no Brasil, que também rola dados Fate
    link: https://github.com/alessio29/savagebot
  - text: Artigos da Dragão Brasil sobre como jogar RPG via Internet
    link: https://jamboeditora.com.br/db-rpg-pela-internet/
  - text: Planilhas do Google Drive para controle de mesa de Fate (incluindo referências de Regras, Aspectos, Fichas de Personagem) - em Inglês, por Artur Gajewski
    link: https://docs.google.com/spreadsheets/d/1eSE76tas5MPxlTIKDB9_4lz2e7ZsW9pLRUXkauyBiYQ/edit?fbclid=IwAR11jaUXZZBZnP8RskZWwwIdAPV42g0zIF8N0V7aw1dQBt6OWFO8XZ_bv5A#gid=933098808
  - text: Planilha igual à acima, em português
    link: https://docs.google.com/spreadsheets/d/1dtB5ePMqqbw4xJo-Jp8EH9jXkqtHaE_e6hZ0gjU0LIE/edit#gid=933098808
  - text: Exemplo de campanha usando personagens de Nest
    link: https://docs.google.com/spreadsheets/d/1yUeQbh1DeoOfNunAq9YxHOeOelxxzx2DNsuJzO6p3HA/edit#gid=933098808
  - text: Dragon Dice - aplicativo para rolar dados e administrar jogos online
    link: https://play.google.com/store/apps/details?id=rpg.mundo.mundorpg_dadosparawhatsapp&hl=pt_BR
episode_count: 59
episode: 50
---

___Alerta Vermelho! Alerta Vermelho!___

Hoje, os Fate Masters abandonam o plano original de um episódio comemorativo para um serviço de utilidade pública, onde o Mr. Mickey Fábio, o Velho Lich Rafael, a Palomita Maína e o Cicerone Luiz se reunem para falar sobre Jogos Online: como sugiram, o que usar, como usar, quais os caminhos para seguir e continuar jogando e socializando nessa situação de emergência na saúde pública.

Lembrem-se de tomar todos os cuidados com a saúde, evitar sair de casa sem necessidade, cuidados no contato com outras pessoas e evitar ao máximo contato com pessoas que sejam parte dos grupos de riscos!

Além disso, nosso _shownotes_ está carregado de links úteis nesse momento, para _bots_, serviços e muitas outras coisas para você se informar sobre o COVID-19 e sobre como jogar _online_ e recursos para seu jogo _online_.

Para que quando isso passe, todos voltemos a jogar presencialmente, afinal quanto _mais Fate melhor_, e acima de tudo _quanto mais humanidade melhor_.

As redes sociais do Fate são:

- **E-mail:** <fatemasterspodcast@gmail.com>
- **Discord:** <http://bit.ly/FateBrasilDiscord>
- **WhatsApp:** <http://bit.ly/FateBrasilWhatsApp>
- **Telegram:** <http://t.me/FateBrasilTelegram>

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `Fábio Costa#9087` no [Discord](https://discordapp.com) e `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ `lcavalheiro#0520` no [Discord](https://discordapp.com) e [`lcavalheiro` no Telegram](https://t.me/lcavalheiro)
+ _**Palomita:**_ `Palomita#1312` no [Discord](https://discordapp.com) e [`mainapaloma.delima` no Facebook](https://www.facebook.com/mainapaloma.delima)

[maps-omniverse]: https://goo.gl/maps/KUoVCfrkU7GvG8FG7
[rpgworld]: https://www.facebook.com/rpgworldsite
[rpgecultura]: https://www.facebook.com/RPGCultura/
[discord-mestredosmagos]: https://discord.gg/rgK9RDk
