---
title: "Fate Masters Episódio 43 - Pauta Livre"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2018-09-01 20:30:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Pauta Livre
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Rafael Sant'anna Meyer
 - Luís Cavalheiro
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
podcast_time: "52min"
itunes:
  duration: "51:45"
audios:
 - OGG: https://archive.org/download/FateMasters43PautaLivre/FateMasters43-PautaLivre.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters43PautaLivre/FateMasters43-PautaLivre.mp3
     size: 71380992
iaplayer: FateMasters43PautaLivre
related_links:
- text: Arecibo
  link: https://www.drivethrurpg.com/product/240445/Arecibo-o-A-World-of-Adventure-for-Fate-Core
- text: Red Planet
  link: https://www.drivethrurpg.com/product/199932/Red-Planet-o-A-World-of-Adventure-for-Fate-Core
- text: Eagle Eyes
  link: https://www.drivethrurpg.com/product/144754/Eagle-Eyes-o-A-World-of-Adventure-for-Fate-Core
- text: Weird World News
  link: https://www.drivethrurpg.com/product/251127/Weird-World-News-o-A-World-of-Adventure-for-Fate-Core
- text: Wearing the Cape
  link: http://www.wearingthecape.com/rpg
- text: Shadowrun
  link: https://newordereditora.com.br/categoria-produto/rpg/shadowrun-rpg/
- text: A poesia citada pelo Cicerone sobre _"A peça poderosa..."_
  link: https://www.poetryfoundation.org/poems/51568/o-me-o-life
- text: Unknown Armies
  link: https://en.wikipedia.org/wiki/Unknown_Armies
- text: Um conto de _Wearing the Cape_ escrito pelo Mr. Mickey  
  link: https://app.simplenote.com/publish/KGwGdm
- text: Fading Suns
  link: https://en.wikipedia.org/wiki/Fading_Suns
- text: Nós (Zamyatin)
  link: https://en.wikipedia.org/wiki/We_(novel)
- text: Perry Rhodan
  link: https://en.wikipedia.org/wiki/Perry_Rhodan
- text: Magic Knight Rayearth
  link: https://en.wikipedia.org/wiki/Magic_Knight_Rayearth
- text: _Hero Games 5th Edition Rule Book Ballistic Test_ (ou, um sistema **LITERALMENTE** à prova de balas)
  link: http://www.youtube.com/watch?v=K3Wmj46S5qo
- text: Apotheosis Drive/X
  link: https://www.drivethrurpg.com/product/117506/Apotheosis-Drive-X--FatePowered-Mecha-RPG--SD-MIX
- text: Silent Möbius
  link: https://en.wikipedia.org/wiki/Silent_M%C3%B6bius
- text: Karyu Desentsu
  link: https://www.catarse.me/kdrpg
- text: Masters of Umdaar
  link: https://www.drivethrurpg.com/product/155458/Masters-of-Umdaar-o-A-World-of-Adventure-for-Fate-Core
- text: Hayiore! Nyaruko-San!
  link: "https://en.wikipedia.org/wiki/Nyaruko:_Crawling_with_Love"
- text: Darling in the Franxx
  link: https://en.wikipedia.org/wiki/Darling_in_the_Franxx
- text: Dungeon Crawl Classics
  link: https://newordereditora.com.br/categoria-produto/rpg/dcc-rpg/
- text: Vampire The Masquerade 5th Edition
  link: https://www.worldofdarkness.com/v5
- text: Changeling The Dreaming 20 Years Edition
  link: http://theonyxpath.com/category/worlds/classicworldofdarkness/changelingthedreaming/
- text: Castelo Falkenstein
  link: http://retropunk.net/editora/o-que-e-castelo-falkenstein/
- text: MADCAP - Screwball Cartoon Role-Play
  link: https://www.drivethrurpg.com/product/213960/MADCAP-Screwball-Cartoon-RolePlay
- text: "Good Society: A Jane Austen Roleplaying Game"
  link: https://www.kickstarter.com/projects/259750074/good-society-a-jane-austen-roleplaying-game
- text: _A Manor of Speaking_ - _Gameplay_ de _Good Society_ que o Mr. Mickey citou
  link: https://www.youtube.com/watch?v=90TpYL6ZB1I
- text: SeanchaS
  link: https://www.dungeonist.com/produto/seanchas-historias-nao-se-repetem-digital/
- text: Asas da Vizinhança
  link: https://www.dungeonist.com/produto/asas-da-vizinhanca/
- text: Jogos a La Carte
  link: https://www.dungeonist.com/vendor/jogos-a-la-carte/
events:
- time: "00:10"
  text: Apresentação
- time: "01:22"
  text: "O que nos leva a adaptar coisas?"
- time: "11:12"
  text: As idéias de Adaptação dos Fate Masters
- time: "34:28"
  text: O que os Fate Masters andaram jogando ou lendo sem ser Fate
- time: "48:02"
  text: Considerações Finais 
episode_count: 54
episode: 43
---

Em um Fate Masters mais curto e um tanto diferente, o Velho Lich Rafael Meyer, o Mr. Mickey Fábio Costa e Cicerone Luís Cavalheiro resolvem ter um papo mais livre, sem pauta fixa, falando sobre coisas que gostam de adaptar, coisas que levam eles a adaptar coisas, e o que tem lido ou jogado fora de Fate. E sobre como narradores de RPG são autores de fanfics por expandir os cenários. 

E também lembramos de alguns sistemas que possuem cenários muito legais, mas com regras talvez complexas demais, sobre como o Mr. Mickey quer se aventurar por mais de 600 livros (!!!) ou 2950 livros (!!!!!!!!!!!) para fazer uma adaptação de cenário, e sobre como o Cicerone não conhece Guerreiras Mágicas de Rayearth ... 

E trazemos de volta um clássico, o sistema LITERALMENTE a prova de balas Hero System

{% include youtube.html id="K3Wmj46S5qo" %}

E reafirmando o Postulado do Cicerone para Adaptações de RPG:

> _"Se X é idéia, X é narrável!"_

Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar na [comunidade do Google+ do Fate Masters][gplus], na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, pela [página do Fate Masters no Facebook][fb-page] e agora pelo [servidor do Movimento Fate Brasil no Discord][fb-discord]

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters

