---
title:  "Fate Masters Episódio 62 - Joga #iHunt (parte 2)"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-12-19 11:00:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - joga
 - ihunt
explicit: yes
header: no
podcast_comments: true
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "137min"
itunes:
   duration: "02:17:30"
audios:
  - OGG: https://archive.org/download/fate-masters-62-jogai-huntpt-2/FateMasters62-JogaiHuntpt2.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-62-jogai-huntpt-2/FateMasters62-JogaiHuntpt2.mp3
      size: 131194880
iaplayer: fate-masters-62-jogai-huntpt-2
soundtrack:
- music: Cutting Edge
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/cutting-edge
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:01"
    text: Alerta de Recomendação Etária e Gatilhos
  - time: "00:01:17"
    text: "Introdução ao podcast, e comentários sobre o financiamento coletivo de iHunt e sobre o Cicerone assumindo a cadeira do narrador"
  - time: "00:08:05"
    text: "_Previously on... iHunt:_ recaptulando os eventos da mesa anterior, enquanto os iHunters são aguardando a cavalaria, e somos apresentados ao Professor Jonas"
  - time: "00:29:08"
    text: "Enquanto Maradona faz uma corrida no 99, Valdeci prepara umas proteções para caso o kisuco ferva e Carina precise dar o pé, e descobrem que a corrida do 99 era para o Professor Jonas, enquanto planos são elaborados para tirar o toba de todo mundo da reta"
  - time: "01:00:05"
    text: "Maradona usa seus contatos da época do Movimento para obter um favor do dono do Morro da Vila Cruzeiro, Pelé"
  - time: "01:07:50"
    text: "Maradona extrai a família do Pelé do Morro, enquanto Carina e Valdeci dão seus pulos para ganhar tempo para os carros de extração os resgatar para fora do morro, e o Professor Jonas consegue ganhar algum tempo quando os carros chegam"
  - time: "01:19:39"
    text: "Quando os carros de extração saem cantando pneu, começa a perseguição da polícia contra os iHunters, com Maradona abalroando os carros da polícia em um momento Pica-Pau Rachador, Jonas chama a atenção (e a raiva) da população do Morro, a Polícia atira para todos os lados (menos nos carros) e Valdeci usa sua _Flashbang Improvisada_ para estourar um transformador. Afinal de contas, _Em Caso de Dúvida... C4_. _Amanhã o Morro desce!_ _Isso vai passar na Grobo!_"
  - time: "01:32:01"
    text: "Valdeci explica seu plano... Invadir o Lobato, roubar o código e mostrar que viola o iHunt, usar os servidores do mesmo para disseminar as provas do pastor e destruir a reputação do pastor-demonho. Enquanto isso, Maradona conta a novidade para Carina: ela é a gata do Maradona. No meio do caminho, o Professor prepara um _Chamado às Armas_ contra o Lobato e Valdeci ativa seus _Cyberarmamentos_. E ao executar o plano, um +21 com a Vantagem é tudo que é necessário para provocar um momento _PIRATEANDO O PLANETA!_"
  - time: "01:58:17"
    text: "No fim... O passado de Valdeci faz ele decidir se mandar, dar um perdido para tentar a sorte fora do Breasil (e do iHunt), enquanto deixa algo para Carina e Maradona."
  - time: "02:05:32"
    text: "Considerações Finais"
related_links:
  - text: Parte 1 do jogo de \#iHunt dos Fate Masters
    link: http://fatemasters.gitlab.io/podcast/FateMasters61-JogaIhunt/
  - text: IndieVisível (Facebook)
    link: https://www.facebook.com/IndieVisivelPress
  - text: Episódio de \#iHunt
    link: http://fatemasters.gitlab.io/podcast/FateMasters51-AnalisaiHunt/
  - text: Movimento Fate Brasil (Facebook)
    link: https://www.facebook.com/groups/336649349791444
  - text: Movimento Fate Brasil (Discor)
    link: https://discord.gg/zp7GQt9YCG
  - text: \#iHunt Brasil (Facebook)
    link: https://www.facebook.com/groups/337882581467185
  - text: Página do \#iHunt em Inglês (Facebook)
    link: https://www.facebook.com/iHuntSeries
  - text: Catarse do \#iHunt em Português
    link: https://catarse.me/ihunt
  - text: Ficha da Carina
    link: http://fatemasters.gitlab.io/personagens/CarinaSouza/
  - text: Ficha do Diego
    link: http://fatemasters.gitlab.io/personagens/DiegoDaSilva/
  - text: Ficha do Valdeci
    link: http://fatemasters.gitlab.io/personagens/ValdeciAndrade/
  - text: Ficha do Professor Jonas
    link: http://fatemasters.gitlab.io/personagens/JonasBeltrao/
episode_count: 82
episode: 62
---

<span style="font-size: larger">**TW:** Violência, Baixo Calão, Citação a violência sexual envolvendo menores, religião como manipulação</span>


E as aventuras de  [Valdeci (Fábio)][valdeci], [Carina (Cicerone)][carina] e [Maradona (Palomita)][diego], continuam, com  os Fate Masters de volta à mesa para encerrar essa aventura de #iHunt!

Os Fate Masters voltam ao Hell de Janeiro, #sendofodidos, enquanto tentam escapar com vida (e se possível com algum no bolso) da ameaça de um Demônio político (em todos os sentidos) enquanto salvam um milagreiro de verdade, agora com a ajuda do Velho Lich, que deixa a cadeira de Narrador (que passa ao Cicerone) e assume a de jogador com um caçador veterano disposto a ajudar o time, [o Professor Jonas Beltrão][professor].

Entre cópias descaradas de aplicativos de celular, Lenovo vs Dell, violência policial, transformadores explodindo no morro, momentos [PIRATEANDO O PLANETA!][hacktheplanet] em motéis de alta rotatividade e uma paixão que surge (ao menos de um deles), os #iHunters se encontram em uma encruzilhada: dinheiro, moral, ambos ou nenhum? Qual será a opção? E quais serão as consequências dos mesmos? Às vezes, #serfodido é melhor do que ter uma Luta Justa!

Lembrando que #iHunt bateu a meta do Financiamento Coletivo Flex pela IndieVisível Press, e você pode conhecer mais na [comunidade do #iHunt no Facebook][fb-ihuntbrasil] e na [página do #iHunt em inglês pela Machine Age][fb-ihunt].

Para saber ainda mais sobre #iHunt, caso esteja caindo de paraquedas, ouça os nossos episódios onde [analisamos o #iHunt][fm-ihunt] e onde falamos com o [Leonardo Himura da IndieVisível Press][fm-indievisivel], além de obviamente ouvir a [1ª Parte dessa aventura][fm-jogaihunt] caso tenha a perdido!

E sempre podem contactar os Fate Masters na [comunidade do Movimento Fate Brasil][fb-fate], pelo [Discord do Movimento Fate Brasil][discord-fatebrasil], e, em especial nesse episódio, pelo email <fatemasterspodcast@gmail.com>. E por meio dele vocês poderão pedir a Parte 2 dessa mesa.

E lembrando... A Lei de Olívia Hill continua valendo, sempre!

E a prova do crime (faltando apenas +2 da _Referência Hacker_ de Valdeci e +1 de ajuda passiva do Professor), para um +21

![/images/RolamentoIhunt.jpeg](/images/RolamentoIhunt.jpeg)


[fm-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt  %}
[fm-indievisivel]: {% post_url podcast/2021-10-09-FateMasters60-ApresentaLeonardoHimura-IndieVisivel %}
[carina]: {% post_url Personagens/2021-11-20-CarinaSouza %}
[valdeci]: {% post_url Personagens/2021-11-20-ValdeciAndrade %}
[diego]: {% post_url Personagens/2021-11-20-DiegoDaSilva %}
[fb-fate]: https://www.facebook.com/groups/336649349791444
[fb-ihuntbrasil]: https://www.facebook.com/groups/337882581467185
[fb-ihunt]: https://www.facebook.com/iHuntSeries
[catarse-ihunt]: https://catarse.me/ihunt
[discord-fatebrasil]: https://discord.gg/zp7GQt9YCG
[fm-jogaihunt]: http://fatemasters.gitlab.io/podcast/FateMasters61-JogaIhunt/
[professor]: http://fatemasters.gitlab.io/personagens/JonasBeltrao/
[hacktheplanet]: https://www.youtube.com/watch?v=W5fM6WpC_nE&t=60s
