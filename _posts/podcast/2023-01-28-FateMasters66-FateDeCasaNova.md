---
title:  "Fate Masters Episódio 66 - Fate de Casa Nova (com Leonardo Himura - IndieVisível Press)"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2023-01-28 18:40:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - fate-masters-apresenta
 - Casa Nova
 - IndieVisível
explicit: no
header: no
podcast_comments: true
hosts:
 - Fábio Emilio Costa
 - Lu Cavalheiro
 - Rafael Sant'anna Meyer
 - Leonardo Himura (Vilão Especialmente Convidado)
podcast_time: "108min"
itunes:
   duration: "01:48:44"
audios:
  - OGG: https://archive.org/download/fate-masters-66-fate-casa-nova/FateMasters66-FateCasaNova.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-66-fate-casa-nova/FateMasters66-FateCasaNova.mp3
      size: 145137664
iaplayer: fate-masters-66-fate-casa-nova
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:00"
    text: Nota sobre o problema de áudio sofrido pelo Mr. Mickey
  - time: "00:00:48"
    text: Início do Podcast
  - time: "00:02:50"
    text: A História do Fate no Brasil e como ela dependeu MUITO da comunidade
  - time: "00:06:01"
    text: "A Chegada da nova casa: a IndieVisível Press, além de detalhes da (nova?) tradução"
  - time: "00:23:53"
    text: "Materiais que vêm para o Brasil agora na nova Casa do Fate"
  - time: "00:29:21"
    text: "Um pouco sobre os embrólios envolvendo o licenciamento do Fate no Brasil"
  - time: "00:45:27"
    text: "Questões de Logística"
  - time: "00:53:54"
    text: "Sobre novidades em produtos para a IndieVisível"
  - time: "01:02:01"
    text: "Sobre produtos nacionais da IndieVisível e estratégias para jogos nacionais em Fate"
  - time: "01:07:45"
    text: "Abrindo as perguntas da comunidade: sobre cenários que potencialmente podem vir"
  - time: "01:14:17"
    text: "Sobre produtos antigos do Fate e foco para curto e médio prazo"
  - time: "01:16:52"
    text: "Sobre jogos de Supers em Fate"
  - time: "01:19:37"
    text: "Sobre o SRD do Fate"
  - time: "01:41:00"
    text: "Considerações Finais"
related_links:
  - text: IndieVisível Press
    link: https://indievisivelpress.com.br/
  - text: FUDGE em Português
    link: https://maisquatro.files.wordpress.com/2008/08/fudge-rediagramado.pdf
  - text: Fate 2.0 em Português
    link: https://maisquatro.files.wordpress.com/2008/08/afim-rediagramado.pdf	
  - text: Espírito do Século
    link: https://ludopedia.com.br/jogo/espirito-do-seculo
  - text: Artigo sobre Fate Básico e Fate Acelerados ___NÃO SEREM___ RPGs diferentes (tradução brasileira)
    link: https://fatesrdbrasil.gitlab.io/rpg/2015/11/12/AlternativasAbordagensFateAcelerado/
  - text: Mindjammer
    link: http://mindjammerpress.com/mindjammer/
  - text: Wearing the Cape, o RPG
    link: https://www.drivethrurpg.com/product/210012/Wearing-the-Cape-The-Roleplaying-Game
  - text: Wearing the Cape, a série
    link: https://www.wearingthecape.com
  - text: Daring Comics
    link: https://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
  - text: Uprising
    link: https://evilhat.com/product/uprising/
  - text: Bukatsu!
    link: http://solarentretenimento.com.br/loja/rpgs/52-bukatsu.html
  - text: _Chopstick_
    link: http://www.flyingape.com.br/chopstick/
  - text: Episódio do Fate Masters sobre Solária
    link: /podcast/FateMasters65-ApresentaSolaria/
  - text: Fate SRD em inglês
    link: http://fate-srd.com
  - text: Fate
    link: https://www.faterpg.com/resources/
  - text: "+2d6 RPG"
    link: https://newtonrocha.wordpress.com/sistema-de-rpg-2d6/
  - text: Troika!
    link: https://www.troikarpg.com/
  - text: Zweihänder
    link: https://publishing.andrewsmcmeel.com/zweihanderrpg/
  - text: ORC License da Paizo
    link: https://paizo.com/community/blog/v5748dyo6si7y?The-ORC-Alliance-Grows
  - text: Disney Lorcana
    link: https://www.disneylorcana.com/en-US
episode_count: 86
episode: 66
season: 2
season-name: "Era IndieVisível"
---

E depois de um longo hiato, os Fate Masters começam 2023 com uma novidade!

O Fate tem uma casa nova no Brasil, e é quem trouxe o \#iHunt, a IndieVisivel Press!

E os Algozes dos Jogadores recebem Leonardo Himura para falar um pouco mais sobre todos os detalhes relacionados, desde como foi as conversas com a Evil Hat, o relacionamento com o Fábio Silva, da Pluma, sobre as questões relacionadas a uma nova tradução e as mudanças por vir, e até mesmo sobre polêmicas relacionadas as questões de SRD do Fate (e de outros sistemas) e assim por diante.

Além disso, serão respondidas questões relacionadas a produtos Fate de fora, como serão escolhidos produtos nacionais, e assim por diante

E confirmamos... A IndieVisível ***NÃO trará*** Lancer para o Brasil



Os contatos do Podcast:

+ Email: <fatemasterspodcast@gmail.com>
+ [Discord do Movimento Fate Brasil](https://discord.gg/7HkRXfqeJq)
+ Comunidade do [Movimento Fate Brasil no Facebook](https://www.facebook.com/groups/336649349791444)
+ Página do [Podcast no Facebook](https://www.facebook.com/fatemasterspodcast)
+ Contato na [Mastodon](https://tabletop.vip/@fatemasterspodcast)

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

E do vilão especialmente convidados:

+ [Leonardo Himura](https://www.canva.com/design/DAErWd_jLIg/m0G88GHgYylwf-1p6sZxlQ/view?website#2)

