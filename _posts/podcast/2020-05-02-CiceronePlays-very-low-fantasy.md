---
title:  "Cicerone Plays 001: _A Torre do Feiticeiro das Trevas_"
teaser: O registro de sessão de um dos Algozes dos Jogadores
layout: post
date: 2020-05-02 10:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Acelerado
 - fate-masters
 - sessão-de-jogo
 - Cicerone
explicit: no
header: no
podcast_comments: true 
hosts:
 - Luís Cavalheiro
podcast_time: "193min"
itunes:
  duration: "03:13:37"
audios:
  - OGG: https://archive.org/download/20200502_20200506/20200502.ogg
  - MP3:
    - file: https://archive.org/download/20200502_20200506/20200502.mp3
      size: 170544439
iaplayer: 20200502_20200506
soundtrack:
- music: Static Motion
  artist: Kevin MacLeod
  link: https://incompetech.filmmusic.io/song/4414-static-motion
  license: http://creativecommons.org/licenses/by/4.0/
- music: Lost Time
  artist: Kevin MacLeod
  link: https://incompetech.filmmusic.io/song/4005-lost-time
  license: http://creativecommons.org/licenses/by/4.0/
- music: Colorless Aura
  artist: Kevin MacLeod
  link: https://incompetech.filmmusic.io/song/3524-colorless-aura
  license: http://creativecommons.org/licenses/by/4.0/
- music: The Snow Queen
  artist: Kevin MacLeod
  link: https://incompetech.filmmusic.io/song/4511-the-snow-queen
  license: http://creativecommons.org/licenses/by/4.0/
- music: Frog's Theme
  artist: Yasunori Mitsuda
- music: Battle with Magus
  artist: Yasunori Mitsuda
events:
  - time: "00:00:00"
    text: Introdução dos Jogadores, dos Personagens e da Aventura
  - time: "00:15:45"
    text: Reunião da Resistência e preparativos antes da ação
  - time: "00:50:48"
    text: Na Grande Sebe
  - time: "01:32:15"
    text: Entrando na Torre do Feiticeiro das Trevas pelas masmorras
  - time: "02:00:14"
    text: Duelo contra o Feiticeiro das Trevas
  - time: "02:49:11"
    text: Conclusão e descompressão
episode_count: 61
episode: 1
special: "Cicerony _Plays_"
---

_Caros espectadores, eu sou o Cicerone, e estou aqui para conduzi-los pelos abismos da loucura da narrativa de RPGs..._

...ou em uma aventura curta mesmo, quem sabe...

O _Cicerone Plays_ é o registro de aventuras de uma sessão narradas pelo Cicerone, que vocês conhecem muito bem. Na primeira postagem da série, o Cicerone narrou uma aventura de sua própria autoria, que no momento desta postagem ainda está em fase de testes, _A Torre do Feiticeiro das Trevas_, para alguns jogadores no [servidor do Movimento Fate Brasil no Discord](https://discord.gg/FnqYJv6). Depois de uma sonorizada básica, eis que a aventura está aqui, para quem quiser ouvi-la.

Dúvidas, comentários ou sugestões? Adorarei ouvi-las! Além do servidor do Movimento Fate Brasil, vocês podem encontrar o Cicerone em:

+ lcavalheiro#0520 no [Discord](https://discordapp.com);
+ lcavalheiro no [Telegram](https://t.me/lcavalheiro).

## Créditos

As seguintes músicas foram usadas na sonorização do arquivo:

Static Motion by Kevin MacLeod  
Link: https://incompetech.filmmusic.io/song/4414-static-motion  
License: http://creativecommons.org/licenses/by/4.0/  

Lost Time by Kevin MacLeod  
Link: https://incompetech.filmmusic.io/song/4005-lost-time  
License: http://creativecommons.org/licenses/by/4.0/  

Colorless Aura by Kevin MacLeod  
Link: https://incompetech.filmmusic.io/song/3524-colorless-aura  
License: http://creativecommons.org/licenses/by/4.0/  

The Snow Queen by Kevin MacLeod  
Link: https://incompetech.filmmusic.io/song/4511-the-snow-queen  
License: http://creativecommons.org/licenses/by/4.0/  

Frog's Theme, Battle with Magus by Yasunori Mitsuda  
All rights reserved

