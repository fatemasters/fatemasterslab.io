---
title:  "Fate Masters Episódio 54 - Apresenta Jorge Valpaços (Lições)"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-12-19 11:30:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate Masters Apresenta
 - Lições
 - Jorge Valpaços
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Rafael Sant'anna Meyer
 - _Vilão Especialmente Convidado:_ Jorge Valpaços
podcast_time: "96min"
itunes:
  duration: "01:36:20"
audios:
  - OGG: https://archive.org/download/fate-masters-54-licoes/FateMasters54-Licoes.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-54-licoes/FateMasters54-Licoes.mp3
      size: 123041792
iaplayer: fate-masters-54-licoes
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:58"
    text: Sobre Lições e os RPGs com temática escolar 
  - time: "00:16:32"
    text: Sobre um possível SRD do Sistema L'Aventure
  - time: "00:19:01"
    text: Sobre as jornadas e problemas no Lições e os impactos em ambos os mundos nas ações dos personagens
  - time: "00:30:04"
    text: Sobre o desenvolvimento do personagem e como isso é influenciado pela participação e colaboração do jogador
  - time: "00:37:55"
    text: Sobre investir experiência no mundo, ao invés de em si mesmos.
  - time: "00:52:41"
    text: Sobre os segredos dos personagens e como as situações envolvendo um personagem podem afetar os demais
  - time: "01:00:30"
    text: Tudo traz lições, tudo tem consequências (e um pequeno _rant_ e debate sobre regras)
  - time: "01:05:46"
    text: "Sobre as minúcias no L'Aventure"
  - time: "01:13:47"
    text: Sobre o Financiamento Coletivo e o projeto Transmídia de Lições
  - time: "01:26:30"
    text: Considerações Finais
related_links:
  - text: Lições no Catarse
    link: https://www.catarse.me/licoes
  - text: Tron
    link: https://en.wikipedia.org/wiki/Tron_(franchise)
  - text: Crônicas de Nárnia
    link: https://en.wikipedia.org/wiki/The_Chronicles_of_Narnia
  - text: Oz
    link: https://en.wikipedia.org/wiki/Land_of_Oz
  - text: The Strange
    link: https://newordereditora.com.br/categoria-produto/rpg/the-strange/
  - text: Nest
    link: https://evilhat.itch.io/nest-a-world-of-adventure-for-fate-core
  - text: Loose Threads
    link: https://evilhat.itch.io/loose-threads-a-world-of-adventure-for-fate-core
  - text: Slip
    link: https://evilhat.itch.io/slip-a-world-of-adventure-for-fate-core
  - text: Prism
    link: https://evilhat.itch.io/prism-a-world-of-adventure-for-fate-core
  - text: Serial Experiments Lain
    link: https://en.wikipedia.org/wiki/Serial_Experiments_Lain
  - text: Digimon
    link: https://en.wikipedia.org/wiki/Digimon
  - text: Guerreiras Mágicas Rayearth
    link: https://en.wikipedia.org/wiki/Magic_Knight_Rayearth
  - text: Persona
    link: https://en.wikipedia.org/wiki/Persona_(series)
  - text: Cypher
    link: http://cypher-system.com/
  - text: Cortex
    link: https://www.cortexrpg.com/
  - text: "[C] – The Money of Soul and Possibility Control"
    link: https://en.wikipedia.org/wiki/C_(TV_series)
  - text: Dark
    link: https://dark.netflix.io/pt
  - text: Sliders
    link: https://en.wikipedia.org/wiki/Sliders
  - text: Voyagers - Viajantes do Tempo
    link: "https://en.wikipedia.org/wiki/Voyagers!"
  - text: Fringe
    link: https://en.wikipedia.org/wiki/Fringe_(TV_series)
  - text: "Steins;Gate"
    link: "https://en.wikipedia.org/wiki/Steins;Gate"
  - text: Tales From The Loop
    link: https://en.wikipedia.org/wiki/Tales_from_the_Loop
episode_count: 71
episode: 54
---

E voltamos com um quadro que a um certo tempo não retorna ao Fate Masters, o _Fate Masters Apresenta_, e trazemos algo muito especial pois os Velho Lich _Rafael Meyer_ e o Mr. Mickey _Fábio Costa_ recebe o grande Jorge Valpaços da Lampião Studio para falar do financiamento coletivo do RPG _Lições_, baseado no sistema _L'Aventure_, o mesmo de [_Deloyal_][deloyal], [_Ceifadores_][ceifadores], [_Arquivos Paranormais_][arquivos], [_Encantos_][encantos] e [_Magos Lacunares da Torre Púrpura_.][magos].

![Capa do Lições](/assets/img/licoescapa.png)

Vamos falar um pouco do sistema, ideias, comparações sobre Fate, e sobre detalhes de financiamento coletivo que às vezes parecem estranhos mas que fazem todo sentido quando a razão fica clara. Teremos muitas comparações com sistemas, ir a outro mundo com poucos poderes, e comentários sobre todo tipo de obra de ficção, das mais diretamente às relacionadas totalmente obscuras!

E lembrem-se que o Lições está em Financiamento coletivo em <https://catarse.me/licoes>!

[deloyal]: https://lampiaogamestudio.wordpress.com/deloyal/
[ceifadores]: https://lampiaogamestudio.wordpress.com/2019/01/03/conheca-ceifadores/
[arquivos]: https://lampiaogamestudio.wordpress.com/linha-editorial/arquivos-paranormais/
[encantos]: https://lampiaogamestudio.wordpress.com/tag/encantos/
[magos]: https://lampiaogamestudio.wordpress.com/magos-lacunares-da-torre-purpura/
