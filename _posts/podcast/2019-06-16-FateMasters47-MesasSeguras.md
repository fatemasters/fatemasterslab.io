---
title: Fate Masters Episódio 47 - Mesas Seguras
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-06-16 16:30:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Mesas Seguras
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Maína "Palomita"
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "110min"
itunes:
  duration: "01:50:00"
audios:
 - OGG: https://archive.org/download/FateMasters47MesasSeguras/FateMasters47-MesasSeguras.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters47MesasSeguras/FateMasters47-MesasSeguras.mp3
     size: 139862568 
iaplayer: FateMasters47MesasSeguras
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:46"
    text: Definição sobre Mesa Segura, Inclusiva
  - time: "00:13:36"
    text: Como construir uma mesa segura
  - time: "00:29:58"
    text: Sobre os Jogadores Tóxicos
  - time: "00:58:52"
    text: Garantindo a segurança da mesa segura, em especial em situações excepcionais
  - time: "01:13:05"
    text: Lidando com Gatilhos e Tabus
  - time: "01:18:18"
    text: As Ferramentas para contornar situações excepcionais
  - time: "01:36:53"
    text: _"Devo punir mecanicamente o Jogodor Tóxico? Se sim, como?"_ 
  - time: "01:41:20"
    text: Considerações Finais
related_links:
  - text: O Kit de Ferramentas de Segurança de Mesa que o Mr. Mickey mencionou
    link: https://drive.google.com/drive/folders/114jRmhzBpdqkAlhmveis0nmW73qkAZCj
  - text: Panda's Talking Games
    link: https://misdirectedmark.com/category/podcasts/panda/
  - text: Morts
    link: https://evilhat.itch.io/morts-a-world-of-adventure-for-fate-core
  - text: O Fate Masters Analisa _Fate Horror Toolkit_
    link: /podcast/FateMasters42-AnalisaHorrorToolkit/
  - text: _Hurt Locker_
    link: https://www.drivethrurpg.com/product/199275/Chronicles-of-Darkness-Hurt-Locker
  - text: X-Card
    link: https://fabiocosta0305.gitlab.io/rpg/Xcard.br/
  - text: Script Change
    link: "https://docs.google.com/document/d/1uVhdDWPSVPG3dsMqupH3C_nLZWs-AXVVXGWoU6MKcbY/edit?usp=sharing"
  - text: Storybrewers
    link: https://storybrewersroleplaying.com/
  - text: Good Society
    link: https://storybrewersroleplaying.com/good-society/
  - text: Violentina
    link: https://www.secular-games.com/violentina/
episode_count: 56
episode: 47
---

E temos um novo Fate Masters, e um muito importante.

Os Fate Masters Fábio, O Mr. Mickey, Luís, o Cicerone, e Rafael, o Velho Lich, recebem Palomita, a mais nova Fate Masters, para um debate sobre mesas seguras em jogo. Vamos falar sobre como lidar com jogadores tóxicos, como evitar problemas quando a pessoa está com um dia ruim, e sobre todos os mecanismos de segurança que existem para não ter problemas em seu jogo, já que RPG é um hobby, que merece ser bom para todos.

E lembrando o que está no Horror Toolkit, _Horror não dá o direito de ser horrível_

[![jogar horror não é desculpa para ser horrível](/images/HorrorToolkitNaoSerHorrivel.png)](/images/HorrorToolkitNaoSerHorrivel.png)



As redes sociais do Fate são:

- **E-mail:** <fatemasterspodcast@gmail.com>
- **Discord:** <http://bit.ly/FateBrasilDiscord>
- **WhatsApp:** <http://bit.ly/FateBrasilWhatsApp>
- **Telegram:** <http://t.me/FateBrasilTelegram>


E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e [`lcavalheiro` no Telegram](https://t.me/lcavalheiro)
+ _**Palomita:**_ Palomita#1312 no [Discord](https://discordapp.com) e [`mainapaloma.delima` no Facebook](https://www.facebook.com/mainapaloma.delima)

