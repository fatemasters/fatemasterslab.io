---
title:  "Fate Masters Episódio 67 - O Retorno do Hiato!"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2024-10-06 22:00:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - fate-masters-apresenta
 - O Retorno
 - IndieVisível
 - Pós-Hiato
explicit: no
header: no
podcast_comments: true
hosts:
 - Fábio Emilio Costa
 - Lu Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "79min"
itunes:
   duration: "01:19:25"
audios:
  - OGG: https://archive.org/download/fate-masters-67-o-retorno/FateMasters67-ORetorno.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-67-o-retorno/FateMasters67-ORetorno.mp3
      size: 98250752
iaplayer: fate-masters-67-o-retorno
soundtrack:
- music: Altered Beast (Arcade) - Rise from Your Grave
  artist: Sega
  link: https://www.youtube.com/watch?v=Qq8G4tZbMmw
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:00"
    text: RISE FROM YOUR GRAVE
  - time: "00:00:13"
    text: Introdução ao Episódio (e comentários rápidos sobre a duração do hiato)
  - time: "00:02:13"
    text: O que fizemos durante o nosso hiato
  - time: "00:13:14"
    text: O porquê do hiato além de algumas decisões sobre terminologias
  - time: "00:17:16"
    text: Sobre um possível _Fate Masters Joga_ e algumas conversas sobre os mesmos, incluindo os que gostaríamos de ver e o que infelizmente pensamos que não cabem no momento
  - time: "00:30:33"
    text: Sobre o que torna um cenário interessante para apresentar Fate (ou, _"por que não Medieval?"_)
  - time: "00:39:31"
    text: A História do Fate (Brasil e Mundo) e o envolvimento dos Fate Masters nela
  - time: "01:02:25"
    text: Sobre o que vem por aí
  - time: "01:12:41"
    text: Considerações Finais
related_links:
  - text: IndieVisível Press
    link: https://indievisivelpress.com.br/
  - text: FUDGE em Português
    link: https://maisquatro.files.wordpress.com/2008/08/fudge-rediagramado.pdf
  - text: Fate 2.0 em Português
    link: https://maisquatro.files.wordpress.com/2008/08/afim-rediagramado.pdf	
  - text: Espírito do Século
    link: https://ludopedia.com.br/jogo/espirito-do-seculo
  - text: Artigo sobre Fate Básico e Fate Acelerados ___NÃO SEREM___ RPGs diferentes (tradução brasileira)
    link: https://fatesrdbrasil.gitlab.io/rpg/2015/11/12/AlternativasAbordagensFateAcelerado/
  - text: Mindjammer
    link: http://mindjammerpress.com/mindjammer/
  - text: Wearing the Cape, o RPG
    link: https://www.drivethrurpg.com/product/210012/Wearing-the-Cape-The-Roleplaying-Game
  - text: Daring Comics
    link: https://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
  - text: Bukatsu!
    link: http://solarentretenimento.com.br/loja/rpgs/52-bukatsu.html
  - text: _Chopstick_
    link: http://www.flyingape.com.br/chopstick/
  - text: Fate SRD em inglês
    link: http://fate-srd.com
  - text: Fate SRD Brasil
    link: http://fatesrdbrasil.gitlab.io
  - text: Wanderhome
    link: https://possumcreekgames.itch.io/wanderhome
  - text: Ryuutama
    link: https://www.huginnemuninn.com.br/product/ryuutama-um-rpg-de-fantasia-natural-digital/
  - text: Shinobigami
    link: https://kotodama.itch.io/shinobigami
  - text: 3DeT Victory
    link: https://site.jamboeditora.com.br/3det/
  - text: Old Dragon 2
    link: https://olddragon.com.br/online
  - text: Sigmata
    link: www.huginnemuninn.com.br/product/sigmata-este-sinal-mata-fascistas-fastplay/
  - text: Passion de Las Passiones
    link: https://jamboeditora.com.br/produto/pasion-de-las-pasiones-2/
  - text: Snowdrop (or Snow White and The Seven Dwarves)
    link: http://www.offairiesandtales.com/2018/03/snowdrop-snow-white-by-brothers-grimm.html
  - text: We're all mad here
    link: https://www.montecookgames.com/we-are-all-mad-here/
  - text: Kids on Bike
    link: https://www.huntersentertainment.com/kidsonbikesrpg
  - text: Spirit of the Century
    link: https://evilhat.itch.io/spirit-of-the-century
  - text: Diaspora
    link: https://evilhat.com/product/diaspora/
  - text: Episódio do Fate Masters sobre a Diversão Offline 2016
    link: /podcast/FMA13-DiversaoOffline2016/
  - text: Episódio do Fate Masters sobre a World RPG Fest de 2015
    link: /podcast/FMA7-WRF2015/
  - text: Wicked Fate
    link: https://magpiegames.com/products/wicked-fate-printpdf
  - text: House of Blood
    link: https://en.wikipedia.org/wiki/Houses_of_the_Blooded
  - text: Primeira Sessão Solo do Puffers Underhill
    link: https://fabiocosta0305.gitlab.io/stories/PuffersUnderhillCh1-Tillsoil/
  - text: Ficha de Puffers Underhill
    link: https://docs.google.com/spreadsheets/d/1FEqsda-gwW1Qp7opN9be39gKmDg4F6-2P76tVb_ZB6Q/edit?gid=0#gid=0
  - text: Ficha de Verity-Daisy "Daisy" Plumepoof
    link: https://docs.google.com/spreadsheets/d/1KIqV7LHN3mMs8tepimjA63SCwcgXNDtgL2OlWylGadM/edit?gid=0#gid=0
  - text: Ficha de Estella Belluovi
    link: https://docs.google.com/spreadsheets/d/1kh4aZraAJL1K3_S1f-w5bu_QMuoZ52rzi7vJmuFsoQA/edit?gid=0#gid=0
episode_count: 87
episode: 67
season: 2
season-name: "Era IndieVisível"
---

E depois de OUTRO longo hiato, os Fate Masters retornam em uma pauta aberta para explicar:

+ Por que Sumimos
+ Onde Estávamos
+ O que fizemos

... HOJE, NO GLOBO REPÓRTER... Ops... Nesse episódio do Fate Masters

Os contatos do Podcast:

+ Email: <fatemasterspodcast@gmail.com>
+ [Discord do Movimento Fate Brasil](https://discord.gg/7HkRXfqeJq)
+ Comunidade do [Movimento Fate Brasil no Facebook](https://www.facebook.com/groups/336649349791444)
+ Página do [Podcast no Facebook](https://www.facebook.com/fatemasterspodcast)
+ Comunidade do [WhastApp do Fate](http://bit.ly/FateBrasilWhatsApp)
+ [BlueSky](https://bsky.app/profile/fatemasters.bsky.social) do Fate Masters 
+ [Mastodon](https://tabletop.vip/@fatemasterspodcast) do Fate Masters

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais,incluindo
  + [BlueSky](https://bsky.app/profile/fabiocosta0305.bsky.social)
  + [Mastodon](http://ursal.zone@fabiocosta0305)
  + [Threads como `rogerrabbitcosplay`](https://www.threads.net/@rogerrabbitcosplay)
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://bsky.app/profile/eavatar.bsky.social)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

{% include wod.html %}
