---
title:  "Fate Masters Episódio 61 - Joga #iHunt"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-11-20 23:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - joga
 - ihunt
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "79min"
itunes:
   duration: "01:19:05"
audios:
  - OGG: https://archive.org/download/fate-masters-61-joga-ihunt/FateMasters61-JogaIhunt.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-61-joga-ihunt/FateMasters61-JogaIhunt.mp3
      size: 158697472 
iaplayer: fate-masters-61-joga-ihunt
soundtrack:
- music: Hard Sell Hotel
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/hard-sell-hotel
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:01"
    text: Alerta de Recomendação Etária e Gatilhos
  - time: "00:01:27"
    text: Apresentação da "primeira parte" do Episódio, com a "Sessão 0" do jogo de \#iHunt
  - time: "00:12:35"
    text: Começando o jogo, onde é feita a apresentação do Hell de Janeiro, e o contrato e o alvo é apresentado aos personagens, e um dos NPCs importantes... O Pastor Emanuel. Além de iPhone vs Android e como tirar dinheiro com uma Roupa de Mickey
  - time: "00:35:36"
    text: Na casa do Valdeci, em meio a uma casa onde a Makita tora todo fim de semana, entre cacarecos Disney e equipamento de tecnologia, os personagens investigam mais sobre o alvo... E percebe que tem coisa esquisita, já que parece que o mesmo entende das coisas
  - time: "00:49:32"
    text: Na Ilha do Governador, os caçadores vão a um culto, onde uma possessão demoníaca realmente acontece, e eles percebem que o alvo realmente tem algo interessante
  - time: "01:08:44"
    text: Após o culto, eles percebem que o Alvo realmente tem fé e sabe fazer as coisas... E descobrem que estão sendo usados... Junto com um aplicativo paralelo ao \#iHunt no Brasil... Com uma Caçada muito mais interessante!
  - time: "01:22:38"
    text: Enquanto procuram proteger o pastor, eles são alvejados por assassinos, mas os caçadores se sobressaem com os "brinquedinhos" da AliExpress de Valdeci... 
  - time: "01:32:59"
    text: Agora, apesar da Aparente segurança, os caçadores perderam a caçada original, e descobrem que nas Caçadas de Pedrinho, eles agora _SÃO A CAÇA!_ Para proteger sua mãe, Diego recorre aos amigos do movimento, e uma guerra de facções começa! Enquanto isso, uma Malina ajuda os caçadores sobre o verdadeiro Alvo!
  - time: "01:48:17"
    text: os caçadores recebem do pastor todas as provas, e, enquanto Carina e Diego vão resgatar a mãe de Diego no meio da Guerra de Facções, Valdeci protege o pastor e aciona a \#OpsFakeMessiah contra o verdadeiro Alvo!
  - time: "02:02:18"
    text: enquanto os 66 disseminam informações que complicam o Alvo... A Malina retorna e avisa que ainda terão que esperar um tempo antes de poderem lidar diretamente com o alvo... O que eles podem não ter... Fica para o próximo episódio!
  - time: "02:08:13"
    text: Considerações Finais... E pedido para que as pessoas falem para que continuemos em um próximo episódio! Nos falem em <fatemasterspodcast@gmail.com>
related_links:
  - text: IndieVisível (Facebook)
    link: https://www.facebook.com/IndieVisivelPress
  - text: Episódio de \#iHunt
    link: http://fatemasters.gitlab.io/podcast/FateMasters51-AnalisaiHunt/
  - text: Movimento Fate Brasil (Facebook)
    link: https://www.facebook.com/groups/336649349791444
  - text: Movimento Fate Brasil (Discor)
    link: https://discord.gg/zp7GQt9YCG
  - text: \#iHunt Brasil (Facebook)
    link: https://www.facebook.com/groups/337882581467185
  - text: Página do \#iHunt em Inglês (Facebook)
    link: https://www.facebook.com/iHuntSeries
  - text: Catarse do \#iHunt em Português
    link: https://catarse.me/ihunt
  - text: Ficha da Carina
    link: http://fatemasters.gitlab.io/personagens/CarinaSouza/
  - text: Ficha do Diego
    link: http://fatemasters.gitlab.io/personagens/DiegoDaSilva/
  - text: Ficha do Valdeci
    link: http://fatemasters.gitlab.io/personagens/ValdeciAndrade/
episode_count: 81
episode: 61
---

<span style="font-size: larger">**TW:** Violência, Baixo Calão, Citação a violência sexual envolvendo menores, religião como manipulação</span>

E os Fate Masters estão de volta!

E fazendo algo que não faziam a muito tempo no Podcast: jogando uma mesa!

E não uma mesa qualquer, uma mesa de #iHunt!

Fábio (o Mr. Mickey), Rafael (o Velho Lich), Maína (a Palomita) e Luíz Cavalheiro (o Cicerone) decidem colocar à prova #iHunt, jogando em uma caçada genuinamente brasileira. Esqueça o Rio de Janeiro dos Playba ou do verão da lata! Aqui é a Cidade Maravilha Purgatório da Beleza e do Caos,

Rafael, como o narrador, guia os três outros Fate Masters como caçadores em busca de uma grana adicional para os mais diversos fins:

+ [Valdeci Andrade (Fábio)][valdeci], um _Fui_ Disneymaníaco que, entre um dia e outro de "conserto" de aparalhos celulares de procedência duvidosa travados nos bloqueios do Google, atua como Hacker para o grupo oferecendo informações;
+ [Carina Souza (Luiz)][carina], a _Cavala_ que, enquanto vende trufa no trem lotado da Central, arruma uns corres no #iHunt para juntar uma grana para bancar sua esperança de um campeonato internacional de MMA;
+ E [Diego da Silva, o Maradona (Maína)][diego], um _66_ que até recentemente estava no Movimento, uma estrela em ascenção do Poder Paralelo do Rio, mas que procura uma forma de sair do crime e dar uma vida decente para a mãe. Se para isso, precisar apagar um monstro (ou um deputado costa quente), que seja;

Entre celulares de procedência duvidosa, cultos religiosos, guerra de facções, possessões demoníacas, aplicativos paralelos ao #iHunt e pastores disputando a fé (e o dinheiro) das pessoas, os três #iHunters vão colocar sua reputação, recursos, e até mesmo a vida em risco...

Três estrelas, 15 mil...

Vale tudo isso?

Arraste para a direita e vamos ver o que acontece!

Mas apenas tome cuidado, ou você terá sua luta justa... E como dizem os _tugas_, você dará o peido mestre!

Lembrando que #iHunt está em Financiamento Coletivo Flex (ou seja, ao [financiar você já garante o seu][catarse-ihunt]) em português pela IndieVisível Press, e você pode conhecer mais na [comunidade do #iHunt no Facebook][fb-ihuntbrasil] e na [página do #iHunt em inglês pela Machine Age][fb-ihunt]. 

Para saber ainda mais sobre #iHunt, caso esteja caindo de paraquedas, ouça os nossos episódios onde [analisamos o #iHunt][fm-ihunt] e onde falamos com o [Leonardo Himura da IndieVisível Press][fm-indievisivel].

E sempre podem contactar os Fate Masters na [comunidade do Movimento Fate Brasil][fb-fate], pelo [Discord do Movimento Fate Brasil][discord-fatebrasil], e, em especial nesse episódio, pelo email <fatemasterspodcast@gmail.com>. E por meio dele vocês poderão pedir a Parte 2 dessa mesa.

E lembrando... A Lei de Olívia Hill continua valendo, sempre!

**Atualização**

Como foi dito no episódio, a personagem Carina Souza foi inspirada em uma pessoa real. Júlia Polastri é uma lutadora de MMA nascida em Duque de Caxias/RJ que vende trufas no ramal Saracuruna dos trens da Supervia a fim de complementar a renda e conseguir manter-se disputando os torneios. Caso você queira saber mais sobre ela, ou mesmo conhecer e divulgar o trabalho da atleta, [siga a Júlia no Instagram][polastri-instagram].

[fm-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt  %}
[fm-indievisivel]: {% post_url podcast/2021-10-09-FateMasters60-ApresentaLeonardoHimura-IndieVisivel %}
[carina]: {% post_url Personagens/2021-11-20-CarinaSouza %}
[valdeci]: {% post_url Personagens/2021-11-20-ValdeciAndrade %}
[diego]: {% post_url Personagens/2021-11-20-DiegoDaSilva %}
[fb-fate]: https://www.facebook.com/groups/336649349791444
[fb-ihuntbrasil]: https://www.facebook.com/groups/337882581467185
[fb-ihunt]: https://www.facebook.com/iHuntSeries
[catarse-ihunt]: https://catarse.me/ihunt
[discord-fatebrasil]: https://discord.gg/zp7GQt9YCG
[polastri-instagram]: https://www.instagram.com/polastrimma/
