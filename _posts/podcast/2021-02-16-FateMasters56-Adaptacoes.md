---
title:  "Fate Masters Episódio 56 - Adaptações"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-02-16 21:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Adaptações
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "113min"
itunes:
  duration: "01:53:00"
audios:
  - OGG: https://archive.org/download/fate-masters-56-adaptacoes/FateMasters56-Adaptacoes.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-56-adaptacoes/FateMasters56-Adaptacoes.mp3
      size: 111470592
iaplayer: fate-masters-56-adaptacoes
soundtrack:
- music: Synapse
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/time-travellin-nancy
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:02:08"
    text: Introdução ao assunto
  - time: "00:10:49"
    text: "Por que fazer adaptações?"
  - time: "00:16:13"
    text: "Existe o sistema perfeito para adaptações? E o que adaptar?"
  - time: "00:24:12"
    text: "Jogar _com_ o Harry Potter ou jogar _no cenário_ de Harry Potter?"
  - time: "00:26:04"
    text: "Viver eventos da série ou viver o cenário da série"
  - time: "00:42:43"
    text: "Começando a falar de regras para Fate, e considerando a importância dos Aspectos para reforçar clima e características do cenário"
  - time: "00:58:11"
    text: "Como os pontos de corte nas adaptações ajudam como descobrir as regras a serem usadas, em especial Extras e Fractais. E sobre o como lidar com _metagaming_ para que eventos ocorram não importe como (vulgarmente conhecido como _Vamos matar Hitler!_)"
  - time: "01:20:12"
    text: "_Oh, Toodles!_ Sobre a importância de formar um arcabouço de regras para quando as coisas sairem do caminho quando matarem Hitler"
  - time: "01:24:30"
    text: "Observando jogos que são adaptações _per se_"
  - time: "01:41:45"
    text: "Considerações Finais"
related_links:
  - text: "New RPG Set in World of Avatar: TLA & TLoK"
    link: https://www.magpiegames.com/2021/02/03/new-rpg-set-in-world-of-avatar-tla-tlok/
  - text: "Tales of Xadia"
    link: https://www.talesofxadia.com/
  - text: "Atomic Robo"
    link: https://www.evilhat.com/home/atomic-robo/
  - text: "Dresden Files"
    link: https://www.evilhat.com/home/dresden-files-rpg/
  - text: "Dresden Files Accelerated"
    link: https://www.evilhat.com/home/dresden-files-accelerated/
  - text: "Carnival Row RPG"
    link: https://nerdist.com/article/carnival-row-rpg-download-free/
  - text: "Star Wars AEG"
    link: https://en.wikipedia.org/wiki/Star_Wars:_The_Roleplaying_Game
  - text: "Middle Earth Roleplaying"
    link: https://en.wikipedia.org/wiki/Middle-earth_Role_Playing
  - text: "The Expanse RPG"
    link: https://greenroninstore.com/collections/the-expanse-rpg
  - text: "Podcast da Dungeon Geek"
    link: "https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy8yMzQ3ZjAyYy9wb2RjYXN0L3Jzcw?sa=X&ved=0CAMQ4aUDahcKEwiozr3Rpu_uAhUAAAAAHQAAAAAQAQ&hl=pt-BR"
  - text: "Highlander - The Quickening"
    link: http://www.highlander.org/roleplaying/
  - text: "Doctor Who RPG (Cubicle 7)"
    link: https://www.cubicle7games.com/our-games/doctor-who-rpg/
  - text: "Perry Rhodan"
    link: https://en.wikipedia.org/wiki/Perry_Rhodan
  - text: "The Talons of Weng-Chiang"
    link: https://en.wikipedia.org/wiki/The_Talons_of_Weng-Chiang
  - text: "Rosa (episódio de Doctor Who)"
    link: https://en.wikipedia.org/wiki/Rosa_(Doctor_Who)
  - text: "Supernatural"
    link: https://en.wikipedia.org/wiki/Supernatural_(American_TV_series)
  - text: "Sistema Star Wars da Fantasy Flight"
    link: https://en.wikipedia.org/wiki/Star_Wars_Roleplaying_Game_(Fantasy_Flight_Games)
  - text: "Mandalorian"
    link: https://en.wikipedia.org/wiki/The_Mandalorian
  - text: "Lobo Solitário"
    link: https://pt.wikipedia.org/wiki/Lobo_Solit%C3%A1rio
  - text: "O Mundo de Nárnia"
    link: https://en.wikipedia.org/wiki/Narnia_(world)
  - text: "Fundação"
    link: https://en.wikipedia.org/wiki/Foundation_series
  - text: "Babylon 5"
    link: https://en.wikipedia.org/wiki/Babylon_5
  - text: "Wheel of Time"
    link: https://en.wikipedia.org/wiki/The_Wheel_of_Time
  - text: "Kamen Rider"
    link: https://en.wikipedia.org/wiki/Kamen_Rider
  - text: "Blinovitch Limitation Effect"
    link: https://en.wikipedia.org/wiki/Blinovitch_Limitation_Effect
  - text: "A _fanfic_ de Harry Potter e Cowboy Bebop do Mister Mickey"
    link: https://www.fanfiction.net/s/2783083/1/Magic-Works
  - text: "O Episódio sobre _Dawn Hunters_"
    link: http://fatemasters.gitlab.io/podcast/FateMasters7.5/
  - text: "True Blood"
    link: https://en.wikipedia.org/wiki/True_Blood
  - text: "Dia Nacional do RPG"
    link: https://www.facebook.com/DiaNacionaldoRPG/
episode_count: 73
episode: 56
---

E os Fate Masters retornam para 2021!!! E a coisa vai ser ainda mais doida!

Pois os Fate Masters Rafael, Luiz, Fábio e Maína decidiram que é hora de falar sobre um tema normalmente quente e espinhoso!

ADAPTAÇÕES!

O que adaptar?

Como adaptar?

O que levar em conta?

Como decidir que regras usar?

E, acima de tudo:

O que fazer quando os seus jogadores matarem Hitler?

E fiquem atentos para os Fate Masters no Dia Nacional do RPG 2021. Vamos estar _online_ com muitas atividades!

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)
