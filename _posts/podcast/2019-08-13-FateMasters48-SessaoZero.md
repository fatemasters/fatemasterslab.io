---
title:  "Fate Masters Episódio 48 - Fate para Leigos: Sessão Zero"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-08-13 12:20:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Maína "Palomita" Paloma de Lima
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "115min"
itunes:
  duration: "01:45:00"
audios:
 - OGG: https://archive.org/download/FateMasters48SessaoZero/FateMasters48-SessaoZero.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters48SessaoZero/FateMasters48-SessaoZero.mp3
     size: 133668864
iaplayer: FateMasters48SessaoZero
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:29"
    text: Definição da Sessão Zero
  - time: "00:05:57"
    text: A Sessão Zero e a Definição de Problemas
  - time: "00:12:50"
    text: Criando Faces e Locais no jogo
  - time: "00:17:33"
    text: Transformando tudo isso em Aspectos
  - time: "00:23:55"
    text: As diferenças de Sessão Zero em _one-shot_ e campanhas
  - time: "00:27:58"
    text: Criação de Cenários compartilhada ou individualizada e a Sessão Zero
  - time: "00:38:16"
    text: Sobre o agnosticismo da Sessão Zero
  - time: "00:56:42"
    text: Sobre as questões de criar o personagem na Sessão Zero, em especial sobre o Trio de Fases e quando ele se aplica
  - time: "01:01:20"
    text: Sobre PvP (_Player vs Player_) e jogos de Arena e a Sessão Zero
  - time: "01:22:41"
    text: Sobre as _Armas de Chekhov_ e _McGuffins_ no cenário   
  - time: "01:33:30"
    text: Considerações e recados finais
related_links:
  - text: Mestres de Umdaar
    link: https://evilhat.itch.io/masters-of-umdaar-a-world-of-adventure-for-fate-core
  - text: Episódio de Mestres de Umdaar
    link: /podcast/FateMasters21-AnalisaMastersOfUmdaar
  - text: TVTropes
    link: https://tvtropes.org
  - text: A _Carta X_ (não oficial)
    link: http://fabiocosta0305.gitlab.io/rpg/Xcard.br/
  - text: Limites e Véus
    link: https://rpg.stackexchange.com/questions/30906/what-do-the-terms-lines-and-veils-mean
  - text: Mudança de Roteiro
    link: https://briebeau.itch.io/script-change
  - text: TVTropes
    link: https://tvtropes.org
  - text: Iron Street Combat
    link: https://evilhat.itch.io/iron-street-combat-a-world-of-adventure-for-fate-core
  - text: Til Dawn
    link: https://evilhat.itch.io/til-dawn-a-world-of-adventure-for-fate-core
  - text: Jace e os Piratas do Espaço
    link: https://en.wikipedia.org/wiki/Jayce_and_the_Wheeled_Warriors
  - text: Mindjammer
    link: https://mindjammerpress.com/mindjammer/
  - text: Silverhawks
    link: https://en.wikipedia.org/wiki/SilverHawks
  - text: Dreadstar
    link: https://en.wikipedia.org/wiki/Dreadstar
  - text: Episódio do Sparks em Fate
    link: /podcast/FateMasters7.5
  - text: Sparks em Fate
    link: https://fatesrdbrasil.gitlab.io/rpg/2016/11/18/SparksEmFateBasico/
  - text: Destino das Séries
    link: http://fabiocosta0305.gitlab.io//rpg/DestinoDasSeries-criacao/
  - text: Passion de las Passiones
    link: https://www.drivethrurpg.com/product/227009/Pasion-de-las-Pasiones-Ashcan-Edition
  - text: "MADCAP: Screwball Cartoon Role-play"
    link: https://www.drivethrurpg.com/product/213960/MADCAP-Screwball-Cartoon-RolePlay
  - text: Shadow of The Century
    link: https://evilhat.itch.io/shadow-of-the-century
  - text: Uprising - The Dystopian Universe Roleplaying Game
    link: https://www.drivethrurpg.com/product/252231/Uprising-The-Dystopian-Universe-RPG
  - text: Episódio sobre _Uprising_
    link: /podcast/FateMasters39-AnalisaUprising/
  - text: Scrum no Rugby
    link: http://www.espn.com.br/video/538572_entenda-o-que-e-e-como-funciona-o-scrum-do-rugby
  - text: Scrum (artigo da Wikipedia, sobre Rugby)
    link: https://en.wikipedia.org/wiki/Scrum_(rugby)
  - text: Scrum (artigo da Wikipedia, sobre desenvolvimento de software)
    link: https://en.wikipedia.org/wiki/Scrum_(software_development)
  - text: Loose Threads
    link: https://evilhat.itch.io/loose-threads-a-world-of-adventure-for-fate-core
  - text: O Conto do Senhor Raposo (em Inglês)
    link: http://www.authorama.com/english-fairy-tales-29.html
  - text: Contos de Barba-Azul
    link: http://www.surlalunefairytales.com/bluebeard/
  - text: Sistema de Sinal
    link: https://www.additudemag.com/using-the-stoplight-system-for-school-behavior-management/
  - text: Arma de Chekhov
    link: https://en.wikipedia.org/wiki/Chekhov%27s_gun
  - text: McGuffin
    link: https://en.wikipedia.org/wiki/MacGuffin
  - text: Zork
    link: https://en.wikipedia.org/wiki/Zork
episode_count: 57
episode: 48
---

E temos um novo Fate Masters, e bem legal!

Continuando a série sobre Fate Para Leigo, em um episódio curiosamente agnóstico, falamos sobre a Sessão Zero, onde é definidos todos os parâmetros do seu jogo, desde coisas que estão valendo em termos mecânicos até o Contrato Social da Mesa.

Entre outras coisas, sobre como utilizar as coisas, ajustar cenários prontos, desenvolver problemas e os traduzir Aspectos, desenvolver locais e faces, entre outras coisas.

Além disso, colocamos coisas como criar cenários de maneira compartilhada ou já trazer algo pronto, como definir contrato social e _primers_, alternativas de sessão zero e afins são debatidas aqui.

Lembrando que:

+ _Fate é mais GRAMÁTICO que MATEMÁTICO!_
+ Não se sobreprepare!
+ Você não joga contra os jogadores, e nem para os jogadores, mas _com_ os jogadores!
+ E que às vezes tudo o que os jogadores querem é _A Cúpula do Trovão_!

E fica as dicas dos Fate Masters:

+ ___Cicerone:___ 
  + [24 de Agosto a partir das 10h](https://www.facebook.com/events/465397250886721/), no [SESC São João de Meriti](https://goo.gl/maps/TZnJuU7jQYUBPVaj8) pela [RPGWorld][rpgworld]
  + [31 de Agosto a partir das 10h](https://www.facebook.com/events/1272685219601113/), na [Biblioteca Municipal Gov. Leonel de Moura Brizola](https://goo.gl/maps/T3Qk1Q8kboWZ6Ghf9) pela [RPGWorld][rpgworld]
+ ___Palomita:___ 
  + Vendendo bolsinhas de dados e coisinhas nerds em geral
+ ___Velho Lich:___ 
  + Aulas de Mestragem no [servidor do Discord _Mestre dos Magos_][discord-mestredosmagos]  
  + [Stoplight system](http://cronistasdastrevasbr.com/2019/07/10/seguro-mesmo-quando-perdido/), o sistema de sinais verde, amarelo e vermelho.  
+ ___Mister Mickey:___ 
  + Todo 2° Sábado do Mês a partir das 12h: _Dungeon Geek_, na [_Omniverse_][maps-omniverse]
  + Em Setembro: _Anima Geek_ no CEU Grajaú, pelo pessoal da [RPG & Cultura][rpgecultura]

As redes sociais do Fate são:

- **E-mail:** <fatemasterspodcast@gmail.com>
- **Discord:** <http://bit.ly/FateBrasilDiscord>
- **WhatsApp:** <http://bit.ly/FateBrasilWhatsApp>
- **Telegram:** <http://t.me/FateBrasilTelegram>

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `Fábio Costa#9087` no [Discord](https://discordapp.com) e `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ `lcavalheiro#0520` no [Discord](https://discordapp.com) e [`lcavalheiro` no Telegram](https://t.me/lcavalheiro)
+ _**Palomita:**_ `Palomita#1312` no [Discord](https://discordapp.com) e [`mainapaloma.delima` no Facebook](https://www.facebook.com/mainapaloma.delima)

[maps-omniverse]: https://goo.gl/maps/KUoVCfrkU7GvG8FG7
[rpgworld]: https://www.facebook.com/rpgworldsite
[rpgecultura]: https://www.facebook.com/RPGCultura/
[discord-mestredosmagos]: https://discord.gg/rgK9RDk
