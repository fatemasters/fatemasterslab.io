---
title:  "Fate Masters Episódio 51 - Analisa #iHunt"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-04-19 10:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate Masters Analisa
 - \#iHunt
 - iHunt 
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "100min"
itunes:
  duration: "02:03:25"
audios:
  - OGG: https://archive.org/download/fatemasters51analisaihunt/FateMasters51-AnalisaiHunt.ogg
  - MP3:
    - file: https://archive.org/download/fatemasters51analisaihunt/FateMasters51-AnalisaiHunt.mp3
      size: 161253376
iaplayer: fatemasters51analisaihunt
soundtrack:
- music: Clandestino
  artist: "Manu Chao / Playing For Change" 
  link: https://www.youtube.com/watch?v=Wm0hI0aJanc
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:14"
    text: Introdução
  - time: "00:01:20"
    text: Uma introdução ao \#iHunt (e sobre como hoje a coisa vai ladeira abaixo)
  - time: "00:04:09"
    text: Sobre o cenário de \#iHunt e o Aplicativo do \#iHunt e sobre como ele é um jogo que não é para qualquer um (nada de Fascistas aqui, catzo!)
  - time: "00:26:01"
    text: As Perícias (ou Pacotes de Perícias) e Aspectos do \#iHunt
  - time: "00:35:30"
    text: Sobre os _Lances_, as formas que existem de se matar monstros em \#iHunt (e como isso não é uma categorização como no Storyteller)
  - time: "00:44:21"
    text: Sobre os Monstros em \#iHunt, seus poderes, características e fraqueza, e como construir suas próprias coisas ruins
  - time: "00:53:29"
    text: O Nível de Ameaça e a Vantagem
  - time: "01:01:21"
    text: Sobre o gerador aleatório de Caçadas de Monstros e sobre como várias caçadas são embebidas na vida cotidiana do \#iHunter, e como nem sempre você é o lado bom da coisa
  - time: "01:10:05"
    text: Sobre _Arriscar Aspectos_ e Essência 
  - time: "01:22:19"
    text: Intervalo Comercial (o _X-Card_ do jogo), Formulário de Consentimento e _Selfies_ como Marcos de Evolução e os _Callbacks_ e o Cenário de San Jenaro
  - time: "01:28:10"
    text: Sobre Notas e Adoção de Regras
  - time: "01:53:47"
    text: Considerações Finais
related_links:
  - text: "#iHunt"
    link: https://machineage.itch.io/ihunt-the-rpg
  - text: Uber
    link: http://www.uber.com
  - text: 99 Taxi
    link: http://www.99app.com
  - text: Microsoft
    link: http://www.microsoft.com
  - text: Facebook
    link: http://www.facebook.com
  - text: Google
    link: http://www.google.com
  - text: iFood
    link: http://www.ifood.com.br
  - text: Rolemaster
    link: https://en.wikipedia.org/wiki/Rolemaster
  - text: Hinode
    link: http://www.hinode.com.br
  - text: OSE (Old-School Essentials)
    link: https://necroticgames.com/collections/old-school-essentials
  - text: Priscila a Rainha do Deserto
    link: https://pt.wikipedia.org/wiki/The_Adventures_of_Priscilla,_Queen_of_the_Desert
  - text: Matadores de Vampiras Lésbicas
    link: https://Fen.wikipedia.org/wiki/Lesbian_Vampire_Killers
  - text: Zumbilândia
    link: https://pt.wikipedia.org/wiki/Zombieland
  - text: Caça-Fantasmas
    link: https://pt.wikipedia.org/wiki/Zombieland
  - text: Elvira
    link: https://pt.wikipedia.org/wiki/Elvira%2C_Mistress_of_the_Dark
  - text: Vampirella
    link: https://en.wikipedia.org/wiki/Vampirella
  - text: Walking Dead
    link: https://en.wikipedia.org/wiki/The_Walking_Dead_(TV_series)
  - text: Resident Evil
    link: https://en.wikipedia.org/wiki/Resident_Evil
  - text: Santa Clarita Diet
    link: https://en.wikipedia.org/wiki/Santa_Clarita_Diet
  - text: Resenha de \#iHunt do Mr. Mickey para a Dungeon Geek
    link: https://www.dungeongeek21.com/resenha-ihunt/
  - text: Gráfico dos Dados Fate no AnyDice
    link: https://anydice.com/program/1035b
  - text: Gráfico dos Dados Fate com Vantagem no AnyDice
    link: https://anydice.com/program/1b0f4
  - text: "#iHunt: Killing Monsters in the Gig Economy (English Edition)" 
    link: https://www.amazon.com.br/iHunt-Killing-Monsters-Economy-English-ebook/dp/B074QNS9S3/
  - text: "Machine Age (itch.io)"
    link: https://machineage.itch.io
  - text: "Machine Age no Twitch"
    link: https://twitch.tv/machineageproductions
  - text: "Aphoteosis Drive/X"
    link: https://www.drivethrurpg.com/product/147795/Apotheosis-Drive-X--FatePowered-Mecha-RPG--HD-Mix
  - text: "Machine Age"
    link: https://machineage.tokyo/
  - text: "Olivia Hill Rule"
    link: https://machineage.tokyo/olivia-hill-rule/
  - text: John Constantine
    link: https://en.wikipedia.org/wiki/Hellblazer
  - text: BBSes
    link: https://en.wikipedia.org/wiki/Bulletin_board_system
  - text: Dracula
    link: https://en.wikipedia.org/wiki/Dracula
  - text: Fate - Criaturas Fantásticas
    link: https://www.catarse.me/fatecriaturas
  - text: Ceifadores
    link: https://lampiaogamestudio.wordpress.com/2019/01/03/conheca-ceifadores/
  - text: Discord da Machine Age - San Genaro
    link: https://discordapp.com/channels/307303866460930052/307303866460930052/701521349817925665
episode_count: 60
episode: 51
---

![[Nada de Fascistas](/assets/img/Olivia-Hill-Rule.png)](/assets/img/Olivia-Hill-Rule.png)


E estamos de volta... E hoje em um episódio totalmente não recomendável para menores de 18 anos.

Em um episódio com um humor de certa forma ácido, caústico e brutal, os Fate Masters Rafael, Fábio e Luís vão revisar um jogo que, tanto pelas suas mecânicas, artes e posicionamento chocou, polemizou e foi um dos mais comentados nos últimos tempos: \#iHunt.

Venha se sujeitar a ser um fodido e caçar monstros na economia colaborativa, onde todas as pequenas e grandes mazelas do capitalismo apenas reforçam três coisas: 

- te fuder de maneira literal e abstrata
- mostrar que monstros não são analogias, são criaturas
- nem todo monstro suga sangue: alguns sugam seu dinheiro

Vamos falar sobre as várias mecânicas novas e não tão novas, como os Pacotes de Perícias, Criação dos Monstros, A Vantagem que faz com que uma luta justa seja apenas um eufemismo para bater as botas e muito mais.

Vamos também falar sobre como os Monstros em \#iHunt não são sempre maus, e às vezes nem o pior mal, embora ocasionalmente alguns deles sejam tão ruins que o Alemão possui uma palavra especial para eles, [_backpfeifengesicht_](https://www.urbandictionary.com/define.php?term=Backpfeifengesicht), ou seja, _"alguém que tá pedindo um soco na cara"_. E sobre como o nível de Ameaça é definido e sobre como é uma péssima ideia tentar estacar um vampiro pelo peito, graças a um treco chamado _Esterno_.

Venha para San Jenaro, a Sunnydale hipster do Vale do Silício, onde uma caçada de monstros está a um clique e enquanto não está caçando monstros você tem um Emprego vindo diretamente do Vagas Arrombadas.

Está desesperado o bastante para arriscar seu pescoço contra a Duqueza Demoníaca de San Jenaro para juntar dinheiro para pagar contas, comprar aquele remédio contra fibromialgia e (se sobrar algum) tomar um sorvete? Se sim, apenas deslize para a direita no \#iHunt para aceitar o serviço. Mas lembre-se: o sistema é foda e tem toda uma série de pequenas merdas para te fuder de todos os jeitos em todos os momentos.

E acima de tudo: NADA DE FASCISTAS!

![[Nada de Fascistas](/images/iHuntNoFascist.png)](/images/iHuntNoFascist.png)

> ### NADA DE FASCISTAS
>
> Se você é um fascista, você não é bem vindo nesse jogo. É contra as regras. Se você está lendo isso e pensando, _"Porra, agora todo mundo que não concorda com você é fascista?"_, então provavelmente você é um fascista, ou ao menos é incapaz de inferir coisas do contexto e reconhecer um ambiente político perigoso que tornou aqueles que são oprimidos mais raivosos. Não jogo esse jogo. Vá se tratar. Cresça. Aprenda. Vá assistir _"Um lindo dia na vizinhança"_ ou algo similar.

> ***Atualização (2024-02-20):*** Após a publicação no Brasil do iHunt, temos a tradução oficial dessa parte!
>
> ![[Nada de Fascistas - IndieVisível](/assets/img/RegraOliviaHill-IndieVisível.png)](/assets/img/RegraOliviaHill-IndieVisível.png)
>
>> ### FORA FASCISTAS
>>
>> Se você é fascista, você não é bem-vindo nesse jogo. É contra as regras. Se você está lendo isso e pensando, “ah, mas você só quer chamar todo mundo que discorda de você de fascista”, você provavelmente é um fascista, ou é incapaz de tirar conclusões com base no contexto e entender um clima político perigoso que faz com que os oprimidos sejam hiperbólicos. Não jogue este jogo. Vai se curar. Cresça. Aprenda. Vai assistir um pouco de Castelo Rá-Tim-Bum ou algo assim.

Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar  na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, pela [página do Fate Masters no Facebook][fb-page] e agora pelo [servidor do Movimento Fate Brasil no Discord][fb-discord]

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            | 5         |
|             | [#iHunt][fma-ihunt] | 5              | 5              | 5            | 5         |
| **3**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         | 4.83      |
| **5**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         | 4,78      |
| **6**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          | 4,73      |
| **7**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          | 4,68      |
|             | [Nest][fma-nest]                                      | 4,8            | 4,5            | 4,75         | 4,68      |
| **10**       | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          | 4,58      |
| **11**      | [Boa Vizinhança/Good Neighbors][fma-nest]             | 4,7            | 4,25           | 4,75         | 4,56      |
| **12**      | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         | 4,5       |
| **13**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            | 4,33      |
| **14**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         | 4,25      |
| **15**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         | 3,92      |
| **16**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         | 3,5       |
| **17**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:---------:|
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         | 4         |
|             | [_Horror Toolkit_][fm-HorrorToolkit]                  | 4,5            | 4,5            | 2            | 3,6       |


[fma-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt %}
[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url podcast/2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url podcast/2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url podcast/2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url podcast/2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url podcast/2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url podcast/2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url podcast/2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url podcast/2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url podcast/2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-mechavskaiju]:  {% post_url podcast/2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url podcast/2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url podcast/2017-11-17-FateMasters35-AnalisaDaringComics %}
[fma-wearingthecape]: {% post_url podcast/2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url podcast/2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url podcast/2018-05-23-FateMasters41-AnalisaSecretsOfCats %}
[fm-HorrorToolkit]: {% post_url podcast/2018-08-06-FateMasters42-AnalisaHorrorToolkit %}
[fma-nest]: {% post_url podcast/2019-03-26-FateMasters45-AnalisaNestBoaVizinhanca %}

[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters
