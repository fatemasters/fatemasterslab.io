---
title:  "Fate Masters Episódio 53 - Analisa Super-Powered Fate"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-12-14 22:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Fate Masters Analisa
 - Fate Condensed
 - Fate Condensado
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "98min"
itunes:
  duration: "01:38:45"
audios:
  - OGG: https://archive.org/download/fate-masters-53-analisa-super-powered-fate/FateMasters53-AnalisaSuperPoweredFate.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-53-analisa-super-powered-fate/FateMasters53-AnalisaSuperPoweredFate.mp3
      size: 121149440
iaplayer: fate-masters-53-analisa-super-powered-fate
soundtrack:
- music: Voyage of Discovery
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/voyage-of-discovery
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:39"
    text: Contexto de Super-Powered Fate
  - time: "00:06:15"
    text: O Histórico de Super-Powered Fate e sua origem, além de questões relacionadas ao quanto de regras são necessárias em jogos de Supers
  - time: "00:17:28"
    text: Como os super-poderes funcionam em Super-Powered Fate e do conceito de poder
  - time: "00:27:18"
    text: Como se cria um personagem super-heróico, incluindo o Aspecto de Poder, Perícias e Façanhas Super-Heróicas
  - time: "00:39:16"
    text: Sobre a _Escala de Poder_, a "Façanha Especial" que define tanto o nível de poder geral como os níveis específicos de cada personagem e sobre como isso pode possibilitar interação de personagens de nível de poder diferenciado.
  - time: "00:55:27"
    text: Os conceitos de poder incluídos no Super-Powered Fate e sobre como eles refletem os principais tipos de personagem incluídos em quadrinhos de super-heróis
  - time: "01:09:58"
    text: Sobre os personagens de exemplo de Super-Powered Fate
  - time: "01:13:30"
    text: Ideias 
  - time: "01:21:33"
    text: Rankeamento
  - time: "01:34:47"
    text: Considerações Finais
related_links:
  - text: Super-Powered Fate
    link: https://www.drivethrurpg.com/product/324434/SuperPowered-Fate
  - text: Wearing the Cape
    link: https://www.drivethrurpg.com/product/210012/Wearing-the-Cape-The-Roleplaying-Game
  - text: Daring Comics
    link: https://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
  - text: Venture City
    link: https://www.drivethrurpg.com/product/127246/Venture-City-o-A-Superpunk-Sourcebook-for-Fate-Core
  - text: Fate de Quatro Cores
    link: http://rpgista.com.br/2017/06/25/destino-em-quatro-cores-beta-para-fate-acelerado-rpg/
  - text: Cartas Selvagens
    link: https://wildcards.fandom.com/
  - text: Dorminhoco
    link: https://wildcards.fandom.com/wiki/Sleeper
  - text: Tartaruga
    link: https://wildcards.fandom.com/wiki/Turtle
  - text: Excalibur
    link: https://marvel.fandom.com/wiki/Excalibur_(Earth-616)
  - text: Atomic Robo
    link: https://www.evilhat.com/home/atomic-robo/
  - text: Fudge em Português
    link: https://maisquatro.files.wordpress.com/2008/08/fudge-rediagramado.pdf
  - text: Mutantes e Malfeitores
    link: https://jamboeditora.com.br/produto/mutantes-malfeitores/
  - text: Marvel Super-Heroes Clássico (TSR)
    link: https://classicmarvelforever.com/cms/
  - text: Besouro Azul
    link: https://en.wikipedia.org/wiki/Ted_Kord
episode_count: 70
episode: 53
---

E estamos de volta... Com um episódio que ao mesmo tempo revisita um clássico traz um novo produto para Supers em Fate.

Nós voltamos a falar de um dos nossos queridos, [_Wearing the Cape_][fma-wearingthecape], e falamos de um produto que basicamente traz o sistema de supers do mesmo repaginado e voltado em especial para o Fate Condensado, o _Super-Powered Fate_!

Falamos aqui sobre como ele é conceitual (para o bem e para o mal) e como em 22 páginas ele traz um sistema de Supers que é elegante e, para o bem ou para o mal, chega a ser bastante minimalista, quando comparado por exemplo a [_Daring Comics_][fma-daringcomics].

Também tecemos comentários sobre o nível de Crunch que um jogo de Supers deve ter e sobre a _Rule of Cool_ (Regra do Legal) e suas implicações ao criar jogos de supers. Também fala-se da _Default Rule_, sobre como quando você usa super-perícias e ao usar de maneira "normal" vira Medíocre (+0). 

E como perícias como _Recursos_, _Contatos_ e afins funcionam enquanto _Perícias Super-Heróica_ e como você pode apostar em usar Racarga muito alta para usar _Poder do Protagonismo_.


E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

Abaixo, a tabela dos materiais analisados até agora

| **Posição** | ***Cenário***                                         | **Mr. Mickey** | **Velho Lich** | **Cicerone** | Palomita | **Média** |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:--------:|-----------|
| **1**       | [Uprising - The Dystopian Universe RPG][fma-uprising] | 5              | 5              | 5            |          | 5         |
|             | [#iHunt][fma-ihunt]                                   | 5              | 5              | 5            |          | 5         |
| **3**       | [Masters of Umdaar][fm-mou]                           | 5              | 4,5            | 5            |          | 4.83      |
|             | [Chopstick][fm-chopstick]                             | 5              | 4,75           | 4,75         |          | 4.83      |
| **5**       | [Wearing the Cape][fma-wearingthecape]                | 4,8            | 4,75           | 4,38         |          | 4,78      |
| **6**       | [Secrets of Cats][fma-secrets]                        | 4,7            | 4,5            | 4,5          |          | 4,73      |
| **7**       | [Templo Perdido de Thur-Amon][fm-thuramon]            | 4,6            | 4,75           | 4,75         |          | 4,68      |
|             | [Jadepunk][fm-jadepunk]                               | 4,8            | 4,75           | 4,5          |          | 4,68      |
|             | [Nest][fma-nest]                                      | 4,8            | 4,5            | 4,75         |          | 4,68      |
| **10**      | [Bukatsu][fm-bukatsu]                                 | 4,75           | 4,5            | 4,5          |          | 4,58      |
| **11**      | [Boa Vizinhança/Good Neighbors][fma-nest]             | 4,7            | 4,25           | 4,75         |          | 4,56      |
| **12**      | [Daring Comics][fma-daringcomics]                     | 4,25           | 4,5            | 4,75         |          | 4,5       |
| **13**      | [Mecha vs Kaiju][fm-mechavskaiju]                     | 4,25           | 4,75           | 4            |          | 4,33      |
| **14**      | [Atomic Robo][fm-atomicrobo]                          | 4              | 4,5            | ----         |          | 4,25      |
| **15**      | [Super-Powered Fate][fm-superpoweredfate]             | 4,2            | 3,75           | 3,75         | 4        | 3,92      |
| **16**      | [Destino em Quatro Cores][fm-faequatrocores]          | 4              | 4              | 3,75         |          | 3,92      |
| **17**      | [Atomic Robo: Majestic 12][fm-atomicrobo]             | 3,5            | 3,5            | ----         |          | 3,5       |
| **18**      | [Projeto Memento][fm-memento]                         | 3,5            | 3,75           | 2,75         |          | 3,33      |
|:-----------:|-------------------------------------------------------|:--------------:|:--------------:|:------------:|:--------:|-----------|
|             | [Ferramentas de Sistema][fm-FerramentasDeSistema]     | 4              | ----           | ----         |          | 4         |
|             | [_Horror Toolkit_][fm-HorrorToolkit]                  | 4,5            | 4,5            | 2            |          | 3,6       |
|             | [Fate Condensado][fm-FateCondesando]                  | ----           | ----           | ----         |          | ---       |
|             |                                                       |                |                |              |          |           |


[fm-FateCondesando]: {% post_url podcast/2020-08-17-FateMasters52-FateCondensed %}
[fma-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt %}
[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fm-mou]: {% post_url podcast/2016-10-10-FateMasters21-AnalisaMastersOfUmdaar %}
[fm-bukatsu]: {% post_url podcast/2016-09-04-FateMasters19-AnalisaBukatsu %}
[fm-memento]: {% post_url podcast/2016-11-17-FateMasters23-AnalisaProjetoMemento %}
[fm-atomicrobo]: {% post_url podcast/2016-12-14-FateMasters25-AnalisaAtomicRobo %}
[fm-chopstick]:  {% post_url podcast/2017-02-13-FateMasters27-AnalisaChopstick %}
[fm-jadepunk]:  {% post_url podcast/2017-05-04-FateMasters28-AnalisaJadepunk %}
[fm-FerramentasDeSistema]:  {% post_url podcast/2017-06-26-FateMasters29-AnalisaFerramentasDeSistema %}
[fm-thuramon]:  {% post_url podcast/2017-07-24-FateMasters31-FateMastersAnalisaThurAmon %}
[fm-faequatrocores]:  {% post_url podcast/2017-10-13-FateMasters33-AnalisaFAEQuatroCores %}
[fm-superpoweredfate]:  {% post_url podcast/2020-12-14-FateMasters53-SuperPoweredFate %}
[fm-mechavskaiju]:  {% post_url podcast/2017-09-04-FateMasters32-AnalisaMechaVsKaiju %}
[fma-chopstick]: {% post_url podcast/2016-03-31-FMA11-Chopstick %}
[fma-daringcomics]: {% post_url podcast/2020-12-14-FateMasters53-SuperPoweredFate %}
[fma-wearingthecape]: {% post_url podcast/2018-03-13-FateMasters38-AnalisaWearingTheCape %}
[fma-uprising]: {% post_url podcast/2018-04-08-FateMasters39-AnalisaUprising %}
[fma-secrets]: {% post_url podcast/2018-05-23-FateMasters41-AnalisaSecretsOfCats %}
[fm-HorrorToolkit]: {% post_url podcast/2018-08-06-FateMasters42-AnalisaHorrorToolkit %}
[fma-nest]: {% post_url podcast/2019-03-26-FateMasters45-AnalisaNestBoaVizinhanca %}
[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters
