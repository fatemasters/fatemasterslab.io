---
title:  "Especial: Cicerone Adapta _Secrets of the Cats_ para _Chronicles of the Darkness_ - Episódio 01: considerações iniciais"
teaser: "O Podcast dos Algozes dos Jogadores"
layout: post
date: 2021-02-21 12:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Adaptações
explicit: no
header: no
podcast_comments: true 
hosts:
 - Luís Cavalheiro
podcast_time: "23min"
itunes:
  duration: "00:22:58"
audios:
  - OGG: https://archive.org/download/especial-cicerone-adapta-sotc-cofd-ep01/2021-02-21-EspecialCiceroneAdapta-SotC-para-CofD-ep01.ogg
  - MP3:
    - file: https://archive.org/download/especial-cicerone-adapta-sotc-cofd-ep01/2021-02-21-EspecialCiceroneAdapta-SotC-para-CofD-ep01.mp3
      size: 25333779
iaplayer: especial-cicerone-adapta-sotc-cofd-ep01
soundtrack:
- music: March of the Mind
  artist: Kevin MacLeod
  licence: https://filmmusic.io/standard-license
events:
  - time: "00:00:00"
    text: "Introdução"
  - time: "00:00:30"
    text: "Apresentação do projeto"
  - time: "00:01:00"
    text: "Por que adaptar SotC para CofD?"
  - time: "00:06:30"
    text: "O legalês obrigatório..."
  - time: "00:07:30"
    text: "Atividades do primeiro ciclo do trabalho de adaptação: considerações sobre as diferenças entre SotC e CofD"
  - time: "00:15:21"
    text: "A questão das Escolas de Magia Felina para CofD: pensamentos iniciais sobre o processo de conversão"
  - time: "00:21:17"
    text: "Avaliação do fluxograma das atividades do processo de adaptação e encerramento do episódio"
related_links:
  - text: "Livro básico do The Secrets of Cats • A World of Adventure for Fate Core (em inglês)"
    link: https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core
  - text: "Livro básico do Chronicles of the Darkness (em inglês)"
    link: https://www.drivethrurpg.com/product/168428/Chronicles-of-Darkness
episode_count: 74
episode: 1
special: "Especial: Cicerone Adapta _Secrets of the Cats_ para _Chronicles of the Darkness_"
---

Caros espectadores, sejam bem-vindos ao primeiro episódio do _Especial Cicerone Adapta_! Na esteira do [episódio 56 do podcast, sobre adaptações](http://fatemasters.gitlab.io/podcast/FateMasters56-Adaptacoes/), Lu _Cicerone_ Cavalheiro decidiu mostrar o passo-a-passo de um processo de adaptação a fim de ilustrar não só o discutido no episódio em questão, como também mostrar como lidar com as dificuldades imprevisíveis que fatalmente serão encontradas.

Diferentemente do que seria esperado, entretanto, o Cicerone decidiu adaptar um dos _Worlds of Adventure_ mais aclamado, [_The Secrets of the Cats_](https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core), para [_Chronicles of the Darkness_](https://www.drivethrurpg.com/product/168428/Chronicles-of-Darkness). Em seu entendimento, os gatos domésticos em _The Secrets of the Cats_ possuem traços e características que os tornam personagens interessantes de se jogar no Mundo das Trevas.

Neste primeiro episódio, Cicerone discutirá suas primeiras impressões do processo de adaptação, bem como exporá quais são seus primeiros pensamentos sobre o processo de adaptação.

Se você quiser entrar em contato com o Cicerone, procure-o nos links abaixo:

+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

**Créditos**

March of the Mind by Kevin MacLeod

Link: https://incompetech.filmmusic.io/song/4020-march-of-the-mind

License: https://filmmusic.io/standard-license
