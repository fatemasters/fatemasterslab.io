---
title:  "Especial: Cicerone Adapta _Secrets of the Cats_ para _Chronicles of the Darkness_ - Episódio 02: estrutura geral"
teaser: "O Podcast dos Algozes dos Jogadores"
layout: post
date: 2021-03-07 12:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Adaptações
explicit: no
header: no
podcast_comments: true 
hosts:
 - Luís Cavalheiro
podcast_time: "23min"
itunes:
  duration: "00:22:49"
audios:
  - OGG: https://archive.org/download/especial-cicerone-adapta-sotc-cofd-ep-02/2021-02-22-EspecialCiceroneAdapta-SotC-para-CofD-ep02.ogg
  - MP3:
    - file: https://archive.org/download/especial-cicerone-adapta-sotc-cofd-ep-02/2021-02-22-EspecialCiceroneAdapta-SotC-para-CofD-ep02.mp3
      size: 26361145
iaplayer: especial-cicerone-adapta-sotc-cofd-ep-02
soundtrack:
- music: March of the Mind
  artist: Kevin MacLeod
  licence: https://filmmusic.io/standard-license
events:
  - time: "00:00:00"
    text: "Introdução e recapitulação dos episódios anteriores"
  - time: "00:01:56"
    text: "Criação de Personagem, Primeiro Passo: Conceito"
  - time: "00:03:20"
    text: "Criação de Personagem, Segundo Passo: Âncoras (Virtude, Vício, Nome Verdadeiro e Fardo)"
  - time: "00:10:23"
    text: "Criação de Personagem, Terceiro, Quarto e Quinto Passos: Atributos, Perícias, Especializações em Perícia (_en passant_)"
  - time: "00:11:08"
    text: "Criação de Personagem, Sexto Passo: Méritos (incluindo Escolas de Magia Felina e Práticas Mágicas), o Inefável (característica de resistência sobrenatural), e Especialização em Escolas de Magia Felina"
  - time: "00:18:38"
    text: "Recapitulação do apresentado até então"
  - time: "00:19:30"
    text: "Criação de Personagem, Sétimo Passo: Vantagens"
  - time: "00:20:22"
    text: "Apresentação do fluxograma de tarefas previstas e encerramento do episódio"
related_links:
  - text: "Livro básico do The Secrets of Cats • A World of Adventure for Fate Core (em inglês)"
    link: https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core
  - text: "Livro básico do Chronicles of the Darkness (em inglês)"
    link: https://www.drivethrurpg.com/product/168428/Chronicles-of-Darkness
  - text: "Livro básico de Vampire: the Requiem, 2nd edition"
    link: https://www.drivethrurpg.com/product/123898/Vampire-The-Requiem-2nd-Edition?term=vampire+the+requiem+2nd+edition
episode_count: 75
episode: 2
special: "Especial: Cicerone Adapta _Secrets of the Cats_ para _Chronicles of the Darkness_"
---

Caros espectadores, sejam bem-vindos ao segundo episódio do _Especial Cicerone Adapta_! Na esteira do [episódio 56 do podcast, sobre adaptações](http://fatemasters.gitlab.io/podcast/FateMasters56-Adaptacoes/), Lu _Cicerone_ Cavalheiro decidiu mostrar o passo-a-passo de um processo de adaptação a fim de ilustrar não só o discutido no episódio em questão, como também mostrar como lidar com as dificuldades imprevisíveis que fatalmente serão encontradas.

Diferentemente do que seria esperado, entretanto, o Cicerone decidiu adaptar um dos _Worlds of Adventure_ mais aclamado, [_The Secrets of the Cats_](https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core), para [_Chronicles of the Darkness_](https://www.drivethrurpg.com/product/168428/Chronicles-of-Darkness). Em seu entendimento, os gatos domésticos em _The Secrets of the Cats_ possuem traços e características que os tornam personagens interessantes de se jogar no Mundo das Trevas.

Neste segundo episódio, Cicerone narrará seus avanços no processo de adaptação, bem como as decisões de _design_ tomadas a fim de saber como traduzir/transladar/adaptar _The Secrets of the Cats_ para _Chronicles of the Darkness_.

Se você quiser entrar em contato com o Cicerone, procure-o nos links abaixo:

+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

**Créditos**

March of the Mind by Kevin MacLeod

Link: https://incompetech.filmmusic.io/song/4020-march-of-the-mind

License: https://filmmusic.io/standard-license
