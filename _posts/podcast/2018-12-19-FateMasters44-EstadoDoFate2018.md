---
title: "Fate Masters Episódio 44 - Estado do Fate 2018"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2018-12-19 15:30:00 -0200
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Estado do Fate
header: no
podcast_comments: true 
hosts:
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
 - "_Vilão Especialmente Convidado:_ Matheus Funfas (Solar Entretenimento)"
podcast_time: "69min"
itunes:
  duration: "01:09:26"
audios:
 - OGG: https://archive.org/download/FateMasters44EstadoGeralFate2018/FateMasters44-EstadoGeralFate2018.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters44EstadoGeralFate2018/FateMasters44-EstadoGeralFate2018.mp3
     size: 65169408
iaplayer: FateMasters44EstadoGeralFate2018
youtube: kKdgCy-aGcs
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:02:01"
    text: O Estado do Fate de 2018
episode_count: 53
episode: 44
---


Encerrando 2018, os Fate Masters fazem seu Estado do Fate 2018, gravado ao vivo: uma sessão de perguntas e respostas sobre Fate no Brasil, no mundo e sobre RPG em geral, envolvendo os Fate Masters Luis Cavalheiro e Rafael Meyer e contando com a presença do Algoz especialmente convidado Matheus Funfas, da Solar!

Falamos sobre novos cenários, novidades e tudo sobre o mundo do Fate.

Também foram convidados o Igor Moreno da Flying Ape, o Aislan da Gentle Ogre e o Fábio Silva, da Pluma Editora, mas infelizmente eles tiveram problema de agenda e não puderam participar.

Agradecemos a todos que participaram ao vivo e a todos os ouvintes, e preparem-se que em 2018 continua o lema ___"Quanto Mais Fate Melhor"___!

Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar na [comunidade do Google+ do Fate Masters][gplus], na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, pela [página do Fate Masters no Facebook][fb-page] e agora pelo [servidor do Movimento Fate Brasil no Discord][fb-discord]

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[spaces]: https://goo.gl/spaces/gFqsaUsaSJN1boHH9
[fb-page]: https://www.facebook.com/fatemasterspodcast/
[fb-discord]: http://bit.ly/DiscordFateMasters

