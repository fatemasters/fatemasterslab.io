---
title: Fate Masters Episódio 3 - Habilidades E Abordagens
subheadline: Testando a paciência dos jogadores
teaser: O Podcast dos Algozes dos Jogadores!
layout: post
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
header: no
podcast_comments: true 
podcast_time: "61min"
itunes:
  duration: "01:01:47"
hosts:
 - Fábio Emilio Costa
 - Rafael Sant'Anna Meyer
 - Filipe Dalmatti Lima
 - Leonardo Paixão (*Vilão Especialmente Convidado*)
audios:
 - OGG: https://archive.org/download/FateMasters3HabilidadesEAbordagens/Fate%20Masters%20%233%20-%20Habilidades%20e%20abordagens.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters3HabilidadesEAbordagens/Fate%20Masters%20%233%20-%20Habilidades%20e%20abordagens.mp3
     size: 59319638
iaplayer: FateMasters3HabilidadesEAbordagens
episode_count: 3
episode: 3
---

E os  Fate Masters estão de  volta. Dessa vez Rafael,  Fábio e Filipe,
além  do vilão  especialmente  convidado Leonardo  Paixão dissecam  as
mecânicas de  Habilidades, Perícias, Abordagens,  e como lidar  com as
possíveis apelações

