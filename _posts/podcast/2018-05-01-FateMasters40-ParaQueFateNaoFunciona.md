---
title: "Fate Masters Episódio 40 - Para que Fate não funciona"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2018-05-01 15:00:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
podcast_time: "85min"
itunes:
  duration: "01:13:14"
audios:
 - OGG: https://archive.org/download/FateMasters40ParaQueFateNaoFunciona/FateMasters40-ParaQueFateNaoFunciona.ogg
 - MP3: 
   - file: https://archive.org/download/FateMasters40ParaQueFateNaoFunciona/FateMasters40-ParaQueFateNaoFunciona.mp3
     size: 70328320
iaplayer: FateMasters40ParaQueFateNaoFunciona
related_links:
- text: Formulário para o _playtest_ de Fate of Cthulhu
  link: https://docs.google.com/forms/d/e/1FAIpQLSdJqxdvW3qmB2hWiUKxKHRIRKeCrOvlLxalOjul_8pmBnEDWg/viewform
- text: Apotheosis Drive/X
  link: http://www.drivethrurpg.com/product/117506/Apotheosis-Drive-X--FatePowered-Mecha-RPG--SD-MIX
- text: Daring Comics
  link: http://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
- text: Masters of Umdaar
  link: https://www.drivethrurpg.com/product/155458/Masters-of-Umdaar--A-World-of-Adventure-for-Fate-Core
- text: Mindjammer
  link: http://mindjammerpress.com/mindjammer/
- text: Nest
  link: http://drivethrurpg.com/product/153980/Nest--A-World-of-Adventure-for-Fate-Core
- text: Secrets of the Cats
  link: http://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats--A-World-of-Adventure-for-Fate-Core
- text: Strays
  link: http://www.drivethrurpg.com/product/169261/Strays
- text: Tianxia
  link: http://www.drivethrurpg.com/product/126883/Tianxia-Blood-Silk--Jade
- text: Tianxia (Site da Vigilance Press)
  link: http://www.vigilancepress.com/tianxia/
- text: Numenera
  link: http://newordereditora.com.br/categoria-produto/rpg/numenera/
- text: Call of Cthulhu
  link: https://www.chaosium.com/call-of-cthulhu-rpg/
- text: Rastro de Cthulhu
  link: http://www.retropunk.net/editora/rpg/rastro-de-cthulhu/
- text: Uprising - The Dystopian Universe RPG
  link: https://www.kickstarter.com/projects/evilhat/uprising-the-dystopian-universe-rpg 
events:
- time: "00:00:10"
  text: Introdução e um chamado às armas para participação no _playtest_ de Fate of Cthulhu
- time: "00:03:03"
  text: O debate sobre _o que não Funciona em Fate_
- time: "01:08:55"
  text: Considerações Finais
episode_count: 51
episode: 40
---

E temos mais um Fate Masters!

Em um Fate Masters mais solto e frouxo, o Velho Lich Rafael, o Cicerone Luís e o Mr. Mickey Fábio se unem para debater um assunto polêmico de se tratar em _qualquer_ comunidade de RPG: para que Fate _**NÃO funciona**_.

Em um debate mais leve e descontraído, falamos sobre várias coisas que podem influenciar: jogos com muitos combates, cenários excessivamente "comuns", falta de preparação e conversação sobre os conceitos que o jogo irá abordar, metatrama complexa ou vaga demais, entre outras coisas.

Além disso, convidamos a todos os ouvintes a participar do _playtest_ do [Fate of Cthulhu][fate-cthulhu] da Evil Hat. Vamos mostrar que no Brasil também tem jogadores e narradores de Fate.

Lembrem-se: qualquer  dúvidas, críticas, sugestões  e opiniões você pode enviar na [comunidade do Google+ do Fate Masters][gplus], na [comunidade do Facebook do Fate (com a hashtag _#fatemasters_)][fb], pelo email <fatemasterspodcast@gmail.com>, e pela [nova página do Fate Masters no Facebook][fb-page].

Em especial, nesse episódio, comentem no _post_ desse episódio no Facebook, para trazer as suas experiências em Fate de modo podermos complementar esse episódio.

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

[gplus]: https://plus.google.com/communities/100913016060492249875
[fb]: https://www.facebook.com/groups/faterpgbrasil/
[fb-page]: https://www.facebook.com/fatemasterspodcast/

[fate-cthulhu]: https://docs.google.com/forms/d/e/1FAIpQLSdJqxdvW3qmB2hWiUKxKHRIRKeCrOvlLxalOjul_8pmBnEDWg/viewform
