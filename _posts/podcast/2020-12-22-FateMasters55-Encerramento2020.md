---
title:  "Fate Masters Episódio 55 - Encerramento de 2020"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-12-22 10:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Encerramento de Ano
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "88min"
itunes:
  duration: "01:27:23"
audios:
  - OGG: https://archive.org/download/fate-masters-55-encerramento-2020/FateMasters55-Encerramento2020.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-55-encerramento-2020/FateMasters55-Encerramento2020.mp3
      size: 111470592
iaplayer: fate-masters-55-encerramento-2020
soundtrack:
- music: Time Travellin' Nancy
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/time-travellin-nancy
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
related_links:
  - text: "A Resenha do Cronistas das Trevas para #iHunt"
    link: http://cronistasdastrevasbr.com/2020/12/09/review-ihunt-the-rpg-olivia-hill-e-filamena-young/
  - text: Uma cerveja antes do fim do mundo
    link: https://ovelhinhodorpg.wordpress.com/2015/12/31/uma-aventura-para-o-fim-de-ano/
  - text: NORAD Santa Tracker
    link: https://www.noradsanta.org/
  - text: Palestra de Segurança em mesa do Cicerone na Cantina dos Heróis no Twitch
    link: https://www.twitch.tv/videos/835533367
  - text: A história do Elf in the Shelf
    link: https://www.huffpost.com/entry/the-elf-on-the-shelf-history_n_5a24c89be4b0a02abe920d71
episode_count: 72
episode: 55
---

Quase sem edição e ___TOTALMENTE SEM FREIO___!

Os Fate Masters Rafael, Luiz, Fábio e Maína se encontram para gravar um podcast solto para reavaliar o que foi o ano de 2020, um dos anos mais duros não apenas para os Fate Masters, mas para o mundo como um todo. Comentamos tudo que foi cagado no ano, tudo que foi tenso, como foi lidar com jogos _online_, fazemos críticas à comunidade, desabafamos as agruras de narrar _online_ e não ter jogadores, o que cada um de nós fez de bom...

...e nos perguntamos o que aconteceu: gato jogando _SimCity_ no Nightmare, Jumanji, Cards against Humanity ou viramos simplesmente um Domínio de Raveloft?

E aproveitamos para fazer um convite a todos para uma _call_ conjunta da comunidade: é um convite para vocês conhecerem os Fate Masters e trocar ideias e desabafar toda a merda que foi esse ano. Será dia 27 à noite: os horários serão definidos em breve e anunciados nas nossas redes sociais do Podcast no Facebook

E por fim... O _Elf in the Shelf_ está observando os ouvintes do Fate Masters!

![Elf in the shelf](/assets/img/elf-on-the-shelfng-book-doll-wikipedia-pages-ideas-story-for-kids.jpeg)

![O Filme de Elf in the shelf](/assets/img/elfintheshelf.jpg)



E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)
