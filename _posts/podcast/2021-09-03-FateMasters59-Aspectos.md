---
title:  "Fate Masters Episódio 59 - Explicando Aspectos"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-09-03 17:56:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Explicando 
 - Aspectos
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "135min"
itunes:
   duration: "02:15:58"
audios:
  - OGG: https://archive.org/download/fate-masters-episodio-59-aspectos/FateMastersEpisodio59-Aspectos.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-episodio-59-aspectos/FateMastersEpisodio59-Aspectos.mp3
      size: 46364672
iaplayer: fate-masters-episodio-59-aspectos
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:41"
    text: Leitura de _email_ do André Soares Nascimento
  - time: "00:06:58"
    text: Conceitualizando Aspectos
  - time: "00:20:16"
    text: Usos para os Aspectos
  - time: "00:31:54"
    text: Porquê tudo pode TER inerentemente um Aspecto, vendo no sistema de _Extras_ de _Shadow of the Century_
  - time: "00:38:09"
    text: Sendo um pouco mais claro sobre como os Aspectos "recortam" o que pode ou não acontecer no jogo
  - time: "00:40:45"
    text: Os principais tipos de Aspectos, começando pelo _Conceito_ 
  - time: "00:45:48"
    text: _Dificuldade_ e a importância de delimitarmos ele corretamente, além de como ele não é um impedimento, mas um complicador
  - time: "00:53:25"
    text: _Relacionamentos_ e o _Trio de Fases_, aquilo que liga personagens entre si e/ou ao cenário
  - time: "01:04:13"
    text: Outra formas de Aspectos importantes, como o sistema de Listas de Aspectos e Aspectos Temáticos, Aspectos Atrelados a Perícias ou Esferas, Perguntas que Geram Aspectos, e a Tensão e Sétimo Aspecto do Loose Threads
  - time: "01:21:23"
    text: Nomeando Aspectos 
  - time: "01:34:46"
    text: Porquê Aspectos devem descrever mais de uma coisas
  - time: "01:50:18"
    text: Usos Mecânicos dos Aspectos, incluindo Pontos de Destino e Descrições Narrativas, Limitação de Contextos, Regras que Entram em Jogo pelos Aspectos e Permissões
  - time: "02:09:56"
    text: Considerações Finais
related_links:
  - text: Definição de Aspecto no Fate Básico
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/aspectos-e-pontos-de-destino/
  - text: Definição de Aspecto no Fate Acelerado
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-acelerado/aspectos-e-pontos-de-destino/
  - text: Definição de Aspecto no Fate Condensado
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-condensado/comecando/#aspectos
  - text: Twitch do André, de quem lemos o _email_
    link: http://twitch.tv/adambravo_79
  - text: Wearing the Cape, o RPG
    link: https://www.drivethrurpg.com/product/210012/Wearing-the-Cape-The-Roleplaying-Game
  - text: Boa Vizinhança (em Inglês)
    link: https://evilhat.itch.io/good-neighbors-a-world-of-adventure-for-fate-core
  - text: "Uprising: The Dystopian Universe RPG"
    link: https://evilhat.itch.io/uprising-the-dystopian-universe-rpg
  - text: "Episódio do Fate Masters sobre Uprising"
    link: /podcast/FateMasters39-AnalisaUprising/
  - text: "Episódio do Fate Masters sobre Accessibility Toolkit"
    link: /podcast/FateMasters58-AnalisaFAcT/
  - text: "Episódio do Fate Masters sobre #iHunt" 
    link: /podcast/FateMasters51-AnalisaiHunt/	
  - text: Sobre a teoria GNS
    link: https://en.wikipedia.org/wiki/GNS_theory
  - text: Calvinball
    link: https://tvtropes.org/pmwiki/pmwiki.php/Main/Calvinball
  - text: SLIP (em Inglês)
    link: https://evilhat.itch.io/slip-a-world-of-adventure-for-fate-core
  - text: Daring Comics
    link: http://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
  - text: Breakfast Cult
    link: https://breakfastcult.com/about/
  - text: Willamina Rabbitt, a Coelha Branca (Breakfast Cult)
    link: /personagens/WillaRabbitt/
  - text: Aubergine, o Coelho Branco (Loose Threads)
    link: /personagens/Aubergine/
  - text: Scott Pilgrim Contra O Mundo 
    link: https://pt.wikipedia.org/wiki/Scott_Pilgrim_contra_o_Mundo
episode_count: 79
episode: 79
---

E os Fate Masters estão de volta, e em algo muito importante!

E além disso, comentamos o _email_ que recebemos do André Soares Nascimento, sobre o [Episódio Anterior][fm-FAcT], mostrando que sim, temos gente que realmente se sentiu representada no _Accessibility Toolkit_. E aproveitamos para deixar o [Twitch](http://twitch.tv/adambravo_79) dele, onde ele vai narrar Fate no futuro!

Fábio (o Mr. Mickey), Rafael (o Velho Lich), Maína (a Palomita) e Luíz Cavalheiro (o Cicerone) levantam o capô e procuram explicar o que são (e o que não são) Aspectos, tanto em termos até filosóficos quanto nas regras mais duras. 

Explicamos o que eles são, como eles ajudam a delimitar os parâmetros tanto do jogo quanto da narrativa, como eles fazem uma ponte entre narrativa, regras e cenário, e como no fim das contas eles, ao mesmo tempo que tornam as coisas potencialmente amplas, não permitem que o jogo vire _Calvinball_. Além disso, explicamos sobre como criar bons Aspectos, sobre seus usos e cuidados a serem tomados ao criar e nomear Aspectos.

E como tudo _pode_ ser um Aspecto, embora _nem tudo_ vai ser um Aspecto, já que Aspectos _têm importância narrativa_ no momento, desde o muro que vira um Aspecto para os personagens se protegerem de um tiroteio ao fato de você pedir para sair no X1 com o Líder de um grupo de orcs guerreiros. E como até uma _Sala típica da Baixada Fluminense_ ou mesmo um _Desodorante Avanço, que espanta vampiros e mortais igualmente_ pode ser narrativamente importantes e portanto tornaram-se Aspectos em jogo.

Sempre lembrando

1. Nem tudo é Aspecto, mas tudo pode ser (ou ter) Aspectos
2. Pontos de Destino devem ser usados
3. Aspectos são _Verdadeiros_
4. Aspectos não são Vantagens ou Desvantagens, "apenas" ***SÃO***
5. A Narração dos Aspectos é Importante
6. Fate é mais _Gramático_ (ou melhor ***Sintático***) que _Matemático_

Sobre o _Calvinball_:

![[Quadrinho de Calvin & Haroldo sobre Calvinball](/images/calvinball20127.webp)](/images/calvinball20127.webp)

Sobre a _Curiosa Menina Britânica_:

{% include youtube.html id="Ao3M9RYlsns?start=63" %}

Sobre Inigo Montoya:

![["Olá... Meu nome é Inigo Montoya. Você matou meu pai. Prepare-se para morrer."](/images/InigoMontoya.jpg)](/images/InigoMontoya.jpg)

E deixando algumas imagens do _Accessibility Toolkit_ para mostrar que ele realmente tá muito bom, agora que já não é mais um _prototype_, mas sim um produto Fate completo!

![Mesa de Jogo](/images/FAcT_Mesa.png)
![Como deve ser um Evento Inclusivo](/images/FAcT_Evento.png)
![Diversos dispositivos de Acessibilidade e suas versões em outros cenários](/images/FAcT_Dispositivos.png)
![Um exemplo em ASL de como se diz "Narrador"](/images/FAcT_ASL.png)

E, para terminar com a mensagem que começamos no episódio anterior, deixamos uma mensagem da própria Evil Hat.

![Devemos ajudar a igualar as coisas](/images/FAcT_Sidebar.png)

> ### Sidebar: Leveling the Playing Field
>
> The world won’t always be perfectly accessible. We may not always have strong lighting, we may not be able to eradicate crosstalk at our tables, or completely eradicate harmful tropes.
>
> But we can try.
>
> We can try to create better gaming spaces, create better characters, develop better systems.
>
> We can make large print character sheets, and design mechanics that represent more people. We can stop using harmful tropes, and develop more content about disabled people.
>
> The best tool I can leave you with is to ask questions of your disabled peers. Want to play a D/deaf character? Ask somebody. Want to have a visually impaired or blind player at your table? Ask them what will make the game more comfortable for them. 
>
> We didn’t cover a lot of disabilities in here, not because they aren’t important but because we only had so much space. Don’t assume that because we didn’t include them doesn’t mean they aren’t worthy of inclusion, representation, or support. Intellectually disabled gamers exist, gamers with MS, with muscular dystrophy, with ALS exist. They deserve representation and inclusion in our world.
>
> We can always do better.

> ### Mensagem: Nivelando o jogo para todos
>
> O mundo nunca vai ser totalmente acessível. Nem sempre teremos luz o bastante, nem sempre conseguiremos acabar com o bate-papo em nossas mesas, ou destrutir completamente tropos horríveis.
>
> Mas podemos tentar.
>
> Podemos criar espaços de jogo melhores, personagens melhores, sistemas melhores.
>
> Podemos criar fichas de personagem maiores, e desenvolver mecânicas que representem mais pessoas. Podemos parar de usar tropos horríveis, e desenvolver mais conteúdo sobre pessoas com deficiências.
>
> A melhor ferramenta quie eu posso dar para vocês é que vocês questionem os seus parceiros com condições. Quer jogar com um personagem Surdo/surdo? Pergunte a um deles. Quer jogar com um jogador com deficiência visual? Pergunte o que vai tornar o jogo mais confortável para eles.
>
> Não cobrimos muitas deficiências aqui, não porque elas não eram importantes, mas porque temos limitação de espaço. Não assuma que, porque não falamos sobre elas, isso significa que elas não mereçam inclusão, suporte ou representatividade. Jogadores com dificuldades intelectuais existem, personagems com [esclerose múltipla][EM], [distrofia múscular][DM] ou [ELA][ELA] existem. Eles merecem representatividade e inclusão no nosso mundo.
>
> Sempre podemos ser melhores.


E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

[fm-FAcT]: {% post_url podcast/2021-06-22-FateMasters58-AnalisaFAcT %}
[ELA]: https://pt.wikipedia.org/wiki/Esclerose_lateral_amiotr%C3%B3fica
[EM]: https://pt.wikipedia.org/wiki/Esclerose_m%C3%BAltipla
[DM]: https://pt.wikipedia.org/wiki/Distrofia_muscular
