---
title:  "Especial: Cicerone Adapta _Secrets of the Cats_ para _Chronicles of the Darkness_ - Episódio 03: andamento do processo"
teaser: "O Podcast dos Algozes dos Jogadores"
layout: post
date: 2021-03-08 00:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Adaptações
explicit: no
header: no
podcast_comments: true 
hosts:
 - Luís Cavalheiro
podcast_time: "17min"
itunes:
  duration: "00:17:07"
audios:
  - OGG: https://archive.org/download/especial-cicerone-adapta-sotc-cofd-ep-03/2021-03-08-EspecialCiceroneAdapta-SotC-para-CofD-ep03.ogg
  - MP3:
    - file: https://archive.org/download/especial-cicerone-adapta-sotc-cofd-ep-03/2021-03-08-EspecialCiceroneAdapta-SotC-para-CofD-ep03.mp3
      size: 21778320
iaplayer: especial-cicerone-adapta-sotc-cofd-ep-03
soundtrack:
- music: March of the Mind
  artist: Kevin MacLeod
  licence: https://filmmusic.io/standard-license
events:
  - time: "00:00:00"
    text: "Introdução e pedido de desculpas pelo hiato"
  - time: "00:00:53"
    text: "Não, eu não pesquisei ainda o licenciamento do _The Secrets of the Cats!_"
  - time: "00:01:15"
    text: "Breve recapitulação do episódio anterior e participações felinas: Pedro, Esther e Serendipe"
  - time: "00:02:08"
    text: "O que eu não preciso fazer neste momento"
  - time: "00:02:33"
    text: "Âncoras: Virtude, Vício, Nome Verdadeiro e Fardo"
  - time: "00:04:07"
    text: "Fardos, _Touchstones_, Degeneração e considerações sobre o tema"
  - time: "00:05:26"
    text: "Como a lista de Perícias ficou (participação felina: Pelúcio)"
  - time: "00:09:56"
    text: "Propósito e Degeneração"
  - time: "00:12:09"
    text: "Escolas de Magia Felina"
  - time: "00:12:43"
    text: "O Inefável, Choque de Vontades e agradecimentos às sugestões recebidas"
  - time: "00:15:43"
    text: "Propaganda gratuita para o evento do Dia Internacional da Mulher promovido pelo servidor WoD LatinAmerica"
  - time: "00:16:36"
    text: "Encerramento"
related_links:
  - text: "Livro básico do The Secrets of Cats • A World of Adventure for Fate Core (em inglês)"
    link: https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core
  - text: "Livro básico do Chronicles of the Darkness (em inglês)"
    link: https://www.drivethrurpg.com/product/168428/Chronicles-of-Darkness
  - text: "Livro básico de Vampire: the Requiem, 2nd edition"
    link: https://www.drivethrurpg.com/product/123898/Vampire-The-Requiem-2nd-Edition?term=vampire+the+requiem+2nd+edition
episode_count: 76
episode: 3
special: "Especial: Cicerone Adapta _Secrets of the Cats_ para _Chronicles of the Darkness_"
---

Caros espectadores, sejam bem-vindos ao segundo episódio do _Especial Cicerone Adapta_! Na esteira do [episódio 56 do podcast, sobre adaptações](http://fatemasters.gitlab.io/podcast/FateMasters56-Adaptacoes/), Lu _Cicerone_ Cavalheiro decidiu mostrar o passo-a-passo de um processo de adaptação a fim de ilustrar não só o discutido no episódio em questão, como também mostrar como lidar com as dificuldades imprevisíveis que fatalmente serão encontradas.

Diferentemente do que seria esperado, entretanto, o Cicerone decidiu adaptar um dos _Worlds of Adventure_ mais aclamado, [_The Secrets of the Cats_](https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core), para [_Chronicles of the Darkness_](https://www.drivethrurpg.com/product/168428/Chronicles-of-Darkness). Em seu entendimento, os gatos domésticos em _The Secrets of the Cats_ possuem traços e características que os tornam personagens interessantes de se jogar no Mundo das Trevas.

Neste terceiro episódio, Cicerone exporá o andamento do processo de adaptação.

Se você quiser entrar em contato com o Cicerone, procure-o nos links abaixo:

+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

**Créditos**

March of the Mind by Kevin MacLeod

Link: https://incompetech.filmmusic.io/song/4020-march-of-the-mind

License: https://filmmusic.io/standard-license
