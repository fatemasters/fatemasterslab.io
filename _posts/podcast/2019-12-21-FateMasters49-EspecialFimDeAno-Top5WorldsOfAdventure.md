---
title:  "Fate Masters Episódio 49 - Especial de Fim de Ano: Top 10 Worlds of Adventure"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-12-21 16:10:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
explicit: no
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Maína "Palomita" Paloma de Lima
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "34min"
itunes:
  duration: "33:57"
audios:
 - OGG: https://archive.org/download/fm49especialfimdeano2019top5worldsofadventure/FM49-EspecialFimDeAno2019-Top5WorldsOfAdventure.ogg
 - MP3: 
   - file: https://archive.org/download/fm49especialfimdeano2019top5worldsofadventure/FM49-EspecialFimDeAno2019-Top5WorldsOfAdventure.mp3
     size: 40110080 
iaplayer: fm49especialfimdeano2019top5worldsofadventure
soundtrack:
- music: Wish You a Merry Christmas
  artist: Twin Musicom
  link: http://www.twinmusicom.org/song/185/we-wish-you-a-merry-christmas
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
events:
  - time: "00:10"
    text: Introdução
  - time: "04:11"
    text: Os _World of Adventure_ do Velho Lich Rafael
  - time: "07:42"
    text: Os _World of Adventure_ da Maína, a Palomita
  - time: "15:09"
    text: Os _World of Adventure_ do Cicerone Luiz
  - time: "21:08"
    text: Os _World of Adventure_ do Mr. Mickey Fábio
  - time: "31:39"
    text: Encerramento
related_links:
  - text: Aether Sea
    link: https://evilhat.itch.io/aether-sea-a-world-of-adventure-for-fate-core
  - text: Blood on Trail
    link: https://evilhat.itch.io/blood-on-the-trail-a-world-of-adventure-for-fate-core
  - text: Ghost Planets
    link: https://evilhat.itch.io/ghost-planets-a-world-of-adventure-for-fate-core
  - text: Boa Vizinhança (Good Neighbors)
    link: https://evilhat.itch.io/good-neighbors-a-world-of-adventure-for-fate-core
  - text: Iron Street Combat
    link: https://evilhat.itch.io/iron-street-combat-a-world-of-adventure-for-fate-core
  - text: Loose Threads
    link: https://evilhat.itch.io/loose-threads-a-world-of-adventure-for-fate-core
  - text: Masters of Umdaar
    link: https://evilhat.itch.io/masters-of-umdaar-a-world-of-adventure-for-fate-core
  - text: Morts
    link: https://evilhat.itch.io/morts-a-world-of-adventure-for-fate-core 
  - text: Nest
    link: https://evilhat.itch.io/nest-a-world-of-adventure-for-fate-core
  - text: Ngen Mapu
    link: https://evilhat.itch.io/ngen-mapu-a-world-of-adventure-for-fate-core
  - text: Prism
    link: https://evilhat.itch.io/prism-a-world-of-adventure-for-fate-core
  - text: Psychedemia
    link: https://evilhat.itch.io/psychedemia-a-world-of-adventure-for-fate-core
  - text: Red Planet
    link: https://evilhat.itch.io/red-planet-a-world-of-adventure-for-fate-core
  - text: Romance in the Air
    link: https://evilhat.itch.io/romance-in-the-air-a-world-of-adventure-for-fate-core
  - text: Secrets of the Cats
    link: https://evilhat.itch.io/the-secrets-of-cats-a-world-of-adventure-for-fate-core
  - text: SLIP
    link: https://evilhat.itch.io/slip-a-world-of-adventure-for-fate-core
  - text: Three Rocketeers
    link: https://evilhat.itch.io/the-three-rocketeers-a-world-of-adventure-for-fate-core
  - text: Til Dawn
    link: https://evilhat.itch.io/til-dawn-a-world-of-adventure-for-fate-core
  - text: Ways of the Pukona
    link: https://evilhat.itch.io/the-way-of-the-pukona-a-world-of-adventure-for-fate-core
  - text: Weird World News
    link: https://evilhat.itch.io/weird-world-news-a-world-of-adventure-for-fate-core
  - text: Episódio de Mestres de Umdaar
    link: /podcast/FateMasters21-AnalisaMastersOfUmdaar
  - text: Passion de las Passiones
    link: https://www.drivethrurpg.com/product/227009/Pasion-de-las-Pasiones-Ashcan-Edition
  - text: Bukatsu
    link: https://www.dungeonist.com/marketplace/product/bukatsu/
  - text: Arquivos Paranormais
    link: https://lampiaogamestudio.wordpress.com/linha-editorial/arquivos-paranormais/
  - text: Podcast sobre _Secrets of the Cats_
    link: /podcast/FateMasters41-AnalisaSecretsOfCats/
  - text: Podcast sobre _Boa Vizinhança_
    link: /podcast/FateMasters45-AnalisaNestBoaVizinhanca/
episode_count: 58
episode: 49
---

E depois de um certo hiato, os Fate Masters Rafael, Fábio, Luiz e Maína se despedem de 2019 fazendo um especial natalino diferente e apresentando os seus _Worlds of Adventures_ favoritos, além de algumas menções honrosas:

+ ___Rafael:___
  + Ghost Planets
  + Blood on Trail
  + Psychedemia
  + Nest
  + Secrets of the Cats
  + _Menções Honrosas_
    + Masters of Umdaar
    + Til Dawn
+ ___Palomita:___
  + Red Planet
  + Loose Threads
  + Morts
  + Weird World News
  + Secrets of the Cats
  + _Menções Honrosas_
    + Bukatsu
    + Pasion de las Pasiones
    + Arquivos Paranormais
+ ___Cicerone:___
  + Aether Sea
  + Red Planet
  + Loose Threads
  + Master of Umdaar
  + Secrets of the Cats
  + _Menções Honrosas_
    + Nest
    + Morts
    + Weird World News
+ ___Fábio:___
  + Weird World News
  + Red Planet
  + Romance in the Air
  + Boa Vizinhança
  + Three Rocketeer
  + _Menções Honrosas_
    + Ngen Mapu
    + Ways of the Pukona
    + Prism
    + SLIP
    + Til Dawn

As redes sociais do Fate são:

- **E-mail:** <fatemasterspodcast@gmail.com>
- **Discord:** <http://bit.ly/FateBrasilDiscord>
- **WhatsApp:** <http://bit.ly/FateBrasilWhatsApp>
- **Telegram:** <http://t.me/FateBrasilTelegram>

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `Fábio Costa#9087` no [Discord](https://discordapp.com) e `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ `lcavalheiro#0520` no [Discord](https://discordapp.com) e [`lcavalheiro` no Telegram](https://t.me/lcavalheiro)
+ _**Palomita:**_ `Palomita#1312` no [Discord](https://discordapp.com) e [`mainapaloma.delima` no Facebook](https://www.facebook.com/mainapaloma.delima)

[maps-omniverse]: https://goo.gl/maps/KUoVCfrkU7GvG8FG7
[rpgworld]: https://www.facebook.com/rpgworldsite
[rpgecultura]: https://www.facebook.com/RPGCultura/
[discord-mestredosmagos]: https://discord.gg/rgK9RDk
