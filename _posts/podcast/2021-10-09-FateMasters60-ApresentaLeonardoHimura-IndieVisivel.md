---
title:  "Fate Masters Episódio 60 - Apresenta Leonardo Himura (IndieVisível - #iHunt no Brasil)"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-10-09 13:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Apresenta
 - ihunt
 - IndieVisivel
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
 - Leonardo Himura (Vilão Especialmente Convidado)
podcast_time: "79min"
itunes:
   duration: "01:19:05"
audios:
  - OGG: https://archive.org/download/fate-masters-60-apresenta-leonardo-himura/FateMasters60-ApresentaLeonardoHimura-IndieVisivel.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-60-apresenta-leonardo-himura/FateMasters60-ApresentaLeonardoHimura-IndieVisivel.mp3
      size: 46364672
iaplayer: fate-masters-60-apresenta-leonardo-himura
soundtrack:
- music: Por el Suelo
  artist: Manu Chao
  link: https://www.youtube.com/watch?v=ulwj8VHnR6s
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:02:13"
    text: Apresentando a IndieVisível Press
  - time: "00:05:35"
    text: "Por que #iHunt?"
  - time: "00:13:21"
    text: "#iHunt no Brasil terá mais coisas além de _San Jenaro_ (ou: prepare-se para caçar Lobisomens na Lapa)"
  - time: "00:18:06"
    text: "#thinkpoor no Brasil (ou seja, #sendoumfodido)"
  - time: "00:32:00"
    text: "Usando #iHunt como o jogo de _Vamos Matar o Monstro da Semana_"
  - time: "00:39:34"
    text: "Sobre as Zines de #iHunt"
  - time: "00:39:34"
    text: "Sobre as Zines de #iHunt"
  - time: "00:45:58"
    text: "Os canais da IndieVisível"
  - time: "00:49:39"
    text: "A relação da IndieVisível com a Pirataria"
  - time: "00:54:00"
    text: "Os planos futuros da IndieVisível e data do finaciamento de #iHunt"
  - time: "01:03:09"
    text: "As regiões brasileiras que estarão em #iHunt para você #serumfodido"
  - time: "01:06:31"
    text: "Considerações Finais"
related_links:
  - text: IndieVisível (Facebook)
    link: https://www.facebook.com/IndieVisivelPress
  - text: IndieVisível (Tapas)
    link: http://tapas.io/indievisivelpress
  - text: IndieVisível (Site)
    link: http://www.indievisivelpress.com
  - text: IndieVisível Press (Instagram)
    link: https://www.instagram.com/indievisivelpress/
  - text: IndieVisível Press (Twitter)
    link: https://twitter.com/IndieVisivelP
  - text: IndieVisível Press (Catarse)
    link: https://www.catarse.me/users/912207
  - text: Episódio de #iHunt
    link: http://fatemasters.gitlab.io/podcast/FateMasters51-AnalisaiHunt/
  - text: Hikaru no Go
    link: https://pt.wikipedia.org/wiki/Hikaru_no_Go
  - text: Yuri!!! On Ice
    link: https://pt.wikipedia.org/wiki/Yuri!!!_on_Ice
  - text: Dumpster Diving
    link: https://en.wikipedia.org/wiki/Dumpster_diving
  - text: iZombie
    link: https://pt.wikipedia.org/wiki/IZombie
  - text: Santa Clarita Diet
    link: https://en.wikipedia.org/wiki/Santa_Clarita_Diet
  - text: True Blood
    link: https://en.wikipedia.org/wiki/True_Blood
  - text: Don Bluth
    link: https://en.wikipedia.org/wiki/Don_Bluth
  - text: Hallmark Channel
    link: https://en.wikipedia.org/wiki/Hallmark_Channel
episode_count: 80
episode: 60
---

![[Pronto para descer o braço em monstros e faturar um em cima?](/images/iHuntBRBanner.jpg)](/images/iHuntBRBanner.jpg)

E os Fate Masters estão de volta em um Apresenta especial demais! E um Apresenta que _não é para **FASCISTAS!**_!

![[Nada de Fascistas](https://i1.wp.com/machineage.tokyo/wp-content/uploads/2020/03/Olivia-Hill-Rule.png?resize=400%2C768&ssl=1)](https://i1.wp.com/machineage.tokyo/wp-content/uploads/2020/03/Olivia-Hill-Rule.png?resize=1024%2C768&ssl=1)

Fábio (o Mr. Mickey), Rafael (o Velho Lich), Maína (a Palomita) e Luíz Cavalheiro (o Cicerone) falam com o nosso _Vilão Especialmente Convidado_ Leonardo Himura sobre o futuro financiamento coletivo de #iHunt pela IndieVisivel Press.

Falamos sobre quem é a IndieVisível, por que trazer o #iHunt, sobre a questão de localização _versus_ tradução, sobre novos cenários brasileiros e sobre como #serumfodido no Brasil é diferente de ser um fodido nos EUA. E sobre como Olivia Hill está pirando nisso tudo!

E sobre taras... 

... não o que você tá pensando, seu tarado, mas a forma de caçar monstros.

E aproveitamos para com esse podcast complementar o [nosso _podcast_ onde analisamos #iHunt][fm-ihunt]

E... Para dar um gostinho... Um trecho de #iHunt

> _"Na verdade, a Tainara diz que lobisomens são seus alvos preferidos, porque ela não precisa pesquisar muito. Depois das 23h, depois de trabalhar a manhã inteira no Carver's, e à tarde na loja de videogames, a última coisa que ela quer fazer é pesquisar._
>
> _Eu não a culpo. Ela diz que matar um lobisomem é excitante, e a ajuda a esquecer a dor por um tempinho. Afinal, é difícil focar na dor de uma inflamação no intestino quando você está sendo atacada por um lobisomem, ela conta em tom de piada. Humor mórbido, ela diz entre goles de café, é algo comum entre #iHunters. Todos fazem piada sobre como vão morrer um dia: sempre na caçada, nunca em casa, de doença ou pobreza._
>
>_Se eu a visse na rua, eu imaginaria que a Tainara era só mais uma pessoa tímida e sem graça. Mas vendo ela pessoalmente? Interagindo com ela? Ela tem uma confiança como eu nunca vi antes. Ela explica que é só parte do trabalho. Quando você enfrenta a morte todo dia, ser bela, recatada e do lar parece uma perda de tempo."_

\#iHunt estará em financiamento coletivo no dia 25/10 no Catarse pela IndieVisivel Press. E temos mais IBAGENS!!!

![[Capa dos Quadrinhos de #iHunt](/images/iHuntBRComic.png)](/images/iHuntBRComic.png)
![[Capa do Livro do Jogador de #iHunt](/images/iHuntBRGuia.jpg)](/images/iHuntBRGuia.jpg)

[fm-ihunt]: {% post_url podcast/2020-04-19-FateMasters51-AnalisaiHunt  %}
[fm-FAcT]: {% post_url podcast/2021-06-22-FateMasters58-AnalisaFAcT %}
[ELA]: https://pt.wikipedia.org/wiki/Esclerose_lateral_amiotr%C3%B3fica
[EM]: https://pt.wikipedia.org/wiki/Esclerose_m%C3%BAltipla
[DM]: https://pt.wikipedia.org/wiki/Distrofia_muscular
