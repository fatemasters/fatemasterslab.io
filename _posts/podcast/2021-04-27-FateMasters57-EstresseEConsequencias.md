---
title:  "Fate Masters Episódio 57 - Estresse e Consequências"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2021-04-24 21:00:00 -0300
categories: 
  - Podcast 
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
 - Adaptações
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Maína "Palomita" Paloma de Lima
 - Rafael Sant'anna Meyer
podcast_time: "85min"
itunes:
  duration: "01:25:42"
audios:
  - OGG: https://archive.org/download/fate-masters-57-estresseeconsequencias/FateMasters57-EstresseEConsequencias.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-57-estresseeconsequencias/FateMasters57-EstresseEConsequencias.mp3
      size: 100624384
iaplayer: fate-masters-57-estresseeconsequencias
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
  - time: "00:01:30"
    text: Explicando o Estresse, incluindo a recuperação e as alternativas dentro do Fate Básico, Acelerado e Condensado
  - time: "00:16:16"
    text: Explicando Consequências, incluindo a recuperação e a importância e impacto narrativo sobre elas, como todas as fontes de dano impactam igualmente nas consequências.
  - time: "00:29:00"
    text: Aplicando Estresse e Consequências no Fractal do Fate
  - time: "00:38:23"
    text: Usando Estresse e Consequências em um Fractal para indicar número de usos de um Fractal
  - time: "00:44:12"
    text: Falando de Condições como Consequências "Pré-Nomeadas"
  - time: "00:57:12"
    text: Aspectos Corrompidos de _Fate of Cthulhu_, a corrupção dos Grandes Antigos traduzidas para Fate
  - time: "01:05:36"
    text: As opções de Estresse e Consequências que os Fate Masters sugerem
  - time: "01:18:49"
    text: Considerações Finais
related_links:
  - text: "A definição de Estresse e Consequências do Fractal do Fate"
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/criando-um-extra/#atribuindo-elementos-ao-personagem
  - text: "A definição de Estresse e Consequências do Fate Básico"
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/conflitos/#estresse
  - text: "Fate of Cthulhu"
    link: https://evilhat.itch.io/fate-of-cthulhu
  - text: "Dresden Files Accelerated"
    link: https://evilhat.itch.io/dresden-files-accelerated-rpg
  - text: "Fate Accessibility Toolkit"
    link: https://evilhat.itch.io/fate-accessibility-toolkit-prototype-edition
  - text: "Fate Horror Toolkit"
    link: https://evilhat.itch.io/fate-horror-toolkit
  - text: "Fate Adversary Toolkit"
    link: https://evilhat.itch.io/fate-adversary-toolkit
  - text: "Fate Ferramentas de Sistema"
    link: https://www.dungeonist.com/marketplace/product/fate-ferramentas-sistema/
  - text: "Uprising - The Dystopian Universe RPG"
    link: https://evilhat.itch.io/uprising-the-dystopian-universe-rpg
  - text: "Daring Comics"
    link: https://www.drivethrurpg.com/product/173130/Daring-Comics-RolePlaying-Game
  - text: "Demos Corporation"
    link: https://www1.folha.uol.com.br/fsp/1994/2/26/ilustrada/10.html
  - text: "Kaninn Dragon sentando o Pau no Jiraya!"
    link: https://www.youtube.com/watch?v=yZS5FGKjU1Q
  - text: "A regra de Contadores _(Countdown)_ do _Fate Accessibility Toolkit_"
    link: https://fate-srd.com/fate-adversary-toolkit/constraints#countdowns
  - text: "O Contador Citado da aventura de _Young Centurions_"
    link: http://fabiocosta0305.gitlab.io/aventuras/AventuraPascoaYoungCenturions/#o-contador-da-transformação
  - text: "Dry Fate"
    link: http://fabiocosta0305.gitlab.io//fate/fate%20b%C3%A1sico/fate%20acelerado/hacks/minimalista/DryFate/
  - text: "Weird World News"
    link: https://evilhat.itch.io/weird-world-news-a-world-of-adventure-for-fate-core
  - text: "Sistema de Condições do Fate Ferramentas de Sistemas"
    link: http://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/condicoes/
episode_count: 77
episode: 57
---

E estamos de volta a um podcast explicando as origens!

Para falarmos sobre um conceito importante e muito mal compreendido: _Estresse e Consequências_.

Falamos sobre:

+ por que na prática Estresse e Consequências _não são_ barras de energia (até serem)
+ a importância das Consequências enquanto Aspectos aplicados no personagem
+ como o Estresse pode ser usado como um contador válido para formas de desgastes de poderes e consumíveis
+ como você pode usar isso como forma de "desabilitar" recursos consumíveis do personagem
+ como Condições podem ser uma alternativa interessante de trazer Consequências pré-criadas e que criem uma mecânica que reforce o impacto narrativo e vice-versa
+ como as Consequências serem apenas em um grupo tornar o impacto das mesmas poderosa como um todo
+ como as Consequências PODEM ocasionalmente ser usadas de maneira favoráveis aos jogadores
+ como usar um Fractal bem rápido para Contadores!

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)
