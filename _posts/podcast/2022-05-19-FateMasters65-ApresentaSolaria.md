---
title:  "Fate Masters Episódio 65 - Apresenta Solaria"
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2022-05-19 12:40:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - fate-masters-apresenta
 - Solaria
 - Solarpunk
explicit: no
header: no
podcast_comments: true
hosts:
 - Fábio Emilio Costa
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
 - Ozymandias (Vilão Especialmente Convidado)
 - Luluzinha (Vilã Especialmente Convidada)
podcast_time: "100min"
itunes:
   duration: "01:40:00"
audios:
  - OGG: https://archive.org/download/fate-masters-65-apresenta-solaria/FateMasters65-ApresentaSolaria.ogg
  - MP3:
    - file: https://archive.org/download/fate-masters-65-apresenta-solaria/FateMasters65-ApresentaSolaria.mp3
      size: 96452608
iaplayer: fate-masters-65-apresenta-solaria
soundtrack:
- music: Tonight at Eight
  artist: Shane Ivers
  link: https://www.silvermansound.com/free-music/chipstep
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Início do Podcast
  - time: "00:02:19"
    text: Introdução sobre Solaria e a ideia do _Solarpunk_ em RPG
  - time: "00:08:41"
    text: Um pouco sobre a questão de forma sobre conteúdo no _Solarpunk_ e sobre como jogos são políticos
  - time: "00:22:24"
    text: Sobre as inspirações para Solaria e sobre como tiveram contato com o Fate
  - time: "00:32:48"
    text: Definindo os pontos de partida e como escolhas podem definir totalmente o modo de se ver o mundo
  - time: "00:47:10"
    text: Entrando na parte de regras, pelos Pontos de Aprendizados, o nome dos Pontos de Destino
  - time: "01:03:29"
    text: Mais comentários sobre regras e sobre a ideia das Fazendas
  - time: "01:17:13"
    text: Os próximos passos do Solaria, incluindo Playtest e Financiamento
  - time: "01:26:28"
    text: Considerações Finais
related_links:
  - text: Dear Alice (Comercial que inspirou Solaria)
    link: https://www.youtube.com/watch?v=z-Ng5ZvrDm4
  - text: "Solarpunk: Histórias ecológicas e fantásticas em um mundo sustentável eBook Kindle"
    link: https://www.amazon.com.br/Solarpunk-Hist%C3%B3rias-ecol%C3%B3gicas-fant%C3%A1sticas-sustent%C3%A1vel-ebook/dp/B00AVWEPLC
  - text: "A Luluzinha"
    link: https://www.canva.com/design/DAErWd_jLIg/m0G88GHgYylwf-1p6sZxlQ/view?website#2
  - text: Ozymandias
    link: https://www.canva.com/design/DAE9OvT5O50/1BEmciB6TmECU2BkdqUwFw/view?website#2
  - text: Twitter do Ozymandias
    link: https://twitter.com/TheOzymandias_
  - text: Catarse do Solaria
    link: https://catarse.me/solaria
  - text: Cory Doctorow
    link: https://craphound.com/
  - text: O Fundo Do Poço No Reino Encantado
    link: https://craphound.com/down/Cory_Doctorow_-_Down_and_Out_in_the_Magic_Kingdom_Brazilian_Portuguese.htm
  - text: O Fundo Do Poço No Reino Encantado (EPUB)
    link: /assets/FundoDoPocoReinoEncantado.epub
  - text: Imagining the end of capitalism
    link: https://doctorow.medium.com/imagining-the-end-of-capitalism-567be9181bbb
  - text: Space Opera (O Livro citado pelo Mr. Mickey)
    link: https://en.wikipedia.org/wiki/Space_Opera_(Valente_novel)
  - text: Artigo da Wikipedia sobre Solarpunk
    link: https://pt.wikipedia.org/wiki/Solarpunk
  - text: Sobre Hopepunk
    link: https://www.momentumsaga.com/2019/05/o-que-e-hopepunk.html
  - text: Stardew Valley
    link: https://pt.stardewvalleywiki.com/Stardew_Valley_Wiki
  - text: Anarco-Ecologia
    link: https://www.anarquista.net/anarquismo-verde-ou-eco-anarquismo-vertentes-do-anarquismo/
  - text: Keep your hands off Eizouken!
    link: https://en.wikipedia.org/wiki/Keep_Your_Hands_Off_Eizouken!
  - text: Strays
    link: https://www.drivethrurpg.com/product/169261/Strays
  - text: Return to the Star
    link: https://festive.ninja/return-to-the-stars-science-fiction-roleplaying-game/
  - text: Til Dawn
    link: https://itch.io/queue/c/1913854/fate-worlds-pdf?game_id=400804
  - text: Ironsworn
    link: https://www.ironswornrpg.com/
episode_count: 85
episode: 65
---

E chegamos com mais um Fate Masters Apresenta

E dessa vez, falando com autores nacionais.

Os Algozes do Jogadores Rafael, Luís e Fábio chamam os vilões especialmente convidados Ozymandias e Luluzinha para falarmos sobre Solaria, um cenário para Fate Condensado a ser publicado em breve, focado em _Solarpunk_, ou seja, ainda que seja um mundo melhor, ainda tem muito o que melhorar.

Falamos sobre regras, cenários, ideias, modos de jogo, sobre o que é _Solarpunk_, sobre o problema dos _x-punk_ resumirem-se muitas vezes a forma sobre conteúdo, e sobre se um "RPG de Fazendinha" pode ser divertido, e sobre como escolhas relacionadas ao "Ano Zero" pode moldar totalmente a percepção do mundo.

Também falarmos sobre o fim do capitalismo (se conseguimos ou não o imaginar), sobre ficção científica, sobre RPGs serem políticos e sobre como isso é importante, sobre escolhas de público e sobre comerciais que não parecem ser comerciais.

E falando de comerciais que não parecem ser comerciais, abaixo está _Dear Alice_, que inspirou Solaria.

{% include youtube.html id="z-Ng5ZvrDm4" %}

E fiquem atentos para o futuro financiamento coletivo do Solaria no [Catarse](https://catarse.me/solaria)

Os contatos do Podcast:

+ Email: <fatemasterspodcast@gmail.com>
+ [Discord do Movimento Fate Brasil](https://discord.gg/7HkRXfqeJq)
+ Comunidade do [Movimento Fate Brasil no Facebook](https://www.facebook.com/groups/336649349791444)
+ Página do [Podcast no Facebook](https://www.facebook.com/fatemasterspodcast)

E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ lcavalheiro#0520 no [Discord](https://discordapp.com) e lcavalheiro no [Telegram](https://t.me/lcavalheiro)

E dos Vilões especialmente convidados:

+ [A Luluzinha](https://www.canva.com/design/DAErWd_jLIg/m0G88GHgYylwf-1p6sZxlQ/view?website#2)
+ [Ozymandias](https://www.canva.com/design/DAE9OvT5O50/1BEmciB6TmECU2BkdqUwFw/view?website#2)
