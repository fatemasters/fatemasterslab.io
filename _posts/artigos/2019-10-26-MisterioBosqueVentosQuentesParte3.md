---
date: 2019-10-26 11:00:00 -03:00
title: 'O Mistério do Bosque dos Ventos Quentes: uma campanha em três atos para "The Secrets of the Cats" – Terceiro ato: O Confronto Final!'
subheadline: 'Terceiro ato: O Confronto Final!'
teaser: 'Relato da campanha em três atos narrada por Luís "Cicerone" Cavalheiro ao longo do mês de outubro de 2019'
author: 'Luís "Cicerone" Cavalheiro'
layout: artigos
categories:
 - artigos
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - The Secrets of the Cats
 - Campanhas
 - Relato de campanha
 - Artigos
header: no
---
  
Outubro é um mês triplamente especial, pois temos tanto a festividade católica de São Francisco de Assis, o Dia das Crianças e o _Halloween_. Com isso em mente, eu[^aboutme] decidi narrar uma breve campanha, de apenas três atos, com o propósito de contemplar todas essas festividades. Aproveitarei o espaço aberto pela frutífera parceria com o [RPG World](https://www.facebook.com/rpgworldsite/) para promover esse mini-evento dentro dos eventos organizados pela equipe.  

<!-- more -->
  
# Regras da campanha
  
_O Mistério do Bosque dos Ventos Quentes_ é uma campanha em três atos para _The Secrets of the Cats_, um dos mais aclamados Mundos de Aventura para FATE RPG. Para quem não conhece, FATE é um dos principais expoentes da chamada _Nova Escola do RPG_, nascida do intuito de dar mais ênfase à narrativa e à história do que às mecânicas, regras ou números. Como carro-chefe da _Nova Escola_, ele é um sistema de regras bem descritivo, muito mais gramático do que matemático, e apresenta ao jogador uma nova forma de ver e pensar RPG. A editora responsável por ele, a [Evil Hat Productions](https://www.evilhat.com/home/) disponibiliza gratuitamente o [livro básico em inglês](http://www.evilhat.com/home/wp-content/uploads/FateCore.zip), mas os não versados em língua inglesa podem acessar todas as regras gratuita e legalmente no [FATE SRD Brasil](https://fatesrdbrasil.gitlab.io/).  
  
Além do FATE Básico, a campanha usará o Mundo de Aventura [_The Secrets of the Cats_](https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core?term=secrets+of+the+cats), disponível no esquema *Pay what you want* no DriveThruRPG.com, e os suplementos [_Feline Magic_](https://www.drivethrurpg.com/product/194834/The-Secrets-of-Cats-Feline-Magic?term=feline+magic) e [_Animals and Threats_](https://www.drivethrurpg.com/product/147773/The-Secrets-of-Cats-Animals--Threats?term=animals+and+threats), ambos também no esquema *Pay what you want*. Infelizmente para algumas pessoas, ainda não existe nenhuma previsão de tradução de *The Secrets of the Cats* para português.  
  
Mas você precisará ler isso tudo[^tamanho] para jogar? Bem, o ideal seria que você lesse, mas a resposta é não. Graças a uma parceria com o blog estrangeiro [_Up to 4 Players_](https://www.uptofourplayers.com/), os FATE Masters traduziram uma breve explicação em quadrinhos [disponível aqui](http://fatemasters.gitlab.io/assets/RegrasDoFate/RegrasDoFate-UpToFourPlayers.pdf)) com tudo que é necessário para jogar FATE Básico. Além disso, eu levarei as fichas prontas, assim poderemos focar no que interessa, que é explorar os mistérios da campanha.  
  
Tirando isso, não existem requisitos para participar da campanha. Jogadores experientes e novatos em RPG apreciarão igualmente a experiência, e não é necessário jogar desde o primeiro ato para poder jogar os atos posteriores. Dito isso, eu preciso deixar bem claras algumas limitações:  
  
1. O máximo de jogadores por sessão será seis, mais do que isso as coisas começam a ficar confusas;
2. A classificação etária é livre, e reforço que a presença de crianças poderá tornar o tom geral da aventura um pouco mais leve do que o planejamento inicial pode dar a entender;
3. Esta será uma mesa segura, então me reservo o direito de tomar quaisquer providências necessárias para garantir que todos os jogadores (eu incluso) se sentirão confortáveis.
  
## Sobre a campanha
  
_O Mistério do Bosque dos Ventos Quentes_ se passa em Jardim Olímpia, um cenário para _The Secrets of the Cats_ que criei de maneira colaborativa com os jogadores @cleedee#4558, @StarDust25#9543 e @Palomita#1312 no [servidor do Discord Movimento Fate Brasil](https://discord.gg/FnqYJv6) e cuja descrição está [provisoriamente disponível aqui](https://hackmd.io/@lcavalheiro/Hy3bBEGDB). Considerarei que todas as informações disponibilizadas no endereço eletrônico citado são válidas para esta campanha em três atos.  
  
## Fichas de personagens
  
[![Bombalurina](/assets/outubrofelino/Bombalurina_sotc.png)](http://fatemasters.gitlab.io/personagens/TheSecretsOfTheCats-Bombalurina/)

[![Cassandra](/assets/outubrofelino/Cassandra_sotc.png)](http://fatemasters.gitlab.io/personagens/TheSecretsOfTheCats-Cassandra/)

![Demeter](/assets/outubrofelino/Demeter_sotc.png)  

![Grisabela](/assets/outubrofelino/Grisabela_sotc.png)  

![Jelly](/assets/outubrofelino/Jelly_sotc.png)  

![Misfistofeles](/assets/outubrofelino/Misfistofeles_sotc.png)  

![Urrotigre](/assets/outubrofelino/Urrotigre_sotc.png)  

![Velho Deuteronômio](/assets/outubrofelino/Velho Deuteronômio_sotc.png)  

# Segundo ato: Caos em Jardim Olímpia!
  
## Sinopse
  
Após muito investigarem, os personagens descobriram uma coisa assustadora: seus _fardos_ foram mentalmente controlados, atraídos até o Bosque dos Ventos Quentes e forçados a realizar um ritual que libertou um espírito maligno há muito aprisionado. Depois de cuidar de seus _fardos_, os personagens devem encontrar e aprisionar novamente o espírito maligno.  
  
Uma vez liberto, o espírito maligno causou caos e confusão em Jardim Olímpia. Sombras pareciam observar e seguir os humanos, fantasmas outrora pacíficos agora estão irriquietos, e seres que não deveriam existir vagam pelas ruas à noite. Antes de confrontá-lo, os gatos deverão cumprir seu dever sagrado para com a humanidade e garantir que esses seres caóticos sejam postos em seus devidos lugares.  
  
Após deterem as ameaças sobrenaturais, os personagens precisarão deter _Aquele que Dorme_ de uma vez por todas. Ele foi visto fugindo para a Mansão Souza Cruz, ainda em construção. Será que nossos bravos e valorosos heróis conseguirão detê-lo?  
  
## Relato de sessão

O terceiro ato foi narrado em 26/10/2019 no evento promovido pelo [RPG World](https://www.facebook.com/rpgworldsite/) na [Biblioteca Pública Municipal Governador Leonel de Moura Brizola](http://bibliotecas.cultura.gov.br/espaco/1946/).   

No terceiro ato reuniram-se Jelly (interpretada por Rafael _Peregrino_ Wernek), Misfistofeles (interpretado por Olívia), Bombalurina (interpretado por Christian) e Cassandra (interpretado por Lucas). Após banirem _Aquele que Dorme_ de seu lar, os bravos felinos o viram fugir para Mansão Souza Cruz, ainda em construção. Anna e Pedro recomendaram que eles partissem logo antes que mais males caíssem sobre Jardim Olímpia.  

Depois de verificar se seus fardos estavam bem, os personagens partiram para a Mansão, com uma pequena pausa para pescar manjubinhas em um riacho que havia pelo caminho. Ao chegar, viram que se tratava de um lugar em construção, com materiais e ferramentas espalhados. Jelly realizou um discurso motivacional, incentivando seus companheiros a ir até o final da investigação (**Thunder, Thunder, Thundercats, HO!**[^motivacional]).  
  
Entretanto, enquanto Bombalurina, Cassandra e Misfistofeles decidiram entrar pela porta da frente, Jelly rodeou a casa em busca de energias místicas. Ela notou que havia uma barreira energética sugando energias espirituais da cidade inteira, mas isolando a casa dessas mesmas energias. Adicionalmente, Jelly sentiu que a origem da barreira era algum ponto no subsolo da mansão em construção. Após notar isso, ela procurou uma entrada para a casa, achando a janela para a futura cozinha aberta.  
  
Simultaneamente, Bombalurina, Cassandra e Misfistofeles descobriram que todas as correntes de ar da casa convergiam para um ponto específico do salão principal, formando um redemoinho. Bombalurina decidiu pular na convergência, e acidentalmente despertou um elemental do ar, guardião do salão principal à serviço d'_Aquele que Dorme_. O elemental tentou atacar Bombalurina, mas quando essa se esquivou e recuou descobriu que o espírito não poderia sair do ponto onde o redemoinho estava. De posse dessa observação, Cassandra e Misfistofeles decidiram contornar o espírito e ir para a cozinha, onde já conseguiam ver que Jelly estava, enquanto Bombalurina distraía o guardião e depois pulou sobre ele, passando ilesa.  
  
Após chegarem na cozinha e Jelly compartilhar com eles a informação sobre a barreira e sua origem, eles decidiram descer para o subsolo da mansão. Lá, viram que não havia paredes e o chão não estava cimentado ainda. Entre as colunas, os personagens encontraram um monte de terra remexida, indicando que algo fora enterrado ali, e decidiram escavar. Oculto no buraco, encontraram uma das estatuetas de pedra, esta do tamanho de um braço humano. Após deliberação conjunta, Misfistofeles quebrou a estatueta, revelando uma substância preta como petróleo e densa e viscosa como graxa, que Jelly reconheceu como sendo a essência de algum espírito maligno. Com cuidado, eles embrulharam a estátua em um pano que por ali estava e decidiram levá-la até Anna.  
  
Ao sair da mansão rumo a Chateau d'Ulthar, os personagens se deparam com Joaquim Souza Cruz, o proprietário da mansão. Porém, ele só teve tempo para dizer "Meus planos não serão impedidos por meros gatos" antes de Bombalurina atacá-lo e o nocautear. Após eliminarem a ameaça que Joaquim poderia vir a ser, os protagonistas levaram a estátua até Anna, que reconheceu sim como sendo uma âncora espirital d'_Aquele que Dorme_ e prometeu tomar providências para que ele jamais despertasse novamente. Pedro e os demais membros do Parlamento dos Sábios congratularam os personagens, que tiveram uma celebração em sua honra para comemorar mais essa vitória sobre os horrores inomináveis dos quais a humanidade nada poderia saber.  
  
# Encerramento
  
> Meus caros amigos felinos, este foi o encerramento da campanha em três atos _O Mistério do Bosque dos Ventos Quentes_ e do microevento #outubrofelino a ela associado. Nossos valorosos amigos garantiram a paz e a segurança em Jardim Olímpia... pelo menos por enquanto. Mas não pensem que esta vitória é razão para que descansemos! Os horrores inomináveis jamais dormem, espreitando enquanto aguardam pela hora certa para saírem de seus covis, e a humanidade deve permanecer na mais abençoada ignorância sobre eles. Por ora, regozijem-se, amigos felinos, pois mais uma vez seu dever sagrado foi cumprido!  
>  
> — Anna de Chateau d'Ulthar.  
>  
\#secretsofthecats #fate  

## Agradecimentos especiais
  
Misfistofeles, o único dos heróis felinos que foi interpretado nas três sessões de jogo (em 05/10 e 19/10 por Maína _Palomita_ de Lima, e em 26/10 por Olívia), é, fora de toda dúvida, o gato que merece um momento _Hall of Fame_. Como _Palomita_ havia definido uma imagem para o bravo felino, nada mais justo que a compartilhar com vocês:  
  
![misfistofeles](/assets/outubrofelino/misfistofeles.jpg)  
Misfistofeles, o Bravo  
  

---

[^aboutme]: Luís _Cicerone_ Cavalheiro é um dos membros do podcast Fate Masters (http://fatemasters.gitlab.io). Contatos: mestre dot cavalheiro at gmail dot com; Telegram: https://t.me/lcavalheiro; Discord: lcavalheiro#0520.  

[^tamanho]: Aproximadamente duzentas páginas em tamanho A5 – uma folha A4, como referência, tem o dobro do tamanho de uma folha A5.  
  
[^motivacional]: Jelly realmente puxou o brado de guerra dos Thundercats. Hilário e inesperado ao extremo, valeu a criação do aspecto _Espírito de Equipe_ com uma invocação gratuita para cada personagem.  
