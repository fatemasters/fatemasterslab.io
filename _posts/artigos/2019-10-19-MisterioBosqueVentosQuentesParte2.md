---
date: 2019-10-19 11:00:00 -03:00
title: 'O Mistério do Bosque dos Ventos Quentes: uma campanha em três atos para "The Secrets of the Cats" – Segundo ato: Caos em Jardim Olímpia!'
subheadline: 'Segundo ato: Caos em Jardim Olímpia!'
teaser: 'Relato da campanha em três atos narrada por Luís "Cicerone" Cavalheiro ao longo do mês de outubro de 2019'
author: 'Luís "Cicerone" Cavalheiro'
layout: artigos
categories:
 - artigos
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - The Secrets of the Cats
 - Campanhas
 - Relato de campanha
 - Artigos
header: no
---
  
Outubro é um mês triplamente especial, pois temos tanto a festividade católica de São Francisco de Assis, o Dia das Crianças e o _Halloween_. Com isso em mente, eu[^aboutme] decidi narrar uma breve campanha, de apenas três atos, com o propósito de contemplar todas essas festividades. Aproveitarei o espaço aberto pela frutífera parceria com o [RPG World](https://www.facebook.com/rpgworldsite/) para promover esse mini-evento dentro dos eventos organizados pela equipe.  

<!-- more -->
  
# Regras da campanha
  
_O Mistério do Bosque dos Ventos Quentes_ é uma campanha em três atos para _The Secrets of the Cats_, um dos mais aclamados Mundos de Aventura para FATE RPG. Para quem não conhece, FATE é um dos principais expoentes da chamada _Nova Escola do RPG_, nascida do intuito de dar mais ênfase à narrativa e à história do que às mecânicas, regras ou números. Como carro-chefe da _Nova Escola_, ele é um sistema de regras bem descritivo, muito mais gramático do que matemático, e apresenta ao jogador uma nova forma de ver e pensar RPG. A editora responsável por ele, a [Evil Hat Productions](https://www.evilhat.com/home/) disponibiliza gratuitamente o [livro básico em inglês](http://www.evilhat.com/home/wp-content/uploads/FateCore.zip), mas os não versados em língua inglesa podem acessar todas as regras gratuita e legalmente no [FATE SRD Brasil](https://fatesrdbrasil.gitlab.io/).  
  
Além do FATE Básico, a campanha usará o Mundo de Aventura [_The Secrets of the Cats_](https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core?term=secrets+of+the+cats), disponível no esquema *Pay what you want* no DriveThruRPG.com, e os suplementos [_Feline Magic_](https://www.drivethrurpg.com/product/194834/The-Secrets-of-Cats-Feline-Magic?term=feline+magic) e [_Animals and Threats_](https://www.drivethrurpg.com/product/147773/The-Secrets-of-Cats-Animals--Threats?term=animals+and+threats), ambos também no esquema *Pay what you want*. Infelizmente para algumas pessoas, ainda não existe nenhuma previsão de tradução de *The Secrets of the Cats* para português.  
  
Mas você precisará ler isso tudo[^tamanho] para jogar? Bem, o ideal seria que você lesse, mas a resposta é não. Graças a uma parceria com o blog estrangeiro [_Up to 4 Players_](https://www.uptofourplayers.com/), os FATE Masters traduziram uma breve explicação em quadrinhos [disponível aqui](http://fatemasters.gitlab.io/assets/RegrasDoFate/RegrasDoFate-UpToFourPlayers.pdf)) com tudo que é necessário para jogar FATE Básico. Além disso, eu levarei as fichas prontas, assim poderemos focar no que interessa, que é explorar os mistérios da campanha.  
  
Tirando isso, não existem requisitos para participar da campanha. Jogadores experientes e novatos em RPG apreciarão igualmente a experiência, e não é necessário jogar desde o primeiro ato para poder jogar os atos posteriores. Dito isso, eu preciso deixar bem claras algumas limitações:  
  
1. O máximo de jogadores por sessão será seis, mais do que isso as coisas começam a ficar confusas;  
2. A classificação etária é livre, e reforço que a presença de crianças poderá tornar o tom geral da aventura um pouco mais leve do que o planejamento inicial pode dar a entender;  
3. Esta será uma mesa segura, então me reservo o direito de tomar quaisquer providências necessárias para garantir que todos os jogadores (eu incluso) se sentirão confortáveis.  
  
## Sobre a campanha
  
_O Mistério do Bosque dos Ventos Quentes_ se passa em Jardim Olímpia, um cenário para _The Secrets of the Cats_ que criei de maneira colaborativa com os jogadores @cleedee#4558, @StarDust25#9543 e @Palomita#1312 no [servidor do Discord Movimento Fate Brasil](https://discord.gg/FnqYJv6) e cuja descrição está [provisoriamente disponível aqui](https://hackmd.io/@lcavalheiro/Hy3bBEGDB). Considerarei que todas as informações disponibilizadas no endereço eletrônico citado são válidas para esta campanha em três atos.  
  
## Fichas de personagens
  
[![Bombalurina](/assets/outubrofelino/Bombalurina_sotc.png)](http://fatemasters.gitlab.io/personagens/TheSecretsOfTheCats-Bombalurina/)  

[![Cassandra](/assets/outubrofelino/Cassandra_sotc.png)](http://fatemasters.gitlab.io/personagens/TheSecretsOfTheCats-Cassandra/)  

![Demeter](/assets/outubrofelino/Demeter_sotc.png)  

![Grisabela](/assets/outubrofelino/Grisabela_sotc.png)  

![Jelly](/assets/outubrofelino/Jelly_sotc.png)  

![Misfistofeles](/assets/outubrofelino/Misfistofeles_sotc.png)  

![Urrotigre](/assets/outubrofelino/Urrotigre_sotc.png)  

![Velho Deuteronômio](/assets/outubrofelino/Velho Deuteronômio_sotc.png)  

# Segundo ato: Caos em Jardim Olímpia!
  
## Sinopse
  
Após muito investigarem, os personagens descobriram uma coisa assustadora: seus _fardos_ foram mentalmente controlados, atraídos até o Bosque dos Ventos Quentes e forçados a realizar um ritual que libertou um espírito maligno há muito aprisionado. Depois de cuidar de seus _fardos_, os personagens devem encontrar e aprisionar novamente o espírito maligno.  
  
Entretanto, o espírito maligno está a causar caos e confusão em Jardim Olímpia. Sombras parecem observar e seguir os humanos, fantasmas outrora pacíficos agora estão irriquietos, e seres que não deveriam existir vagam pelas ruas à noite. Antes de confrontá-lo, os gatos deverão cumprir seu dever sagrado para com a humanidade e garantir que esses seres caóticos sejam postos em seus devidos lugares.  
  
## Relato de sessão

O segundo ato foi narrado em 19/10/2019 no evento promovido pelo [RPG World](https://www.facebook.com/rpgworldsite/) no [SESC Unidade São João de Meriti](http://www.sescrio.org.br/category/unidades/sesc-sao-joao-de-meriti).  

No segundo ato reuniram-se Urrotigre (interpretado por Rafael Wernek), Demeter (interpretada por Vinícius) e Misfistofeles (interpretado por Maína _Palomita_ de Lima). Após Misfistofeles inadvertidamente ter libertado _Aquele que Dorme_, eles sentiram um chamado mental orientando-os a se juntar a uma reunião de emergência em Chateau d'Ulthar.  
  
Ao chegarem, eles viram que quase todos os gatos da cidade haviam atendido ao chamado, e estavam freneticamente relatando manifestações sobrenaturais por toda a cidade. Urrotigre circulou entre os presentes para ouvir melhor as entrelinhas, e descobriu que todas as manifestações convergiam para um mesmo ponto: o princípio delas era marcado por redemoinhos de sombras, dos quais coisas espíritos saíam para causar caos na cidade. Ao descobrir isso, ele chamou Pedro, o Protetor, para uma conversa em local reservado, a fim de revelar a ele coisas sem causar alvoroço nos demais gatos. Demeter convenceu um dos gatos presentes a conversar com os demais e compilar os relatos para ela. Misfistofeles permaneceu o mais discreto possível, a fim de não levar uma reprimenda por suas ações desajeitadas durante os eventos no Bosque dos Ventos Quentes.  

Na reunião com Pedro, eles contaram o que havia acontecido a ele, e Urrotigre apresentou o resultado de suas análises. Pedro, pensativo, concordou que a família Sousa precisaria de maior observação. Findada a reunião, Misfistofeles decidiu ouvir os relatos dos outros gatos antes de agir. Tanto ele quanto o gato que Demeter convencera a ajudá-la descobriram que as manifestações estavam por toda Jardim Olímpia, mas elas eram mais fortes em três pontos: no playground da praça central, na clínica veterinária de Alice, e na mansão que os Souza Cruz estavam construindo. Isso fez Urrotigre disparar para sua casa. Urrotigre descobriu que os fardos estavam imersos em um sono profundo, mas se despreocupou quando conseguiu acordar sua protegida pessoal. Ele visitou os três pontos focais das manifestações, e o centro dos focos era exatamente a casa dos personagens.  

Demeter decidiu rondar pela cidade em busca de espíritos com os quais pudesse conversar e, talvez, obter informações sobre o que estava acontecendo. Ela acabou parando em uma casa abandonada, habitada por um fantasma que se apresenta como Preto Velho. Após levar uma oferenda apropriada, Preto Velho lhe contou o que sabia. Quando Anna chegou em Jardim Olímpia, muito antes de haver uma Jardim Olímpia, ela aprisionou _Aquele que Dorme_, e foi isso que permitiu que a cidade um dia surgisse. Na época, os espíritos concordaram que seria uma boa idéia permitir humanos no local, mas os anos passaram, os humanos mostraram sua inabilidade em conviver pacificamente, e agora os espíritos estariam mais dispostos a ficar do lado d'_Aquele que Dorme_. Demeter conseguiu convencer Preto Velho de que ajudaria os espíritos a se livrar dos humanos, e Preto Velho lhe disse que bastaria remover Anna da cidade que o resto _Aquele que Dorme_ providenciaria.  
  
Misfistofeles foi investigar a clínica, procurando por outras estatuetas como as que encontraram no quarto de Gabriel Sousa. Após alguma investigação, ele encontrou uma próximo à mangueira do jardim, mas quando foi pegá-la foi atacado por um dos espíritos nascidos do redemoinho. Corajosamente, o _filhote ligado na tomada_ enfrentou o espírito e destruiu a estatueta, acabando com a emanação, e descobriu dentro dela uma pedra vermelha pulsante do tamanho de um grão de feijão.  
  
Após suas ações, os personagens retornaram ao Chateau d'Ulthar a fim de comparar suas informações. Urrotigre sutilmente convidou Pedro para uma reunião privada, para a qual Misfistofeles se encaminhou furtivamente e sem ter sido convidado. Sem contar o que descobriu na casa dos seus fardos, Urrotigre relata como a casa parecia ser o ponto focal das manifestações, momento em que Misfistofeles invadiu a sala e revelou a pedra que encontrara. Pedro a analisou e viu se tratar de um _coração do poder_, e explicou que espíritos realmente poderosos não estão restritos a uma âncora no mundo físico, mas podem criar várias pedras como aquela e ter, assim, várias âncoras. Urrotigre e Misfistofeles ainda queriam saber como _Aquele que Dorme_ havia influenciado a família humana deles toda, e Pedro comentou como espíritos poderosos poderiam influenciar a mente de humanos magicamente dotados mas que nunca haviam sido treinados.  
  
Enquanto Urrotigre e Misfistofeles conversavam com Pedro, Demeter solicitou uma audiência privada para Anna, onde expôs seu plano de fingir aliança com _Aquele que Dorme_ para armar uma armadilha e assim capturá-lo. Anna, ciente de que precisaria de proteções fortes para essa idéia dar certo, foi consultar-se com Pedro, que estava a sair da reunião com Urrotigre e Misfistofeles. Após uma breve discussão, decidiu-se primeiro garantir a segurança da família Sousa. Misfistofeles garantiu que houvesse sacrifícios suficientes para todas as magias necessárias, Urrotigre combinou com Nakas e Nina que reforços deveriam ser enviados em ondas, e então partiram para a casa da família Sousa.  
  
Lá chegando, eles sentiram uma manifestação poderosa vindo da casa. Pedro e Anna começaram a traçar as proteções mágicas, enquanto os personagens foram para dentro. Lá, organizaram os demais gatos da casa a fim de livrar seus fardos de uma vez por todas: Velho Deuteronômio e Jelly iriam ajudar Pedro e Anna, Grisabela iria caminhar para os sonhos dos fardos e libertá-los de quaisquer influências, e os demais gatos iriam protegê-la.  
  
Algum tempo depois de Grisabela partir para o mundo onírico, manifestações sobrenaturais violentas começaram a acontecer dentro da casa, e _Aquele que Dorme_ apareceu diante dos personagens. Demeter tentou intimidá-lo, mas conseguiu apenas irritá-lo e revelar para ele que ela não iria cumprir com a parte dela do acordo, e _Aquele que Dorme_ a atacou, mas Urrotigre pulou na frente para tirá-la da linha do ataque, recebendo-o ele mesmo. Demeter insistiu em tentar intimidar _Aquele que Dorme_, o que levou Misfistofeles, que já estava em _Shadow Form_, a usá-la como trampolim para propelir-se em um ataque monumental contra _Aquele que Dorme_. O ataque, junto com os rituais, expulsou _Aquele que Dorme_ permanentemente da casa e da vida dos fardos dos personagens, libertou Urrotigre e fez _Aquele que Dorme_ considerar se os demais espíritos da cidade o apoiariam de fato. Na última cena, os personagens viram _Aquele que Dorme_ fugindo para a mansão em construção dos Souza Cruz, enquanto deixava cair um papel com um desenho intrincado e as iniciais da família Souza Cruz.  
  
# Teaser

O _Terceiro ato: O Confronto Final_ será narrado em 26 de outubro de 2019 no evento promovido pelo [RPG World](https://www.facebook.com/rpgworldsite/) no [SESC Unidade São João de Meriti](http://www.sescrio.org.br/category/unidades/sesc-sao-joao-de-meriti), com início previsto para 10h00min. Para jogar o terceiro ato não é necessário ter jogado nem o primeiro, nem o segundo. Aguardamos vocês lá!  

---

[^aboutme]: Luís _Cicerone_ Cavalheiro é um dos membros do podcast Fate Masters (http://fatemasters.gitlab.io). Contatos: mestre dot cavalheiro at gmail dot com; Telegram: https://t.me/lcavalheiro; Discord: lcavalheiro#0520.  

[^tamanho]: Aproximadamente duzentas páginas em tamanho A5 – uma folha A4, como referência, tem o dobro do tamanho de uma folha A5.
