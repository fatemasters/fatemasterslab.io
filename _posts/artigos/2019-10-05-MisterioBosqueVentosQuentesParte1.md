---
date: 2019-10-05 11:00:00 -03:00
title: 'O Mistério do Bosque dos Ventos Quentes: uma campanha em três atos para "The Secrets of the Cats" – Primeiro ato: o desaparecimento dos "fardos"'
subheadline: 'Primeiro ato: o desaparecimento dos "fardos"'
teaser: 'Relato da campanha em três atos narrada por Luís "Cicerone" Cavalheiro ao longo do mês de outubro de 2019'
author: 'Luís "Cicerone" Cavalheiro'
layout: artigos
categories:
 - artigos
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - The Secrets of the Cats
 - Campanhas
 - Relato de campanha
 - Artigos
header: no
---
  
Outubro é um mês triplamente especial, pois temos tanto a festividade católica de São Francisco de Assis, o Dia das Crianças e o _Halloween_. Com isso em mente, eu[^aboutme] decidi narrar uma breve campanha, de apenas três atos, com o propósito de contemplar todas essas festividades. Aproveitarei o espaço aberto pela frutífera parceria com o [RPG World](https://www.facebook.com/rpgworldsite/) para promover esse mini-evento dentro dos eventos organizados pela equipe.  

<!-- more -->
  
# Regras da campanha
  
_O Mistério do Bosque dos Ventos Quentes_ é uma campanha em três atos para _The Secrets of the Cats_, um dos mais aclamados Mundos de Aventura para FATE RPG. Para quem não conhece, FATE é um dos principais expoentes da chamada _Nova Escola do RPG_, nascida do intuito de dar mais ênfase à narrativa e à história do que às mecânicas, regras ou números. Como carro-chefe da _Nova Escola_, ele é um sistema de regras bem descritivo, muito mais gramático do que matemático, e apresenta ao jogador uma nova forma de ver e pensar RPG. A editora responsável por ele, a [Evil Hat Productions](https://www.evilhat.com/home/) disponibiliza gratuitamente o [livro básico em inglês](http://www.evilhat.com/home/wp-content/uploads/FateCore.zip), mas os não versados em língua inglesa podem acessar todas as regras gratuita e legalmente no [FATE SRD Brasil](https://fatesrdbrasil.gitlab.io/).  
  
Além do FATE Básico, a campanha usará o Mundo de Aventura [_The Secrets of the Cats_](https://www.drivethrurpg.com/product/134533/The-Secrets-of-Cats-o-A-World-of-Adventure-for-Fate-Core?term=secrets+of+the+cats), disponível no esquema *Pay what you want* no DriveThruRPG.com, e os suplementos [_Feline Magic_](https://www.drivethrurpg.com/product/194834/The-Secrets-of-Cats-Feline-Magic?term=feline+magic) e [_Animals and Threats_](https://www.drivethrurpg.com/product/147773/The-Secrets-of-Cats-Animals--Threats?term=animals+and+threats), ambos também no esquema *Pay what you want*. Infelizmente para algumas pessoas, ainda não existe nenhuma previsão de tradução de *The Secrets of the Cats* para português.  
  
Mas você precisará ler isso tudo[^tamanho] para jogar? Bem, o ideal seria que você lesse, mas a resposta é não. Graças a uma parceria com o blog estrangeiro [_Up to 4 Players_](https://www.uptofourplayers.com/), os FATE Masters traduziram uma breve explicação em quadrinhos [disponível aqui](http://fatemasters.gitlab.io/assets/RegrasDoFate/RegrasDoFate-UpToFourPlayers.pdf)) com tudo que é necessário para jogar FATE Básico. Além disso, eu levarei as fichas prontas, assim poderemos focar no que interessa, que é explorar os mistérios da campanha.  
  
Tirando isso, não existem requisitos para participar da campanha. Jogadores experientes e novatos em RPG apreciarão igualmente a experiência, e não é necessário jogar desde o primeiro ato para poder jogar os atos posteriores. Dito isso, eu preciso deixar bem claras algumas limitações:  
  
1. O máximo de jogadores por sessão será seis, mais do que isso as coisas começam a ficar confusas;  
2. A classificação etária é livre, e reforço que a presença de crianças poderá tornar o tom geral da aventura um pouco mais leve do que o planejamento inicial pode dar a entender;  
3. Esta será uma mesa segura, então me reservo o direito de tomar quaisquer providências necessárias para garantir que todos os jogadores (eu incluso) se sentirão confortáveis.  
  
## Sobre a campanha
  
_O Mistério do Bosque dos Ventos Quentes_ se passa em Jardim Olímpia, um cenário para _The Secrets of the Cats_ que criei de maneira colaborativa com os jogadores @cleedee#4558, @StarDust25#9543 e @Palomita#1312 no [servidor do Discord Movimento Fate Brasil](https://discord.gg/FnqYJv6) e cuja descrição está [provisoriamente disponível aqui](https://hackmd.io/@lcavalheiro/Hy3bBEGDB). Considerarei que todas as informações disponibilizadas no endereço eletrônico citado são válidas para esta campanha em três atos.  
  
## Fichas de personagens
  
[![Bombalurina](/assets/outubrofelino/Bombalurina_sotc.png)](http://fatemasters.gitlab.io/personagens/TheSecretsOfTheCats-Bombalurina/)  

[![Cassandra](/assets/outubrofelino/Cassandra_sotc.png)](http://fatemasters.gitlab.io/personagens/TheSecretsOfTheCats-Cassandra/)  

![Demeter](/assets/outubrofelino/Demeter_sotc.png)  

![Grisabela](/assets/outubrofelino/Grisabela_sotc.png)  

![Jelly](/assets/outubrofelino/Jelly_sotc.png)  

![Misfistofeles](/assets/outubrofelino/Misfistofeles_sotc.png)  

![Urrotigre](/assets/outubrofelino/Urrotigre_sotc.png)  

![Velho Deuteronômio](/assets/outubrofelino/Velho Deuteronômio_sotc.png)  

# Primeiro ato: o desaparecimento dos _fardos_
  
## Sinopse
  
Os personagens serão gatos que habitam na mesma casa em Jardim Olímpia, e seus _fardos_ são membros da mesma família. Há algumas semanas, eles têm agido de modo esquisito: manifestam sonambulismo, falam coisas sem nexo, fazem coisas sem sentido. Nakas, o Vidente, não consegue usar seus dons mágicos para discernir o que está a acontecer com os _fardos_, e nem Nyan, o Nomista sabe o que está acontecendo. Na ausência de informações conclusivas sobre o que está acontecendo, o Parlamento dos Sábios de Jardim Olímpia deixou aos personagens o encargo de vigiá-los.  
  
Na noite anterior ao início da aventura, os _fardos_ saíram para ir à Primeira Igreja Luterana de Jardim Olímpia, mas não só não foram para a Igreja como Anna os viu entrando no Bosque dos Ventos Quentes. Assim que amanheceu, os _fardos_ não haviam voltado. Caberá aos personagens descobrirem o que aconteceu com eles – afinal, esse é o dever sagrado de um gato para com seu _fardo_!  
  
## Relato de sessão

O primeiro ato foi narrado em 05/10/2019 no evento promovido pelo [RPG World](https://www.facebook.com/rpgworldsite/) no [SESC Unidade São João de Meriti](http://www.sescrio.org.br/category/unidades/sesc-sao-joao-de-meriti).  

No primeiro ato reuniram-se Grisabela (interpretada por Jorge Valpaços), Urrotigre (interpretado por Wallace), Demeter (interpretada por Lola) e Misfistofeles (interpretado por Maína _Palomita_ de Lima). Eles acordaram e descobriram que seus fardos não haviam voltado!  

Grisabela, uma _seeker_ experiente, usou _psicometria_ para tentar vivenciar os sonhos de seu _fardo_, a pequena Madalena Souza, e assim ter uma idéia do que aconteceu. Ela viu que em seus sonhos, a garotinha vivenciava uma espécie de chamado para o Bosque dos Ventos Quentes, e que todos em sua família a seguiam. Ela também descobriu, entre as bonecas da criança, oito bonecas (o número de membros da família Souza) posicionadas em círculo ao redor de um pedaço de papel preto amassado.  

A _seeker_ então saiu do quarto de seu _fardo_ e se deparou com Misfistofeles, o filhote _ligado na tomada_, reclamando que a vasilha de comida estava vazia. Ela explicou que os humanos estavam desaparecidos e ordenou que ele reunisse os demais gatos da casa. Demeter a princípio se recusou, pois seria uma oportunidade para ficar livre do seu _fardo_, mas prontificou-se a ajudar quando soube que estavam sem comida. Urrotigre concordou com a convocação, e todos foram encontrar a anciã na sala.  

Lá chegando, Grisabela narrou a eles suas descobertas, e após alguma deliberação eles decidiram investigar o quarto do fardo de Misfistofeles, Luís. Lá, após alguma investigação, eles encontraram lenços de papel com sangue no cesto de lixo, livros estranhos embaixo da cama e da mochila, a capa de um deles reproduzida à mão em um papel na mochila, e o mapa do Bosque dos Ventos Quentes com um _X_ marcando uma clareira[^notaimportante]. Grisabela usou seus conhecimentos em _seeking_ para descobrir que os livros e o desenho à mão se referiam a uma entidade trevosa e perigosa, e que Luís estava sonâmbulo e com o nariz sangrando quando reproduziu a capa do livro.  

A seguir eles partiram para investigar o quarto de Gabriel, o _fardo_ de Demeter. Grisabela encontrou uma estatueta pequena, de uns 5 cm de altura, embaixo da cama, mas ao tentar analisá-la despertou acidentalmente um espírito sombrio. Misfistofeles imediatamente assumiu sua _Forma de Sombra_ para se esconder, e Urrotigre pulou em um baú no quarto com o mesmo propósito. Demeter, mentirosa nata, conseguiu manipular o espírito para revelar seus propósitos: ele era um servo d'_Aquele que Dorme_, e seu propósito na casa era garantir que todos os membros da família fossem possuídos e controlados por meio de sonhos a fim de realizar um ritual no meio do Bosque cujo propósito seria libertar _Aquele que Dorme_. Demeter conseguiu também que o espírito lhe revelasse que os humanos deveriam realizar o ritual. Durante a conversa, Misfistofeles roubou de Demeter o mapa anteriormente encontrado, e atacou o espírito, conseguindo destruí-lo.  

Após uma breve investigação no quarto de Débora, o _fardo_ de Urrotigre, que revelou haver mais uma estatueta ali, eles partiram para o Bosque. Ao chegar na clareira marcada no mapa, os gatos encontraram seus humanos nus venerando um monolito de pedra negra. Grisabela conseguiu determinar que as estatuetas eram feitas do mesmo material do monolito, e então eles elaboraram um plano de ação: Grisabela iria libertar os _fardos_ no mundo onírico; Urrotigre e Demeter iriam conduzir os humanos para fora do bosque; e Misfistofeles, com o tamanho devidamente ampliado, iria enterrar o monolito com cuidado para não o quebrar. Ao cumprir sua parte no plano, Grisabela viu os _fardos_ cobertos por larvas e cultuando extasicamente um emaranhado de tentáculos sombrios. Quando Misfistofeles enterrou o monolito, o dia virou noite no Bosque.

# Teaser

O _Segundo ato: Caos em Jardim Olímpia!_ será narrado em 19 de outubro de 2019 no evento promovido pelo [RPG World](https://www.facebook.com/rpgworldsite/) no [SESC Unidade São João de Meriti](http://www.sescrio.org.br/category/unidades/sesc-sao-joao-de-meriti), com início previsto para 10h00min. Para jogar o segundo ato não é necessário ter jogado o primeiro. Aguardamos vocês lá!  

---

[^aboutme]: Luís _Cicerone_ Cavalheiro é um dos membros do podcast Fate Masters (http://fatemasters.gitlab.io). Contatos: mestre dot cavalheiro at gmail dot com; Telegram: https://t.me/lcavalheiro; Discord: lcavalheiro#0520.  

[^tamanho]: Aproximadamente duzentas páginas em tamanho A5 – uma folha A4, como referência, tem o dobro do tamanho de uma folha A5.

[^notaimportante]: Demeter, a responsável por encontrar o mapa, não possui a façanha _Letrado_, mas como foi um sucesso com estilo decidi pular uma parte prevista da aventura (os gatos levarem o mapa para Anna a fim de que ela o lesse para eles).
