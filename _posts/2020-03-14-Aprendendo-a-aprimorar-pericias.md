---
title: "Aprendendo a aprimorar perícias com um gif"
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2020-03-14 00:00:00 -0300
tags:
 - Fate
 - Fate-Condensed
 - Fate-Básico
---

No rastro do lançamento do [_Fate Condensed_](https://www.evilhat.com/home/fate-condensed/), sobre o qual falaremos em breve, temos mais uma novidade para todos vocês, caros leitores, que acompanham o podcast!    

Um dos membros do [servidor do Discord do _Movimento Fate Brasil_](https://discord.gg/FnqYJv6), [_yukiartic - Estevan_](https://discordapp.com/channels/@me/688059441714954267), não só traduziu para português e disponibilizou as fichas para _Fate Condensed_ em pdf (disponíveis [neste link](http://fatemasters.gitlab.io/Ficha-FConn-pt-BR/)), como criou uma explicação visual e animada sobre como aprimorar corretamente suas perícias em Fate!    

Confiram o trabalho fantástico dele:   

![Como aprimorar perícias](/assets/Ficha_FConn_pt-BR/APRIMORANDO_PERICIAS_PT_BR.gif)
