---
title: 'Diego "Maradona" da Silva (ele/dele)'
subheadline: A Promessa do Poder Paralelo que Conhecendo todos no Movimento caça Monstros
layout: personagens
categories:
 - personagens
comments: true
tags:
  - ihunt
  - Personagem
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                                       |
|----------------------:|:--------------------------------------------------------------------|
|              **Tara** | 66 (Recorte)                                                  |
|     **Alto Conceito** | Menino prodígio do poder paralelo  |
|             **Drama** | Rapaz mas tá caro esse negócio de se honesto, hein?                         |
| **Quadro dos Sonhos** | Sair da vida sem virar pastor pra dar uma condição melhor pra velha                                |
|           **Emprego** | De aviãozinho a motor da carga, o sonho empresarial do jovem da favela                                |
|           **Aspecto** | "Nem Deus trabalha de graça mermão, você tem que pagar a vela"                               |
|           **Aspecto** | Se tem roda, eu faço chegar do ponto A ao ponto B               |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Trambiqueiro       |                |                |                |                |
|      **Boa (+3)** | Sobrevivente | Guerrilheiro   |                |                |                |
| **Razoável (+2)** | Espião | Influencer | Organizador         |                |                |
|    **Média (+1)** | Lutador | Atleta | Ocultista | Socialite   |                |

## Manobras (Reforço: 3)

+ ***Pessoas que conhecem pessoas (da tara)***: A fundação básica do trabalho dos 66 envolve conhecer gente de todos os cantos. Quando você gastar um ponto de destino para declarar um detalhe da cena, e esse detalhe for um personagem que você conhece, essa pessoa é um especialista: ou ela tem uma habilidade relevante com +4, ou tem essa habilidade com +3 e mais um grupo pequeno de conhecidos à disposição. Além disso, crie um aspecto situacional baseado nessa personagem, com uma invocação sem custo.
+ ***Imunidade diplomática (de Recorte)***: Recortes criam laços. Agem como intermediários. Você é bom em conseguir a aceitação ou até aprovação do inimigo. Como você tende a caçar predadores sociais, você é bom em fazer com que seus superiores e colegas olhem para o outro lado. Se você criar uma vantagem relacionada à aceitação geral de um grupo (ou reconhecimento a contragosto), esse aspecto permanece enquanto for relevante, e a cada cena você ganha uma invocação de graça para esse aspecto. Além disso, toda vez que você tiver sucesso em se defender de alguém que esteja tentando abalar esse relacionamento, considere que você defendeu com estilo.
+ ***Segue o fluxo (Atleta)***: Você é um mestre de parkour. Depois que você começa, nada pode te parar. Depois da primeira ação de Atleta basada em parkour ou movimento durante uma cena, todas as próximas ganham +2.
+ ***Fuga impressionante (Espião)***: Você é muito bom em sair andando, colocando seus óculos escuros, enquanto as coisas explodem atrás de você. Quando você usar sua habilidade de Espião para criar uma vantagem como distração para uma fuga, ela também pode funcionar como ação de ataque.

## Estresse

+ Físico: `5`{: .fate_font}
+ Mental: `7`{: .fate_font}
