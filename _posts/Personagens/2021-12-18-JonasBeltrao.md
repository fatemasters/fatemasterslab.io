---
title: "Jonas Beltrão (ele/dele)"
subheadline: O Professor de Escola, e Professor Mentor de \#iHunters
layout: personagens
categories:
 - personagens
comments: true
tags:
  - ihunt
  - Personagem
language: br
header: no
---


## Aspectos

|            ***Tipo*** | ***Aspecto***                                                |
|----------------------:|:-------------------------------------------------------------|
|              **Tara** | Gothi                                                 |
|     **Alto Conceito** | Um veterano que já viu coisas demais desde que o app chegou no Brasil                          |
|             **Drama** | Os boletos nunca param de vir                                     |
| **Quadro dos Sonhos** | Próxima parada o Galeão                                     |
|           **Emprego** | Mais um escravo num sistema quebrado |
|           **Aspecto** | Eu já vi merdas demais                            |
|           **Aspecto** | Eu devo ao mundo da mesma forma que o mundo me deve |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Acadêmico        |                |                |                |                |
|      **Boa (+3)** | Influencer     | Atleta        |                |                |                |
| **Razoável (+2)** | Médico      | Ocultista   | Organizador |                |                |
|    **Média (+1)** | Sobrevivente   | Socialite    | Espião         | Hacker         |                |

## Manobras (Reforço: 2)

+ ___Raciocínio Frio (Acadêmico):___ você entende como o mundo funciona, e consegue utilizar a razão frente a influências sobrenaturais. Você pode usar _Acadêmico_ para se _Defender_ contra influências mentais sobrenaturais.
+ ___Sujeito Esguio (Atleta):___ Você não pode ser tocado se não desejar. Sempre que utilizar _Atleta_ para se esquivar de Ataques, receba +2 no rolamento
+ ___Convidado no meu canal (Influencer):___ Se você permitir, você pode permitir que outros usem sua habilidade de _Influencer_ ao invés da deles. Entretanto, se eles falharem, você divide a consequências com eles.
+ ___Você me deve uma (Tara):___ _Uma vez por episódio_, ao gastar _1 Ponto de Destino_ para trazer um elemento para a cena, você pode introduzir ao jogo um outro #iHunter na cidade que te deve um favor. O Diretor pode fazer uma ficha para o mesmo se necessário. O caçador não irá necessariamente se arriscar por você, mas irá realizar qualquer ação dentro do que seria esperado do mesmo.
+ ___Primeiro, fira (Médico):___ se você tiver disposto a mandar a ética às favas, você pode usar sua habilidade de _Médico_ no lugar de _Assassino_ quando tentar machucar alguém baseando-se no seu conhecimento médico especializado.

## Estresse

+ Físico: `7`{: .fate_font}
+ Mental: `5`{: .fate_font}
