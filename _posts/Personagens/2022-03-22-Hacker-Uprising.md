---
title: The Hacker
layout: personagens
# language: en
categories:
  - personagens
tags:
  - Uprising
  - Dystopian Universe 
header: no
---

## Aspects

|                                          __Type__ | __Aspect__                                                 |
|--------------------------------------------------:|------------------------------------------------------------|
|                                          _Class:_ | Les Citoyens/The Hacker                                    |
|       _What's your reputation in the Datasphere?_ | A ponte entre a sua felicidade e a sua realidade           |
| _What hack do you really wish you hadn't pulled?_ | Um mistério a ser resolvido e uma mentira a ser desmentida |
|  _Who do you often rely upon for meatspace help?_ | Claudia Maret, aquela que aponta quem morrerá              |
|     _Who do you want to take down a peg or town?_ | Sangue de Silicio, os colaboracionistas traidores          |
|                 _Why did you join la Résistance?_ | Trazer paz de espirito pros outros e pra mim               |

## Means

| __Mean__     | __Level__    |
|-------------:|--------------|
| _Fight_      | Average (+1) |
| _Manipulate_ | Good (+3)    |
| _Maneuver_   | Fair (+2)    |
| _Observe_    | Fair (+2)    |

## Stunts [ Refresh: 1 ] 

+ __`0`{: .fate_font}__ Because I’m __just another citizen__, once per session, I can choose to ignore the effects of the Compromised or Blacklisted conditions during the current scene. If I do so, the opposition I have to roll against to clear these conditions increases by 2.
+ __`0`{: .fate_font}__ Because I can __spoof my markers__, once per mission I can recover from Compromised immediately by giving the GM 2 blowback. 
+ __`1`{: .fate_font}__ Because I __have eyes everywhere__, I get +2 to gain the advantage with Observe, provided I can access security cameras & the like.
+ __`0`{: .fate_font}__ Because I can __spike the A/R__, I can spend a fate point to take out all blanks and agents in a single zone without rolling, provided they have active neural casings. The GM gets blowback for each of them, though.
+ __`0`{: .fate_font}__ Because I can __spoof the A/R__, I can enter any scene disguised as someone else. My disguise is an aspect with a boost on it.
+ __`1`{: .fate_font}__ Because I post __screeds on the boards__, whenever I gain an advantage related to mobilizing the hacker underground of Paris Nouveau, I get an additional boost. I can give the GM blowback to get more boosts after I roll, at a rate of 2 blowback per boost.

