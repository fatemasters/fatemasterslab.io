---
title: Notas da Sessão Zero de Uprising
layout: personagens
#language: en
categories:
  - personagens
tags:
  - Uprising
  - Dystopian Universe 
header: no
---

## Limites e Véus

+ ___Velho Lich:___ 
  + Sem violência sexual (off-screen sem personagem envolvidos)
  + Sem _gore_
  + Sem horror de corpo
  + Sem violência contra criança
+ ___Cicerone:___ 
  + Sem violência contra animais
  + Sem violência sexual (off-screen sem personagem envolvidos)
  + Sem violência contra criança
  + Racismo/xenofobia/homofobia moderada
  + Sem gore e _body horror_

---

## Faces e Locais

### O Governo

#### Aspects

|   __Type__ | __Aspect__                            |
|-----------:|---------------------------------------|
|  _Slogan:_ | Oportunidade, segurança, prosperidade |
| _Scandal:_ | Eugenia social                        |

#### Corporate

+ `0`{: .fate_font} **Taxation**: At start of prep, add a fate point to Bank.
  + `0`{: .fate_font} **Wage Negotiations**: At start of prep, add a fate point to Bank.

#### Templo de Montmartre

+ ___Alma de Paris Nouveau___
+ ___Jerome Duchamps: Mestre do Culto ao Futuro___
+ ___O Berçário do übermensch___

#### Vice-Ministro de Propaganda

+ ___Émile Candide d'Arouet, o marqueteiro-mor do governo___
+ ___Age sempre de acordo com o regulamento___
+ ___Tem telhado de vidro, mas adora jogar pedra nos telhados alheios___

---

### La Resistance

#### Aspects [ Cache: 3 ]

|     __Type__ | __Aspect__                  |
|-------------:|-----------------------------|
| _Manifesto:_ | Retorno à Realidade Natural |
|  _Weakness:_ | Ideas demais, atos de menos |

#### Intel

+ `0`{: .fate_font} **Training Archive Access**: Once per mission, before roll, discover passive opposition or means ratings of opposition.

#### Support

+ `0`{: .fate_font} **Hearts & Minds**: Tie +2 boosts to Maneuver prep advantages.

#### Um antigo ginásio na Vila dos Cem Mil

+ ___Selene Beauchamps, a comandante dos Resgateiros___
+ ___Uma das poucas Exilé com trânsito livre___
+ ___Resgateiros conhecem Paris como a palma da mão___

#### O Maestro

+ ___Organiza grandes concertos sem AR ou VR___
+ ___Lidera não presencialmente a Resistência___

---

## NPCs

### Claudia Marè

+ ***Sabe quem irá viver ou morrer***
+ ***Dançcando no Fim da Navalha***

### Hackers Colaboracionista ___Sangues de Silício___

+ ***Menos espertos do que gostam de mostrar***
+ ***Números e brinquedinhos brilhantes ao invés de habilidade***

