---
teaser: O Podcast dos Algozes dos Jogadores!
subheadline: A Ficha da Metamorfa Bombalurina para The Secrets of the Cats
title: Personagem Pronto – Bombalurina
date: 2019-10-10 12:00:00 -0300
layout: personagens
categories:
  - personagens
tags:
  - The Secrets of the Cats
header: no
---

Bombalurina é uma gatinha de pelagem frajola (como o gato homônimo do desenho animado _Piu-Piu e Frajola_) branco e laranja, de olhos verdes e queixo bege. Ela é ativa e brincalhonha, um pouco parruda, e que gosta de fazer amizades – o tipo de gato que vai se comportar como filhote para o resto da vida, mesmo que esta dure vinte anos. Ela não tem medo de nada, e por conta disso é bastante impulsiva às vezes. Seu _fardo_ é uma adolescente chamada Carmela Souza, uma jovem curiosa e com talento para fazer más amizades.  

<!-- more -->

# Aspectos

| **Conceito** | _Metamorfa[^shaper] destemida e impetuosa_ |
| ---: | :--- |
| **Dificuldade** | _Carrego o fardo da juventude_ |
| **Fardo** | _A jovem Carmela Sousa_ |
| **Nome Verdadeiro** | _Sempre jovem_ |
| **Aspecto Livre** | |

# Perícias

| **Nível** | | | | |
| ---: | :---: | :---: | :---: | :---: |
| **Ótimo (+4)** | _Atletismo_ | | | |
| **Bom (+3)** | _Metamorfose_ | _Vigor_ | | |
| **Razoável (+2)** | _Comunicação_ | _Conhecimento_ | _Percepção_ | |
| **Regular (+1)** | _Nomismo[^namer]_ | _Investigação_ | _Empatia_ | _Vontade_ |

# Façanhas comuns

- **Combate felino**: Bombalurina pode usar _Atletismo_ ao invés de _Lutar_ para realizar ações de _Ataque_ ou _Defesa_ contra oponentes localizados na mesma zona que ela.  
- **Totalmente adorável**[^loveable]: Bombalurina é tão doce e adorável que é praticamente impossível mentir para ela. Ela pode usar _Comunicação_ ao invés de _Empatia_ para detectar mentiras, exceto mentirinhas inofensivas.  
- **Assustadiça**[^skittish]: Bombalurina é tão paranoica e nervosa que reage rapidamente a qualquer ameaça que ela consiga perceber. Mesmo se ela for emboscada, Bombalurina age primeiro em um conflito.  

# Façanhas mágicas

- **Efígie**[^harm]: Bombalurina é talentosa o suficiente em Nomismo para ferir diretamente um oponente a uma zona dela. Ao falar o _Nome Verdadeiro_ do inimigo para um pequeno animal em suas patas, Bombalurina fere o animal para causar ferimentos idênticos porém maiores no alvo. Isso é um _Ataque_ com _Nomismo_, a ser defendido com _Vontade_ ou _Nomismo_. A escala de tamanho (_The Secrets of the Cats_, página 20) não se aplica a esta façanha. O pequeno animal não morrerá exceto se Bombalurina infligir uma consequência em seu alvo, e ela pode limitar ataques feitos com este poder a uma tensão de estresse. Ela pode ser desarmada se uma forçada em aspecto ou uma vantagem criada fizer com que ela acidentalmente mate o animal ou o deixe escapar.  
- **Tino para Metamorfose**[^knack]: Bombalurina é uma metamorfa mais experiente que o normal em mudar sua forma física. Para cada vez que ela adquirir esta façanha, ela pode sustenar um aspecto adicional de _Metamorfose_.  
- **Alterar Tamanho (Exclusiva)**[^change]: Bombalurina pode usar _Metamorfose_ para mudar radicalmente de tamanho pelo resto da cena, permitindo-a mover-se ao longo da escala de tamanho. A oposição aumenta em dois para cada grau da escala de tamanho entre seu tamanho atual e o tamanho desejado.  

# Estresses e Consequências

**Estresse Físico**: 1[ ] 2[ ] 3[ ] 4[ ]  
**Estresse Mental**: 1[ ] 2[ ] 3[ ]  

| **Consequências** | |
| :--- | :--- |
| **Suave (2)** | |
| **Moderada (4)** | |
| **Severa (6)** | |

# Ficha em formato .png

[![bombalurina](/assets/outubrofelino/Bombalurina_sotc.png)](http://fatemasters.gitlab.io/assets/outubrofelino/Bombalurina_sotc.png)  

---

[^shaper]: _Metamorfose_ foi o termo que escolhi para traduzir _Shaping_.  
[^namer]: _Nomismo_ foi o termo que escolhi para traduzir _Naming_.  
[^harm]: _Efígie_ foi como traduzi o nome da façanha mágica _Harm_ (_The Secrets of the Cats_, página 18).  
[^knack]: _Tino para Metamorfose_ foi como traduzi o nome da façanha mágica _A Knack for Change_ (_The Secrets of the Cats_, página 21).  
[^change]: _Alterar Tamanho_ foi como traduzi o nome da façanha mágica _Change Size_ (_The Secrets of the Cats_, página 21).  
[^loveable]: _Totalmente adorável_ foi como traduzi o nome da façanha comum _Totally Lovable_ (_The Secrets of the Cats_, página 29).  
[^skittish]: _Assustadiça_ foi como traduzi o nome da façanha comum _Skittish_ (_The Secrets of the Cats_, página 28).  
