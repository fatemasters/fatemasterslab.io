---
title: "Valdeci Andrade (ele/dele)"
subheadline: O Disneymaníaco consertador de Celular que caça Monstros
layout: personagens
categories:
 - personagens
comments: true
tags:
  - ihunt
  - Personagem
language: br
header: no
---

**Referência Visual**: ![https://i.pinimg.com/originals/b9/8a/d2/b98ad27d3bf1abb8405fd0fe0fd434d2.jpg](https://i.pinimg.com/originals/b9/8a/d2/b98ad27d3bf1abb8405fd0fe0fd434d2.jpg)


## Aspectos

|            ***Tipo*** | ***Aspecto***                                                |
|----------------------:|:-------------------------------------------------------------|
|              **Tara** | Fui (Hacker)                                                 |
|     **Alto Conceito** | Alguém mais otimista do que deveria                          |
|             **Drama** | Dúvidas demais na cabeça                                     |
| **Quadro dos Sonhos** | Disneymaníaco Inveterado                                     |
|           **Emprego** | O melhor consertador de qualquer cacareco eletrônico (MESMO) |
|           **Aspecto** | Tira mais um como personagem vivo                            |
|           **Aspecto** |                                                              |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Hacker         |                |                |                |                |
|      **Boa (+3)** | Influencer     | Criador        |                |                |                |
| **Razoável (+2)** | Acadêmico      | Investigador   | Serviço Social |                |                |
|    **Média (+1)** | Sobrevivente   | Organizador    | Espião         | Atleta         |                |

## Manobras (Reforço: 3)

+ ___Estoque de Fui (por Fui):___ Fuis lidam com ferramentas e tecnologia. Eles resolvem problemas. Se eles tiverem acesso até às ferramentas mais rudimentares, uma vez por cena eles podem eliminar um aspecto situacional sem precisar superá-lo nem nada. Eles ligam um interruptor, apertam um botão, desligam qualquer coisa, e está consertado, desde que seja uma coisa que dê para consertar com uma ferramenta.
+ ___Referência Hacker (por Hacker):___ Hackers mexem com computadores. Com essa manobra, desde que a vantagem não tenha sido estabelecida ainda, e suas ações estejam diretamente ligadas a usar um computador, você começa com a vantagem. Onde quer que você possa criar um aspecto situacional usando um sistema de computador que esteja conectado a outro personagem, qualquer tentativa de superá-lo parte de um limiar igual ao de sua habilidade Hacker. Se você estiver ativamente se opondo à ação, ganhe +2. Se você puder fazer isso sem usar um computador, você sempre pode apostar aspectos contra alguém, uma vez por cena.
+ ___Todo o Catálago da AliExpress (MESMO) (Criador):___ Pagando 1 PD, uma vez por cena, posso declarar que tenho um cacareco útil qualquer de origem duvidosa. Ele entra em jogo com uma _Invocação Gratuíta_, e o Narrador pode dar uma _Falha Estrutural_ para ele
+ ___Em caso em dúvida... C4 (Criador):___ pode usar _Criador_ para _Atacar_, mas em caso de Falha o Ataque vira ___Indiscriminado___

## Estresse

+ Físico: `5`{: .fate_font}
+ Mental: `5`{: .fate_font}
