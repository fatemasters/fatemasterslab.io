---
title: The Blueblood
layout: personagens
#language: en
categories:
  - personagens
tags:
  - Uprising
  - Dystopian Universe 
header: no
---

## Aspects

|                                               __Type__ | __Aspect__                                                      |
|-------------------------------------------------------:|-----------------------------------------------------------------|
|                                               _Class:_ | La Societé/The Blueblood                                        |
|       _Where does the Wealth and Privilege Come From:_ | Faço meu futuro baseado no passado                              |
|                 _What do you want, but you can't buy:_ | Uma vida mais simples, sem neon                                 |
| _Who do you Rely upon to get you the things you want:_ | Uma grande rede (formal e informal) de funcionários e serviçais |
|          _Who's looking to usurp your rightful place:_ | Aqueles que desprezam o passado, ou seja, quase todo mundo      |
|                      _Why did you join la Résistance?_ | Quero uma vida REALMENTE REAL                                   |


## Means

| __Mean__     | __Level__    |
|-------------:|--------------|
| _Fight_      | Average (+1) |
| _Manipulate_ | Good (+3)    |
| _Maneuver_   | Fair (+2)    |
| _Observe_    | Fair (+2)    |

## Stunts [ Refresh: 1 ]

+ `0`{: .fate_font} Because I have __great wealth__, the cost of equipment that I buy is 1  lower than usual, to a minimum of 1.
+ `0`{: .fate_font} Because __money talks__, whenever I buy equipment, I can choose to reduce the cost by 1. If I do, the GM gets 2 blowback.
+ `0`{: .fate_font} Because I __deal in favors__, when I gain the advantage in a social situation, I get an additional boost.
+ `0`{: .fate_font} Because __the camera loves me__, I can make a Manipulate- or Maneuver-based prep advantage without spending a prep action, but only if I tell the press something juicy (which gives the GM 5 blowback).
+ `1`{: .fate_font} Because I have __bodyguards__, I can choose to bring them into any conflict. If I do, they’re an aspect with a boost on it.
+ `1`{: .fate_font} Because __words cut__, when I deal harm in a social situation I can give the GM 1 blowback to force my opponent to mark an additional condition.
