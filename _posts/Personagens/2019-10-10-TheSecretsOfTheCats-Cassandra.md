---
teaser: O Podcast dos Algozes dos Jogadores!
subheadline: A Ficha da Nomista Cassandra para The Secrets of the Cats
title: Personagem Pronto – Cassandra
date: 2019-10-10 12:00:00 -0300
layout: personagens
categories:
  - personagens
tags:
  - The Secrets of the Cats
header: no
---

Cassandra é uma gata de pelo laranja, olhos verdes e o rabo permanentemente dobrado na ponta, como se estivesse quebrado. Ela é uma gata amistosa e que odeia conflitos, mas não medirá esforços para acabar com eles por meios não pacíficos se necessário – no melhor do _si vis pacem para bellum_. Seu _fardo_, João Souza, pai da família Souza, é dono de um mercado em Jardim Olímpia. Ele é um homem honesto, mas severo e conservador.  

<!-- more -->

# Aspectos

| **Conceito** | _Nomista[^namer] protetora e impaciente_ |
| ---: | :--- |
| **Dificuldade** | _Ação fala mais do que palavras_ |
| **Fardo** | _O adulto João Souza_ |
| **Nome Verdadeiro** | _Arauta da Paz_ |
| **Aspecto Livre** | |

# Perícias

| **Nível** | | | | |
| ---: | :---: | :---: | :---: | :---: |
| **Ótimo (+4)** | _Lutar_ | | | |
| **Bom (+3)** | _Nomismo_ | _Atletismo_ | | |
| **Razoável (+2)** | _Empatia_ | _Vigor_ | _Provocar_ | |
| **Regular (+1)** | _Metamorfose[^shaper]_ | _Furtividade_ | _Roubo_ | _Percepção_ |

# Façanhas comuns

- **Gingado**[^dangerous]: Cada um dos seus movimentos telegrafa suas habilidades marciais. Você pode usar _Lutar_ ao invés de _Provocar_ para intimidar.  
- **Defesa Ofensiva**[^defense]: Posicionando-se cuidadosamente, você consegue fazer seus oponentes se ferirem enquanto você se esquiva dos ataques. Quando você defende por _Atletismo_ e tem um sucesso com estilo, você pode abrir mão do impulso para causar dois estresses em seu atacante.  
- **Terror Noturno**[^night]: Você é um gato muito mais assustador à noite do que de dia. +2 em testes de _Provocar_ para causar medo à noite.  

# Façanhas mágicas

- **Efígie**[^harm]: Cassandra é talentosa o suficiente em Nomismo para ferir diretamente um oponente a uma zona dela. Ao falar o _Nome Verdadeiro_ do inimigo para um pequeno animal em suas patas, Cassandra fere o animal para causar ferimentos idênticos porém maiores no alvo. Isso é um _Ataque_ com _Nomismo_, a ser defendido com _Vontade_ ou _Nomismo_. A escala de tamanho (_The Secrets of the Cats_, página 20) não se aplica a esta façanha. O pequeno animal não morrerá exceto se Cassandra infligir uma consequência em seu alvo, e ela pode limitar ataques feitos com este poder a uma tensão de estresse. Ela pode ser desarmada se uma forçada em aspecto ou uma vantagem criada fizer com que ela acidentalmente mate o animal ou o deixe escapar.  
- **Tino para Metamorfose**[^knack]: Cassandra é uma metamorfa mais experiente que o normal em mudar sua forma física. Para cada vez que ela adquirir esta façanha, ela pode sustenar um aspecto adicional de _Metamorfose_.  
- **Animar (Exclusiva)**[^animate]: Ao sacrificar um pequeno animal e sussurrar seu _Nome Verdadeiro_ para um objeto inanimado – incluindo cadáveres –, Cassandra o infunde com energia vital até o próximo nascer ou pôr do sol. A oposição depende do tamanho do objeto, começando em _Regular (+1)_ para objetos do tamanho de um inseto e aumenta em dois para cada grau na _Escala de Tamanho_ (_The Secrets of the Cats_, página 20). Objetos animados só podem se mover conforme seus formatos lhes permitem: uma corda pode rastejar, um boneco de ação pode caminhar, mas uma estátua inflexível pode apenas balançar em sua base. Os objetos que ela animar seguirão instruções simples, mas não lutarão. Ela também pode assumir controle direto do objeto, permitindo-a executar quaisquer ações que desejar. Um objeto animado tem uma perícia em _Bom (+3)_ e uma em _Ruim (-1)_. Objetos animados podem ver e ouvir, mesmo que eles não tenham olhos ou ouvidos. A magia de animação é relativamente frágil; ela pode ser destruída atacando a energia de animação no plano astral ou atacando o objeto físico. Objetos animados possuem uma caixa de estresse. Se Cassandra obtiver um sucesso com estilo na hora de animar objeto, ele adquire uma consequência suave. Cassandra só pode animar um objeto por vez, a não ser que ela adquira a façanha _Multitarefa_[^multi].  

# Estresses e Consequências

**Estresse Físico**: 1[ ] 2[ ] 3[ ] 4[ ]  
**Estresse Mental**: 1[ ] 2[ ] 3[ ]  

| **Consequências** | |
| :--- | :--- |
| **Suave (2)** | |
| **Moderada (4)** | |
| **Severa (6)** | |

# Ficha em formato .png

[![cassandra](/assets/outubrofelino/Cassandra_sotc.png)](http://fatemasters.gitlab.io/assets/outubrofelino/Cassandra_sotc.png)  

---

[^shaper]: _Metamorfose_ foi o termo que escolhi para traduzir _Shaping_.  
[^namer]: _Nomismo_ foi o termo que escolhi para traduzir _Naming_.  
[^harm]: _Efígie_ foi como traduzi o nome da façanha mágica _Harm_ (_The Secrets of the Cats_, página 18).  
[^knack]: _Tino para Metamorfose_ foi como traduzi o nome da façanha mágica _A Knack for Change_ (_The Secrets of the Cats_, página 21).  
[^animate]: _Animar_ foi como traduzi o nome da façanha mágica _Animate_ (_The Secrets of the Cats_, página 18).  
[^dangerous]: _Gingado_ foi como traduzi o nome da façanha comum _Dangerous Moves_ (_The Secrets of the Cats_, página 27).  
[^defense]: _Defesa Ofensiva_ foi como traduzi o nome da façanha comum _Offensive Defense_ (_The Secrets of the Cats_, página 26).  
[^night]: _Terror Noturno_ foi como traduzi o nome da façanha comum _Night Terror_ (_The Secrets of the Cats_, página 28).  
[^multi]: _Multitarefa_ foi como traduzi o nome da façanha mágica _Multitasking_ (_The Secrets of the Cats_, página 19).  
