---
title: "Carina Souza (ela/dela)"
subheadline: A Kickboxer vendedora de trufas que caça monstros
layout: personagens
categories:
 - personagens
comments: true
tags:
  - ihunt
  - Personagem
language: br
header: no
---

**Referência Visual**: ![https://i.pinimg.com/236x/ec/87/80/ec878073262e80a0fe4788904546aed2.jpg](https://i.pinimg.com/236x/ec/87/80/ec878073262e80a0fe4788904546aed2.jpg)

## Aspectos

|            ***Tipo*** | ***Aspecto***                                                       |
|----------------------:|:--------------------------------------------------------------------|
|              **Tara** | Cavalo (Rebatedor)                                                  |
|     **Alto Conceito** | Lutadora que topa qualquer trabalho, qualquer bico, qualquer caçada |
|             **Drama** | Aluguel, contas, cartão... tá tudo atrasado                         |
| **Quadro dos Sonhos** | Serei uma campeã internacional de MMA                               |
|           **Emprego** | Camelô lá no trem da Central                                        |
|           **Aspecto** | Coração muito maior do que a carteira                               |
|           **Aspecto** | Desaforo não levo nem pra esquina, quem dirá pra casa               |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Lutadora       |                |                |                |                |
|      **Boa (+3)** | Atleta         | Sobrevivente   |                |                |                |
| **Razoável (+2)** | Influencer     | Criadora       | Médica         |                |                |
|    **Média (+1)** | Organizadora   | Serviço Social | Espiã          | Trambiqueira   |                |

## Manobras (Reforço: 3)

+ ***A melhor defesa (da tara)***: A manobra de Cavalo básica te ensina que a melhor defesa é um bom ataque. Como você é um Cavalo forte e resistente, você sempre pode fazer algo ameaçador e dar uma surra em algum mané. Quando você tirar uma boa rolagem de ataque, você pode gastar um ponto de destino para "guardar" essa rolagem. Resolva o ataque normalmente, mas anote o total que você tirou, depois que todas as invocações e habilidades e todo o resto tiver sido somado. Em qualquer momento da mesma sessão, quando você estiver fazendo uma ação de defesa, você pode escolher "gastar" aquela rolagem que você tinha guardado, e usá-la ao invés de sua ação de defesa. Você pode fazer isso depois de rolar, igual a uma invocação, mas ela substitui o resultado da rolagem em vez de ser somada a ele.
+ ***Machuca mas não dói (da tara)***: Rebatedores aprendem a elevar seus corpos além do que qualquer humano responsável deveria, e na volta ainda metem a porrada. Quando você for atingido por dano vindo de algo físico, você pode gastar um ponto de destino para ignorar uma quantidade desse dano e mandá-lo de volta para seu atacante. Reduza o golpe em 2 e ganhe um impulso. Você só pode fazer isso uma vez por golpe. Além disso, cada vez que você pegar uma consequência grave, você imediatamente ganha a vantagem.
+ ***_É três por por cinco, bacana!_**: +2 em _Influencer_ para convencer uma outra pessoa se ficar claro que a outra pessoa também vai ganhar algo no processo.
+ ***Reto que arrebenta o nariz***: +2 em _Lutadora_ quando o alvo não estiver esperando pelo ataque.

## Estresse

+ Físico: `7`{: .fate_font}
+ Mental: `7`{: .fate_font}
