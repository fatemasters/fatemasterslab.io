---
title: Ficha Editáveis para Fate - traduções da Solar e da IndieVisivel
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2024-02-17 12:00:00 -0300
tags:
 - Fate
 - Fate-Basico
 - Fate-Condensado
 - Fate-Acelerado
 - Fichas
 - Fichas-Editáveis
 - Solar
 - IndieVisivel
---

Caros espectadores, depois de um hiato de um ano, eis que a _Tia Cicerone_ dá as caras por aqui -- para a alegria de alguns e tristeza de outros. Na postagem de hoje, porém, ela não vai conduzir ninguém pelos abismos da loucura da narração, nem desejar a ninguém _bons pesadelos_. Não, não... hoje o assunto é outro.

Atendendo a um pedido recorrente da comunidade tanto no WhatsApp quanto no Discord (links no topo da barra lateral da página), ela sentou a busanfa na cadeira e encodou e diagramou **Fichas editáveis** para todos os sabores de Fate! São arquivos bem pequenos (20 kB cada, aproximadamente), mas compatíveis com os principais leitores de arquivos .pdf disponíveis por aí. Como um bônus (_overachiever_ alguém?), ela criou versões tanto para a tradução da Solar, quanto para a tradução da IndieVisível. _Observação: considerando que na data em que esta postagem é escrita a IndieVisivel ainda não liberou as versões finais do Fate, pode ser que a terminologia final mude, e se isso acontecer, os arquivos abaixo serão editados de acordo._

As fichas foram diagramadas em uma página tamanho A5, um tamanho bem próximo das dimensões do livro físico e facilmente escalável para impressão A4 por qualquer leitor de arquivos .pdf que preste. A exceção a isso é o xodozinho da Cicerone, a _Ficha Cartão de Visitas para Fate Acelerado_, diagramada em frente e verso no tamanho de um cartão de visitas (90 mm x 50 mm) -- uma prova de como um sistema de RPG completo e poderoso como Fate não precisa de uma ficha cheia de números e informações atabalhoadas.

Agora, acabou a desculpa para não jogar Fate! Baixe gratuitamente as fichas editáveis, mande para os amigos, poste no grupo da família/trabalho/time de futebol/igreja/terreiro/coven/círculo, enfim, faça o que quiser com ela! Afinal de contas, a ficha está disponível sob licença [Creative Commons Atribuição CompartilhaIgual 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR). Use o texto abaixo para realizar a atribuição de crédito:

```
Ficha editável para Fate
2024 Lu "Cicerone" Cavalheiro (lu dot cicerone dot cavalheiro at gmail dot com)
Disponibilizada originalmente em Fate Masters - os algozes dos jogadores - https://fatemasters.gitlab.io/
Distribuída sob licença Creative Commons Atribuição CompartilhaIgual 4.0
```

Findo o palavrório, aos downloads!

## Tradução Solar

[Fate Básico](/assets/fichaseditaveis/2024/era_solar/core/FATE-Básico-PT-Era_Solar.pdf)

[Fate Acelerado](/assets/fichaseditaveis/2024/era_solar/fae/FATE-Acelerado-PT-Era_Solar.pdf)

[Fate Acelerado - Cartão de Visitas](/assets/fichaseditaveis/2024/era_solar/fae/cartao-de-visita/FATE-Acelerado-cartão-de-visitas-PT-Era_Solar.pdf)

[Fate Condensado](/assets/fichaseditaveis/2024/era_solar/condensado/FATE-Condensado-PT-Era_Solar.pdf)

## Tradução IndieVisivel

[Fate Base](/assets/fichaseditaveis/2024/era_indievisivel/core/FATE-Básico-PT-Era_IndieVisivel.pdf)

[Fate Acelerado](/assets/fichaseditaveis/2024/era_indievisivel/fae/FATE-Acelerado-PT-Era_IndieVisivel.pdf)

[Fate Acelerado - Cartão de Visitas](/assets/fichaseditaveis/2024/era_indievisivel/fae/cartao-de-visita/FATE-Acelerado-cartão-de-visitas-PT-Era_IndieVisivel.pdf)

[Fate Condensado](/assets/fichaseditaveis/2024/era_indievisivel/condensado/FATE-Condensado-PT-Era_IndieVisivel.pdf)

