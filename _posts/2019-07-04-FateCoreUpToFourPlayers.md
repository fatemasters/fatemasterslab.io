---
title: As Regras do Fate - Pelo UpTo4Players
#teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-06-16 16:30:00 -0300
categories:
  - Regras
tags:
 - Fate
 - Fate-Core
 - regras
---

E temos uma novidade pelos _Fate Masters_!

Conseguimos uma autorização do _Eran Aviram_ do _blog_ [Up to 4 Players](https://www.uptofourplayers.com) para traduzir e publicar em Português [os quadrinhos deles explicando as regras do Fate](https://www.uptofourplayers.com/fate-core-rules/)! 

Então, aqui está a explicação das regras do Fate: baixe, imprima e distribua a seus amigos!

+ ___Tradução:___ Maína "Palomita"
+ ___Revisão:___ Lu "Cicerone" Cavalheiro
+ ___Letras:___ Fábio "Mr. Mickey" Costa

+ [PDF para impressão](/assets/RegrasDoFate/RegrasDoFate-UpToFourPlayers.pdf)

Quadrinhos para visualização Online!

[![Página 1](/assets/RegrasDoFate/prefix_FATE_01.png)](/assets/RegrasDoFate/FATE_01.png)
[![Página 2](/assets/RegrasDoFate/prefix_FATE_02.png)](/assets/RegrasDoFate/FATE_02.png)
