---
layout: authors
show_meta: false
title: Lu Cavalheiro – _Cicerone_
contacts:
    email: lu.cicerone.cavalheiro@gmail.com
    telegram: lcavalheiro
avatar: /images/avatar_cicerone.png
---

Lu Cavalheiro, a _Cicerone_, nasceu em 30 de janeiro de 1986, e mora em Duque de Caxias/RJ. Tem nove filhos, todos felinos.  
  
Escritoa, servidoa pública federal e RPGista desde os 14 anos, já jogou vários sistemas e cenários de RPG até conhecer o FATE em 2014 por influência de Rafael Meyer, o _Velho Lich_. Juntou-se à equipe dos Fate Masters em 2016.  
  
Entre outras atividades, seus hobbies incluem artesanato e atividades manuais em geral, leitura, música e reclamar da vida.  
  
O apelido veio de seu bordão: _caros espectadores, eu irei guiá-los pelos abismos da loucura da narração_, uma clara referência a um de seus autores favoritos, HP Lovecraft.  
