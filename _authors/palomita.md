---
layout: authors
show_meta: false
title: Maína Paloma – _Palomita_
contacts:
    email: pampampaloma@gmail.com
    facebook: mainapaloma.delima
avatar: /images/avatar_palomita.jpg
---

Maína "Palomita" Paloma nasceu em 20 de maio de 1983, e mora em Duque de Caxias/RJ. Tem um filho humano e nove felinos.

Professora de História formada pela Universidade Federal do Rio de Janeiro, já exerceu diversas profissões antes de se tornar uma Fate Master. Começou a jogar RPG aos 13 anos, primeiro o irmão mais velho, depois GURPS e por fim _World of Darkness_, onde possui experiência e conhecimentos ímpares. Foi apresentada ao FATE por Lu "Cicerone" Cavalheiro em 2018, e juntou-se à equipe do podcast em 2019 no episódio 47.

Além de professora e RPGista, Palomita é artesã e segue o paradigma das ciências sociais de viver da arte. Entre outras coisas, ela faz produtos nerds em geral sob medida e por encomenda, e entrega para todo o Brasil.
