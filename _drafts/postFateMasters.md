---
title: 
teaser: O Podcast dos Algozes dos Jogadores
layout: post
date: 2019-06-16 16:30:00 -0300
categories:
  - Podcast
tags:
 - Fate
 - Fate-Core
 - fate-masters
 - fate-masters-podcast
 - Podcast
explicit: yes
header: no
podcast_comments: true 
hosts:
 - Fábio Emilio Costa
 - Maína "Palomita" Paloma de Lima
 - Luís Cavalheiro
 - Rafael Sant'anna Meyer
podcast_time: "110min"
itunes:
  duration: 01:50:00
audios:
 - OGG: https://archive.org/download/FateMasters47MesasSeguras/FateMasters47-MesasSeguras.ogg
 - MP3: https://archive.org/download/FateMasters47MesasSeguras/FateMasters47-MesasSeguras.mp3
iaplayer: FateMasters47MesasSeguras
soundtrack:
- music: Ambient Pills
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills
- music: Ambient Pills Update
  artist: Zeropage
  link: http://www.zeropage-media.com/index.php/free-music/ambient-pills-update
events:
  - time: "00:00:10"
    text: Introdução
related_links:
  - text: O Kit de Ferramentas de Segurança de Mesa que o Mr. Mickey mencionou
    link: https://drive.google.com/drive/folders/114jRmhzBpdqkAlhmveis0nmW73qkAZCj
episode: 65
---

E temos um novo Fate Masters, 



As redes sociais do Fate são:

- **E-mail:** <fatemasterspodcast@gmail.com>
- **Discord:** <http://bit.ly/FateBrasilDiscord>
- **WhatsApp:** <http://bit.ly/FateBrasilWhatsApp>
- **Telegram:** <http://t.me/FateBrasilTelegram>


E as redes sociais dos Fate Masters:

+ _**Mr Mickey:**_ `Fábio Costa#9087` no [Discord](https://discordapp.com) e `fabiocosta0305` ou `hufflepuffbr` em quase todas as redes sociais
+ _**Velho Lich:**_ [`rafael.meyer` no Facebook](https://facebook.com/rafael.meyer) ou [`eavatar` no Tumblr](https://eavatar.tumblr.com)
+ _**Cicerone:**_ `lcavalheiro#0520` no [Discord](https://discordapp.com) e [`lcavalheiro` no Telegram](https://t.me/lcavalheiro)
+ _**Palomita:**_ `Palomita#1312` no [Discord](https://discordapp.com) e [`mainapaloma.delima` no Facebook](https://www.facebook.com/mainapaloma.delima)

